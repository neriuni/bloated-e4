# はじめに
E4は、Everquestのエミュレーションサーバの一つである、Lazarusサーバでの使用に特化した、統合マクロツールである。同一の機能を提供するものとしてE3があるが、もともとはPEQサーバでの使用を想定したものである。E4はE3から余計な処理とコードを取り除き、いくつかの追加機能を加えたものである。いくつかのコードはE3から流用しているが、ほぼすべてスクラッチから作ったもので、E3と直接的な関係はない。機能としてはE3のもつものをすべて持ち、依存するものはなにもない。ここでは、E4の設定方法と使い方に関して説明を行う。なお、E4自体はまだ開発途中であり、機能と使い方は変更される。それに合わせてこのマニュアルも更新を行う予定。
# インストール方法
解凍したものを/macros直下にコピーする。最新のMQnextを利用すること。Macroquest2では動作検証をしておらず、正しく動作しないと思われる。
# 実行方法
ゲーム中で、/mac e4と実行する。ログイン時から常にE4を起動したい場合は、config/ingame.iniの設定を以下に変更する。
```
/mac e4
```
# 設定ファイル
Macros/e4_botsettings下に、キャラクター名.iniのファイルを作る。マクロ実行時にキャラクター名.iniを読み込むが、/mac e4 xxxxx.ini（あるいは/mac e3 xxxxx.ini）と指定すると、キャラクター名.iniのかわりにxxxxx.iniを読み込む。
設定ファイルはE3とは互換性がないため、最初から作り直す必要がある。ファイルがない場合、ほとんどの機能は利用できない。/Macros/e4_botsettings/template.iniというファイルが設定のテンプレートファイル。
# 制限
E4はE3と比較し、[Ifs]が使えず、E3のコマンドのみ使えるキャラクター制限（たとえば/only|xx、/not|xxなど）は使うことができない。E4では[Ifs]は使わずともよいように条件がつけられるようにしており、キャラクター制限に関しては非E3コマンドではいずれにしろ使用することができず紛らわしいため廃止した。これらの機能がなくとも別手段で同等以上の機能をE4は提供している。
# Everquestクライアントの設定
EverquestクライアントのOptions->Display->AdvancedのMax. Frames Per SecondとMax. Background FPSを両方ともUnlimitedに設定しておくこと。マシンパワーが厳しい場合は20から30に設定する。これが低いと、MQnextの動作もそれに合わせて遅くなる。
# 機能
以下にE4の各種機能に関して説明していく。
## 基本コマンド

**/followme**

実行したキャラクターに同一グループ内のキャラクターがあとを追いかける

**/bc follow all**

実行したキャラクターにEQBCSに接続した同一ゾーンにいるキャラクターが近づく

**/stop**

追いかけているキャラクターを停止させる

**/opendoor**

実行したキャラクターの一番近いドアを開ける、あるいはオブジェクトをクリックする

**/clickit**

実行したグループ全員が、一番近くのオブジェクトをクリックする

**/mtmあるいは/movetome**

実行したキャラクターにEQBCSに接続した同一ゾーンにいるキャラクターが近づく

**/mttあるいは/movetotarget**

実行したキャラクター以外のEQBCSに接続した同一ゾーンにいるキャラクターが近づく

**/rtz**

実行したキャラクターが向いている方向へ向けて、グループ全員が前へ進む。ゾーンするときに利用

**/Yes**

確認ダイアログのYesをグループ全員がクリックする

**/No**

確認ダイアログのNoをグループ全員がクリックする

**/dropinvis**

グループ全員が透明状態を解除する

**/dropbuff xxxx**

xxxxという名前のバフを解除する。xxxxにallを指定すると、経験値potのバフ以外すべて解除する

**/bc drop petbuff xxxx**

xxxxという名前のバフがペットに入っていれば解除する。xxxxにallを指定すると、ペットに入っているすべてのバフを解除する

**/consentme**

コマンドを実行した人に対して、EQBCSに接続した全キャラクターが/consentを行う

**/savegroup xxxx**

xxxxという名称でコマンドを実行したキャラクターの今のグループ構成を登録する

**/group xxxx**

登録されたxxxxというグループ構成でグループを編成する。

**/group again**

現在のグループを一度解散し、同じメンバーで組み直す

**/gathercorpsesあるいは/gathercorpse**

コマンドを実行したキャラクターの下に周囲の死体を集める

**/fdi xxxx**

xxxxというアイテム名を持っているなら報告する

**/fmi xxxx**

xxxxというアイテムを持っていないなら報告する

**/bark xxxx**

xxxxというメッセージを現在ターゲットしている相手にコマンド実行者の近くにいるEQBCSに接続されたキャラクターが話しかける

**/portto xxxxあるいは/bc port xxxx**

グループ内でGGが使える者がxxxxへGGを行う。xxxxの設定はe4_globalsettings\spell aliases.iniを参照。エイリアスの設定以外に、以下の設定も使用可能。
- aoaあるいはanguish: WIZとDRUで、OoW tier2 boots armorを持っている場合、Anguish前へGGする
- seru: Arx Keyを持っている場合、Sanctum SeruへGGする
- time: Secrets of the Planes、あるいはTime Portalの呪文を持っている場合、Plane of TimeAへGGする
- all: WIZの場合、Teleportを使う
- bind: WIZとDRUの場合、Teleport Bindを使う

**/Evac**

Evacする。DRUあるいはWIZがグループ内にいるとき有効

**/bc gohome**

グループ内のキャラクター全員がOriginあるいはGateを使う

**/bc gomarr**

グループ内のキャラクター全員がMarr's Callingを使う

**/bc lazCoH**

コマンドを実行したキャラクターがCoHアイテムを持っている場合、グループ全員をCoHする

**/bc Lesson**

EQBCSに接続した全員がLesson of the Devotedを使う

**/bc Equip slotname itemname**

slotname(mainhand, offhand等々)で指定した箇所にitemnameを装備する。

**/bc Halt anchorname**

**/bc Halt**

backoffし、stopし、その後anchornameで指定したキャラクターに移動する。すでにanchorでキャラクターを設定している場合はanchornameを指定しなくともよい。

## バフ
### 自動更新機能
E3にある、ペット管理、ペットバフ、proc系の呪文更新、自動Harvestはここに統合された。ただ自動Aura機能としては存在するため、Auraを指定する必要はない。自動Canniも専用の機能がある。
この機能はEQBCSに接続されたキャラクターにのみ効果がある。以下に設定ファイルに登録するフォーマットを示す。
```
[Buffs]
Buff=SpellName/Who|botname1 botname2 WAR SHD/Combat/Check1|SpellName2/Check2|SpellName3

/Who|botname1 botname2... @WAR @CLR @BRD...: Target to cast buffs
/Me: Cast on me
/All: Cast to everyone
/Only|botname: Cast on botname. This is faster than using /Who and /All
/OnlyMe: Cast on just me. This is faster than /Me
/NoTarget: Cast spell without changing the target. Uses caster's buff list for checking

/Gem|xx: Specify Spell Gem slot
/Disc: Add this if this is not an instant disc

/Reagent|itemname,reqnumber: Cast if you have reqnumber of "itemname"
/Grouped: Cast only to group members
/NotDead: Do not cast while dead
/NotFollowing: Cast when not following
/Tribute: cast when in tribute zone
/NoTribute: cast when you are not in tribute zone
/Raid: cast when you are in raid
/NotRaid: cast when you are not in raid

/GoM: cast when Gift of Mana or similar buff is in
/MGB: cast Mass Group Buff(when in combat) or Tranquil Blessing before cast

/Combat: cast only when you are in combat
/Always: cast even if you are in combat
/Check1|SpellName or spell ID: Check buffs
/Check2|SpellName or spell ID
/Check3|SpellName or spell ID
/Check4|SpellName or spell ID

/HateBelow|xx: cast if hate is lower than xx
/HateOver|xx: cast if hate is equal or higher than xx
/Hate2ndBelow|xx: cast if target second hate is lower than xx
/Hate2ndOver|xx: cast if target second hate is higher than xx
/Named: cast if target is Named
/NotNamed: cast if target is not Named

/ManaBelow|xx: cast if mana is lower than xx
/ManaOver|xx: cast if mana is equal or over than xx
/EndBelow|xx: cast if endurance is lower than xx
/PetBuff|mastername1 mastername2
/Summon: This is a pet spell
/Safe: cast only in safe zone.
/Unsafe: cast only if not in safe zone.
/HoT: This is a non-regeneration type Heal over Time spell
/IsPet: Cast if you have a pet
```

/Whoには詠唱したい相手を指定する。クラス名、キャラクター名で指定できる。クラスは短い名称で指定し、頭に@をつける(例、@WAR @PAL)

/Meは自分にバフを詠唱したい場合に設定する

/AllはEQBCSに接続された全員に詠唱したい場合に設定する

/PetBuffは、ペットにバフを詠唱したいときに指定する。指定するのはペットの主人の名前

/Onlyは単体で指定する場合に使用する。キャラクタの名前を指定する。/Onlyは/Whoや/Allを使用するより高速

/OnlyMeは自分だけに指定する場合に使用する。/Meより高速

/NoTargetは現在のターゲットを変更せずに使用する。バフの判定は自分のバフを利用する。自分専用のアイテムクリッキーなどで有効

/Gemは、spell gemスロットに呪文がない場合、セットするスロット番号を示す。指定しない場合は8番目にセットされる

/Discは、バフ型のDiscの場合に指定する。すでに既存のDiscが入っている場合は詠唱しない

/Reagent|itemname,reqnumberは、itemnameという名前のアイテムをreqnumber個持っているときのみ詠唱する

/Groupedは、対象が同じグループにいるときのみ詠唱する。SHMのメレーバフ等一部の呪文はグループに入っていないと使えないため、そのような呪文にはこれを指定する必要がある

/NotDeadは、死んで裸のときには詠唱しないようにする。CannibalizeなどHPをへらす技には必ずつけること

/NotFollowingは、フォローしていない間のみ詠唱する

/Tributeは、Tributeゾーンにいるときに詠唱する

/NoTributeは、現在のゾーンがTributeゾーンでないなら使用する

/Raidは、キャラクターがレイドグループに入っているなら使用する

/NotRaidは、キャラクターがレイドグループに入っていないなら使用する

/GoMは、Gift of Manaかそれに相当するバフが入っているときのみ詠唱する

/MGBは、戦闘中であれば、Mass Group Buffを、戦闘外であればTranquil Blessingを詠唱前に使う

/Combatは戦闘中のときのみこのバフを詠唱する。デフォルトでは戦闘中にバフは詠唱しない

/Alwaysは、戦闘中、戦闘外かまわずバフを詠唱する

/Check1は、ここで指定した呪文あるいはSpell IDがバフとしてすでに入っている場合、この呪文は使わない指定。Checkは1から4まで指定できる

/HateBelowは、対象のHateが指定した%以下になっているときのみ詠唱する

/HateOverは、対象のHateが指定した%以上になっているときのみ詠唱する

/Hate2ndBelowは、対象の二番手のHateが指定した%以下になっているときのみ詠唱する

/Hate2ndOverは、対象の二番手のHateが指定した%以上になっているときのみ詠唱する

/Namedは、ターゲットがゲームとしてNamedとして扱われている場合に使う

/NotNamedは、ターゲットがゲームとしてNamedとして扱われていない場合に使う

/ManaBelowは、マナが指定した%以下になっているときのみ詠唱する

/ManaOverは、マナが指定した%以上になっているときのみ詠唱する

/EndBelowは、Enduranceが指定した%以下になっているときのみ詠唱する

/Summonは、この呪文が永続するペット召喚呪文であるときに指定する。裸のときは使わない。

/Safeは、安全なゾーンにいるときのみ使う

/UnSafeは、安全でないゾーンにいるときのみ使う

/HoTは、リジェネレーションでない、HoT呪文である場合に指定する

/IsPetは、自身にペットがいる場合に詠唱する

### バフを乞う機能
以下のメッセージがきたときに対象に登録されたバフを詠唱する機能。
/tell bufferbotname buff me
自分にバフをしてもらう
/bc buff me
自分にバフをしてもらう
/tell bufferbotsname buff someone
someoneというキャラクターをバフする
/bc buff someone
someoneというキャラクターをバフする
```
[Buff]
Beg Buff=SpellName
```
バフを乞う機能では、相手のバフの有無や自分のマナの量はチェックしない。
### ペットのバフを消す機能
以下のように前もって不要なバフを設定しておくことで、定期的にそのバフを消す
```
[Buffs]
Pet Block=xxxx
Pet Block=yyyy
```
xxxx, yyyyにはバフの名前を登録する

### 自動オーラ機能
Warrior, Monk, Berserker, Bard, Druid, Cleric, Enchanter, Paladin, Magicianは現状使用可能な最も強力なオーラを自動的に使う。

## ヒール
### 自動ヒール機能
EQBCSに接続されたメンバーに対してヒールを行う。ヒールの対象は、基本的に現在のグループで割合で最も低いキャラクターに対して、設定されたヒールの条件があうかどうかで検出する。そのため、自分専用のヒールスキルはここで設定した場合、ヒールの頻度が大きく減る。自分専用のヒールは後述する自動ポーション使用機能を利用すること。
```
[Heals]
Heal=Healspell/Who|botname botname...WAR CLR.../HPBelow|80/HPOver|80/ManaOver|xx/ManaBelow|xx

/Who|botname1 botname2... @WAR @CLR @BRD...: Target to cast heal
/All: Cast to all
/Me: Cast on me
/Only|botname: Cast on botname. This is faster than using /Who and /All
/OnlyMe: Cast on just me. This is faster than /Me
/Pet|mastername1 mastername2
/Mate|botname: target who is in raid but out of EQBCS./HoT does not work if you put /Mate.
/ToT: This spell heals target of target. Works only you are attacking.
/NoTarget: Cast spell without changing the target. Uses caster's HP for checking.

/Gem|xx: specify Spell Gem slot

/ManaBelow|xx: cast if mana is lower than xx
/ManaOver|xx: cast if mana is equal or over than xx
/HPBelow|xx: cast if hp is lower than xx
/HPOver|xx: cast if hp is eual or over than xx
/AveHPBelow|xx: cast if than group avarage hp is lower than xx. does not consider pet hp

/Force: Tries to cast anyway and cancel last minuite if /HPBelow setting does not meet

/HoT: this spell is heal over time spell. checks the existing HoT
/GoM: cast when Gift of Mana or similar buff is in
/Donate: cast if caster's HP is over than 40%

/Calm: cast when not in combat nor following
/Combat: cast when in combat
/NotFollowing: cast when not following
/Tribute: cast when in tribute zone
/NoTribute: cast when you are not in tribute zone
/Raid: cast when you are in raid
/NotRaid: cast when you are not in raid
/NotDead: Do not cast while dead

/Safe: cast only in safe zone.
/Unsafe: cast only if not in safe zone.

/IsPet: cast if you have a pet

```
/Whoに詠唱したい対象を設定する。クラスでも設定可能。クラスは短い名称で指定し、頭に@をつける(例、@WAR @PAL)

/Allはヒール対象が全員であるときに設定する

/Petはヒール対象をペットにするときに設定する。ペットの主人を指定する

/Meはヒール対象が自分であるときに設定する

/MateはEQBCSに接続されていないレイドグループに入っている名前を指定する。複数人数指定できない。Mateを設定している場合は、/HoTは効果がない。同一のRaidグループにいないとヒールは行わない

/Onlyは単体で指定する場合に使用する。キャラクタの名前あるいはMeを指定する。/Onlyは/Whoや/Allを使用するより高速

/OnlyMeは自分だけに指定する場合に使用する。/Meより高速

/ToTはこの呪文の対象がターゲットのターゲットの場合に指定する。相手を攻撃しているときのみ機能する

/NoTargetは現在のターゲットを変更せずに使用する。HPの判定は自分のHP量を使う。heal potion、PALなどでのグループヒールなどで有効

/Gemは、spell gemスロットに呪文がない場合、セットするスロット番号を示す。指定しない場合は8番目にセットされる

/ManaBelowは、自分のマナが指定した%以下になっているときのみ詠唱する

/ManaOverは、自分のマナが指定した%以上になっているときのみ詠唱する

/HPBelowは、対象のHPが指定した%以下になっているときのみ詠唱する

/HPOverは、対象のHPが指定した%以上になっているときのみ詠唱する

/AveHPBBelowは、グループのHP平均が指定した%以下になっているときのみ詠唱する。ペットのHPは考慮しない

/Forceは、設定されているとHPBelowの設定条件を満たさなくとも呪文を詠唱する。詠唱完了500ms前にHPBelowの条件にマッチしなければキャンセルする。基本的にこの設定がされると唱え続けるため、Heal=の設定の最後の行に設定することを推奨する。

/HoTは、詠唱したい呪文がHoTであるときに指定する。これを指定することで、すでにHoTが入っているかチェックする

/GoMは、Gift of Manaかそれに相当するバフが入っているときのみ詠唱する

/Donateは、術者のHPが40%以上のときのみ詠唱する

/Calmは、戦闘外でかつフォローしていないときに詠唱する

/NotFollowingは、followしていないときに詠唱する

/Tributeは、tributeが有効なゾーンにいるときに詠唱する

/NoTributeは、現在のゾーンがTributeゾーンでないなら使用する

/Raidは、キャラクターがレイドグループに入っているなら使用する

/NotRaidは、キャラクターがレイドグループに入っていないなら使用する

/NotDeadは、死んで裸のときには詠唱しないようにする

/Safeは、安全なゾーンにいるときのみ使う

/UnSafeは、安全でないゾーンにいるときのみ使う

/IsPetは、自身にペットがいる場合に詠唱する

### 自動一時アイテム使用
EnchanterのSanguine Mind Crystal、Azure Mind Crystal、NecromancerのOrb of Souls、Magicianのマナ変換棒をマナあるいはHPが減ったときに自動的に使用する機能。自動的に有効になる。以下の設定を有効にすると、ペットに対してOrb of Soulsを使用する。
```
[Heals]
Enable PetHeal by Orb of Souls(On/Off)=On
```
NECのOrbはNEC以外戦闘中でしか使用しない。

### 自動ポーション使用
自身が対象で、ターゲットをとらないヒールスキルを設定する。設定できるスキルはポーションに限らない。複数設定可能
```
[Heals]
Potion=Distillate of Divine Healing IX/HPBelow|60
```
設定は自動ヒールと同じだが、/whoなど対象を指定する設定、/avehpbelow、/force、/hotの設定は無視される。セーフゾーンでは機能しない。また、以下の設定が追加される
```
/EndBelow|xx: cast if endurance is lower than xx
/EndOver|xx: cast if endurance is equal or over than xx
```

### 完全自動ヒール機能
特定のヒール呪文をセットしておくことで、詠唱するタイミングも自動的に決定されるモード。これが有効であるときは、自動ヒール機能は設定が無視され無効となるが、自動一時アイテム使用と自動ポーション使用は引き続き機能する。このモードは、基本的にヒールを潤沢に行うかわりにマナを無駄に使いがちであるため、マナ供給が安定している場合でないと効果を発揮しない。また、他グループのメンバーやペットにはヒールを行わない。
```
[AutoHeal]
Auto-Heal (On/Off)=On

Priority=tankname
Priority=tankname2

Short=Ancient: Wilslik's Mending
HoT=Spiritual Serenity
Group=Word of Reconstitution
GHoT=Ghost of Renewal

Fast=Zun'Muram's Spear of Doom
Fast=Union of Spirits
Fast=Aged Hammer of the Dragonborn

Self=Rabid Bear

MyPet=pethealspell
MyPet=pethealspell2

Pet=petmastername
Pet=petmastername2
```
Priorityにはタンクの名前を記述する。一人のみ指定可能。Shortにはスペルのショートヒールを登録、HoTにはスペルの単体HoTを登録、Groupにはスペルのグループヒールを登録、GHoTにはスペルのグループHoTを登録する。Fastには、AAやアイテムなど高速だが再使用が長いスキルを登録する。ここは複数登録が可能。SelfもFastと同じだが、自分専用の高速ヒールを登録する。MyPetもFastと同様だが、自ペット専用のヒールを登録する。Pet=はPetでタンクをしたい場合のPetの主人名を設定する。Priorityにはグループ外のキャラクター名を設定しても動作する。Petはグループ外のキャラクター名のペットにはヒールは行わない。

## 自動キュア
毒、病気、呪いを自動的に解除する機能。この３つのカテゴリ外のデバフは手動でRadiant Cureを使うなどで治療する必要がある。自動キュアはSelfでない場合は基本戦闘外でのみ機能する。もし戦闘中にキュアを行いたいのであれば、/bc combatcure onと指定する。
```
[Cures]
All=Purify Soul
Poison=Pure Blood
Disease=Pure Blood
Curse=Remove Greater Curse

Self=Purified Spirits
Self Poison=
Self Disease=
Self Curse=

Mate=toon1
Mate=toon2
```
All=にはどのデバフでも解除できる呪文を指定する
Poison=には毒を解除できる呪文を指定する
Disease=には病気を解除できる呪文を指定する
Curse=には呪いを解除できる呪文を指定する
Self=には自分専用のすべてのデバフを解除できる呪文を指定する
Self Poison=には自分専用の毒を解除できる呪文を指定する
Self Disease=には自分専用の病気を解除できる呪文を指定する
Self Curse=には自分専用の呪いを解除できる呪文を指定する
Mate=には同じraidにいてEQBCSに接続されていないキャラクターを指定することでキュア対象として扱う
## 戦闘
戦闘マクロは、設定したターゲットが死ぬまで続く。戦闘マクロを実行したときに、条件によって、制御権をマクロにわたす場合と渡さない場合の二種類にわかれる。制御権を渡した場合、マクロが移動と攻撃、スキルや呪文の使用を自動的に行う。Feign Deathの状態も勝手に解除する。制御権を渡さなかった場合、現在のターゲットにスキルや呪文を使うが、移動やターゲットの変更はユーザーが行うことができる。

**/duel**

ターゲットしている相手にコマンドの実行者だけ攻撃を開始する。制御権は渡さない。このコマンドだけトグル式で、もう一度実行すると攻撃をやめる。

**/combat**

ターゲットしている相手にグループ全員が攻撃を開始する。制御権は、コマンドの実行者は渡さず、他のメンバー全員e4に渡す

**/fight**

ターゲットしている相手に自分以外のグループ全員が攻撃を開始する。制御権は、他のメンバー全員e4にわたす

**/battle**

ターゲットしている相手にグループ全員が攻撃を開始する。制御権は全員渡す

制御権を渡した場合、ターゲットのmobが死ぬまでマクロによる制御が続く。制御権を渡さなかった場合、XTargetウインドウから敵対的なmobが範囲内から消えるまで続く。
/combatと/battleは引数にキャラクター名を与えることで、そのキャラクター名を戦闘に参加させることができる。例えば、
/combat toonname1 toonname2
と実行した場合、コマンドを実行した本人と、toonname1とtoonname2が戦闘に参加し、その他のキャラクターは何もしない。
### 戦闘方法・位置設定
```
[Combat]
SmartTaunt (On/Off)=On
Attack Type (Melee/Ranged/Off)=Melee
Stick Point (Front/Behind)=Front
Range distance=50
```
SmartTauntは、仮に今一番高いヘイトを持っている相手が非タンククラスであった場合、Tauntして取り返す機能を有効にするかどうかを決める。Tauntスキルを持つクラスのみ有効。

Attack Typeは、近接か遠隔か、あるいは武器による攻撃をしないかを設定する。Stick Pointはmobの全面に回ろうとするか、後ろに回ろうとするかを設定する。attack typeとstick pointは独立しており、別々に指定することができる。例えば、attack typeをOffにし、stick pointをFrontに指定すると、常に攻撃はしないものの、常にmobの正面を向こうとする。Attack TypeとStick Pointで指定できる設定は以下の通り。

#### Attack Type
**Melee:** 近接攻撃を行う。最もHPが低いmobをターゲットする。

**Ranged:** 遠隔攻撃を行う。最もHPが低いmobをターゲットする。

**Off:** 近接攻撃を行わない（AutoSkillは使用する）。最もHPが低いmobをターゲットする。

#### Stick Point
**Behind:** 対象のmobの後ろへ回り込もうとする。ヘイトが100%になったときは背面にまわろうとせず、その場にとどまろうとする

**Front:** 対象のmobの正面に回り込もうとする

**Ranged:** 対象のmobからRange distance=で設定された距離より近くにいる場合、後ろに下がる

**Keep:** 対象のmobからRange distance=で設定された距離を取ろうとする

**pbshoot:** 対象のmobの近接距離に移動する

**see:** 対象のmobの視線が通るところまで移動する

**cling:** 対象のmobの視線が通り、グループのリーダーとの距離を80以下に保とうとする

**coward:** ヘイトが100以上のmobからRange distance=で設定された距離以上に保とうとする。ヘイトが100より下であった場合、現在ターゲットしているmobの視線内に入ろうとする。petタンクモードが有効なときに便利

**Off:** 移動しない

キャスターがAEを行うときは、Attack TypeはOff、Stick Pointをpbshootにするとよい。この設定はini以外にコマンドで変更することが可能。

**/bc Stance attacktype sticktype**
コマンドを実行したキャラクターのロールが変更される。attacktypeにAttack TypeをsticktypeにStick Pointを指定する。

RangedとKeepに関しては、以下の設定で距離を設定できる。デフォルトは100。
```
[Combat]
Stick Point (Front/Behind)=ranged
Range distance=20
```

### 自動スキル・呪文使用
/duel、/combat、/battleが有効である間、制御権を渡した場合、コマンドを実行したときにターゲットしたmob、制御権を渡さない場合、現在のターゲットに対して登録されたスキル、呪文を使用する。
```
[Combat]
Auto Skill=spellskillname/Me/HPBelow|80/HPOver|80/ManaOver|xx/ManaBelow|xx

/Me: target self when cast
/Cycle: target most highest HP mob in XTarget. Enabled when in autobattle

/Gem|xx: set spell gem slot

/MGB: cast Mass Group Buff(when in combat) or Tranquil Blessing before cast
/GoM: cast if you have Gift of mana or similar effects
/RCheck1|xx: cast if you have xx buff
/Named: cast if target is Named
/NotNamed: cast if target is not Named
/Reagent|itemname: Cast if you have "itemname"
/Orb: Casts if you have less than 4000 shadow/soul orbs. This also stores orbs to bag. Works only with Necromancer

/ManaBelow|xx: cast if my mana is lower than xx
/ManaOver|xx: cast if my mana is equal or higher than xx
/HPBelow|xx: cast if my hp is lower than xx
/HPOver|xx: cast if my hp is equal or higher than xx
/MobHPBelow|xx: cast if target hp is lower than xx
/MobHPOver|xx: cast if target hp is equal or higher than xx
/HateBelow|xx: cast if hate is lower than xx
/HateOver|xx: cast if hate is equal or higher than xx
/Body|xx: cast if target body type is xx
/MobLvBelow|xx: cast if target level is lower than xx
/MobLvOver|xx: cast if target level is higher than xx
/Hate2ndBelow|xx: cast if target second hate is lower than xx
/Hate2ndOver|xx: cast if target second hate is higher than xx
/AveHPBelow|xx: cast if than group avarage hp is lower than xx. does not consider pet hp

/AE|xx: checks there are equal or more than xx mobs in AE range

/Debuff: this spell is debuff. Checks target has this debuff
/Stackable: put this option if the DoT is stackable with other character's same spell. Works with /Debuff
/Abashi: Check mob has beneficial buffs. Works with /Debuff.
/Trap: check if the spell is idol of malos
/Dampen: Cast Dampen before casting. Works only with /Debuff
/Disc: Add this if this is not an instant Disc
/DCheck1|xx: Available when /Debuff is set. Checks target has xx debuff
/DCheck2|xx: Available when /Debuff is set. Checks target has xx debuff
/DCheck3|xx: Available when /Debuff is set. Checks target has xx debuff
/DCheck4|xx: Available when /Debuff is set. Checks target has xx debuff

/Buff: skill is a beneficial buff. checks same buff is already in or not
/Check1|xx: Do not cast if you already have xx in buff/song slot

/Body|xx: Checks body type(e.g. Humanoid, Undead, Muramite etc)
/Caster: Cast when target is caster

/Tribute: Checks if it is tribute zone
/NoTribute: cast when you are not in tribute zone
/Raid: cast when you are in raid
/NotRaid: cast when you are not in raid

/Force: Ignore resist count
/Face: Face target before casting. Need to be set if the spell is cone or bolt type

/stun|xx: Do not cast stun spell for xx sec. If you set this when you are CLR, it will check the group leader has "Recourse of Life Rune" Buff
/melee: casts when "Attack Type" is not set "Off"

/IsPet: cast if you have a pet
```
/Meは、スキルや呪文を使用するときに自分をターゲットする

/Cycleは、XTarget内で最もHPの高いmobをターゲットする。自動戦闘使用時のみ有効

/Reagent|itemnameは、itemnameという名前のアイテムを持っているときのみ詠唱する

/MGBは、戦闘中であれば、Mass Group Buffを、戦闘外であればTranquil Blessingを詠唱前に使う

/GoMは、Gift of Manaかそれに類するバフが効いているときに使う

/NoGoMは、Gift of Manaかそれに類するバフが効いていないときに使う

/RCheck1|xxは、自分にxxのバフが入っているときのみ使う

/Namedは、ターゲットがゲームとしてNamedとして扱われている場合に使う

/NotNamedは、ターゲットがゲームとしてNamedとして扱われていない場合に使う

/Reagent|itemnameは、itemnameというアイテムを所持しているときに使う

/Orbは、Orb of ShadowsかOrb of Soulsの所持数が4000個以下のときに使う。生成されたorbは自動的にインベントリへ入れる。NECでしか機能しない。

/Gemは、spell gemスロットに呪文がない場合、セットするスロット番号を示す。指定しない場合は8番目にセットされる

/ManaBelowは、自分のマナが指定した%以下になっているときのみ詠唱する

/ManaOverは、自分のマナが指定した%以上になっているときのみ詠唱する

/HPBelowは、自分のHPが指定した%以下になっているときのみ詠唱する

/HPOverは、自分のHPが指定した%以上になっているときのみ詠唱する

/MobHPBelowは、対象のHPが指定した%以下になっているときのみ詠唱する

/MobHPOverは、対象のHPが指定した%以上になっているときのみ詠唱する

/HateBelowは、対象のHateが指定した%以下になっているときのみ詠唱する

/HateOverは、対象のHateが指定した%以上になっているときのみ詠唱する

/Body|xxは、対象のボディタイプがxxであるときに詠唱する

/Casterは、対象がWAR, ROG, BER, MNKでないときに詠唱する

/MobLvBelowは、対象のLvが指定した値以下になっているときのみ詠唱する

/MobLvOverは、対象のLvが指定した値以上になっているときのみ詠唱する

/Hate2ndBelowは、対象の二番手のHateが指定した%以下になっているときのみ詠唱する

/Hate2ndOverは、対象の二番手のHateが指定した%以上になっているときのみ詠唱する

/AveHPBBelowは、グループのHP平均が指定した%以下になっているときのみ詠唱する。ペットのHPは考慮しない

/AE|xxは、呪文の範囲内に指定した数以上のNPCがいるときに詠唱する

/Debuffは、DoTやデバフが対象にすでに入っているかどうかをチェックする

/Stackableは、同一クラスの他キャラクターとスタック可能な同一のDoTを使うときに指定する。/debuffを指定したときに有効になる。一般的に、ダメージのみのDoTは他キャラクターとスタックし、ダメージにレジスト低下などのデバフ効果のついたDoTはスタックしない。

/Abashiは、対象に有益なバフが入っているかどうかをチェックする。/debuffを指定したときに有効になる。

/Trapは、Shamanのスペル、Idol of Malosの場合設定する

/Discは、バフ型のDiscの場合に指定する。すでに既存のDiscが入っている場合は詠唱しない

/DCheck1|xxは、xxで指定したデバフが対象に入っているかどうかをチェックする。/Debuffが設定されているときのみ有効。最大4つ指定できる

/Buffは、スキルが有益なバフかどうかを指定する。この指定がある場合、同じIDのバフがすでにはいっているのであれば詠唱しない

/Check1|xxは、xxのバフが自分にはいっていない場合に詠唱する

/Body|xxは、対象のbodyの種類をチェックする。UndeadやSummoned等々

/Tributeは、現在のゾーンがTributeゾーンなら使用する

/NoTributeは、現在のゾーンがTributeゾーンでないなら使用する

/Raidは、キャラクターがレイドグループに入っているなら使用する

/NotRaidは、キャラクターがレイドグループに入っていないなら使用する

/Forceは、対象に対してレジストされても無視して詠唱を続けるようになる

/Faceは、詠唱する前にターゲットの方向を向く。コーン、ボルト系の呪文に指定する

/Stun|xxは、詠唱後xx秒間、/stunが指定された呪文は詠唱しないようにする。また、CLRで指定した場合、グループのリーダーにRecourse of Life Runeが入っていない場合詠唱をする

/meleeは、Attack TypeがOffでない場合に呪文を詠唱する

/IsPetは、自身にペットがいる場合に詠唱する

#### AE有効、無効フラグ
バーン、コマンドリスト、Auto-Skillにおいて、呪文のタイプがAE系(PB AE、Target AE等)を以下のコマンドを実行することで、使用を抑制することができる。再び同じコマンドを実行することで有効になる。

**/bc ToggleAE**

### バーン・コマンドリスト実行
```
[Burn]
Burn A=Fundament: First Spire of the Sensei
Burn A=Infusion of Thunder
Burn A=Heel of Kanji

Burn B=xxxx

Burn C=xxxx

Burn D=xxxx

Command Skill A=xxxx

Command Skill B=xxxx

Command Skill C=xxxx

Command Skill D=xxxx
```
Auto Skillと同じように設定することが可能。コマンドリストは、全部で8つ。Burn AからBurn DとCommand有効にするコマンドは以下の通り。

**/bc commandset BurnA**

バーンAを有効にする

**/bc commandset BurnB**

バーンBを有効にする

**/bc commandset BurnC**

バーンCを有効にする

**/bc commandset BurnD**

バーンDを有効にする

**/bc commandset CommandA**

コマンドリストAを有効にする

**/bc commandset CommandB**

コマンドリストBを有効にする

**/bc commandset CommandC**

コマンドリストCを有効にする

**/bc commandset CommandD**

コマンドリストDを有効にする

これらのコマンドリストの実行は戦闘が終了すると自動的に無効になる。コマンドの実行優先度は、バーンA>バーンB>バーンC>バーンD>コマンドA>コマンドB>コマンドC>コマンドD>Auto Skillとなる。

### ペットタンク機能
以下の設定にすることで、ペットはXTarget内のmobのヘイトを100に保とうとする。stick typeはcowardがおすすめ。
```
[Combat]
PetTank=Main
```
もしグループで複数のペットでタンクをさせる場合、2匹目以降は以下の設定を行うこと
```
[Combat]
PetTank=Sub
```

## 自動戦闘
自動戦闘マクロは、戦闘マクロを利用したもので、基本的に現在のXTargetウインドウ内にいる近くのmobがすべて倒されるまで持続する。コマンドは、グループ全体の制御権を渡すものと、コマンドの実行者以外の制御権を渡すものの二種類がある。後者はレイドなどで、タンクを個別に制御したいときに利用する。
自動戦闘を利用するためには、役割を設定する必要がある。デフォルトでは役割は設定されていないため、そのままでは自動戦闘を利用することができない。役割は、iniファイルで設定する。
```
[Auto Battle]
Role (Tank/Attacker/Off)=Attacker
```
Tankはタンクとして動く場合に設定する。XTarget内でヘイトを常に100に維持しようとし、全てのmobが100であった場合は、最もHPの高いmobを攻撃する。Attackerは、mobに対して攻撃を行う場合に設定する。attack typeがmeleeの場合、現在XTarget内で最もレベルが低く、かつ最もHPの低い対象を攻撃する。attack typeがoffの場合、現在のXTarget内で最もHPの高い対象を攻撃する。自動戦闘に参加させない場合はoffと設定する。

グループ内に複数タンク職(WAR, PAL, SHD)がおり、そのキャラクターにはタンクの仕事をさせたくない場合、役割はAttackerと設定し、役割をTankにするキャラクター側で以下の設定をする。Non-Tank=にタンクの仕事をさせたくないタンク職のキャラクター名を書く。
```
[Auto Battle]
Role (Tank/Attacker/Off)=Tank
Non-Tank=toonname
```
以下が自動戦闘を開始するコマンド

**/bc autobattle**

自動戦闘を開始する。制御権をすべて渡す。

**/bc autocombat**

自動戦闘を開始する。コマンドの実行者の制御権は渡さない。

**/bc stopbattle**

自動戦闘を停止する。現在戦っているmobとの戦闘は終了しない

**/bc retarget**

attackerの攻撃対象を設定しなおす。現在XTarget内で最もレベルが低く、かつ最もHPの低い対象を攻撃する。

**/bc role toonname newrole**

toonnameというキャラクターに新しい役割を設定する。newroleにはtankあるいはattackerを設定する。
## スキルの使用
手動でスキル・呪文を使う場合は、通常通りにスペルジェムやホットキーを押すことで使用することができるが、マクロが動いてなにかしらの行動を行っている間は、そちらに行動が優先され、無視されることがある。以下に示すコマンドによって、スキルや呪文はキューイングされ、詠唱が完了するまで4回実行しようとする。

**/qcast spellname**

spellnameという呪文・スキルを現在のターゲットに使う

**/bc QSelf spellname**

spellnameという呪文・スキルを自分につかう

**/bc QMe spellname**

spellnameという呪文・スキルをコマンド実行者に詠唱してもらう。EQBCS内にいる設定された呪文・スキルが詠唱可能なキャラクターがコマンド実行者へ詠唱する

**/bc botname QBot spellname**

spellnameという呪文・スキルをbotnameというキャラクター名に詠唱する。botnameはEQBCSに接続されたキャラクターのみ有効

**/queue targetid "spellname"**

**/bc targetid QTarget spellname**

spellnameという呪文・スキルをtargetidに設定されたIDのキャラクターに詠唱する。

## クラス特有の機能
ここではクラス特有の機能に関して説明する。
### Warrior
#### Field Armorer使用
自動的に戦闘中の間Field Armorerを使用する。この機能を無効にする場合は以下の設定を行う。
```
[Warrior]
Armorer (On/Off)=Off
```
#### 自動Second Wind使用
戦闘外でEnduranceが7割を切った場合、あるいは戦闘中で3割切った場合にSecond WindあるいはThird Windを自動的に実行する。戦闘中でEnduranceが85%を超えた場合は自動的に停止する

### Paladin
#### 自動Yaulp実行機能
AAのyaulpをもっている場合、戦闘中自動的に使用する。

### Bard
#### 自動演奏・楽器切り替え機能
設定された曲を並べられた優先度で、曲を回す機能。曲が有効になる瞬間設定された楽器にも自動的に切り替える。以下が設定例
```
[Bard]
Auto-Melody=normal

Song=Edge of Chaos
String=Lute of False Worship
Drum=Lizard Skin Wardrums
Wind=Muramite Fife of Battle
Brass=Deathcaller's Stonewind
Normal=Death's Head Mace

[normal Melody]
Song=Psalm of Veeshan/Song
Song=Song of the Storm/Song
Song=Niv's Harmonic/Song
Song=War March of the Mastruq/Brass
Song=Song of Sustenance

[slow Melody]
Song=Lute of False Worship/Debuff/AE|4
```
Song=、String=、Drum=、Wind=、Brass=にはそれぞれブーストのつく楽器を設定する。この楽器は左手に装備されることを想定している。Normalは歌を歌っていないときに装備したい武器を設定する。
Auto-Melody=にデフォルトで歌いたい歌を設定する。歌は、[xxxx Melody]という項目で歌を並べる。各歌のあとに/で使用したい楽器を設定する。デバフなどの有害な呪文には/Debuffをオプションとしてつける。/Debuffをつけた場合、/AE|xx、/Named、/Dcheck1、/MobLvBelowと/MobLvOverが有効になる。/AE|xxは周囲にxx匹より下のmobがいる場合歌い、/Namedはターゲットしている相手がNamedであれば歌う。
Selo's Accelerating Chorusを歌として設定した場合は持続時間が残り20秒になったときに使用する。Song of Sustenanceも同じで、残り時間が20秒になったときに使用する。また、戦闘外でのみ使う。Song=に/raidを指定すると、raidグループを組んでいるときのみその歌を歌う/Forceを指定すると、まだ歌のバフがあった場合でも設定された歌を使う。/calmをつけた場合戦闘外でのみ歌う。
設定されたメロディの切り替えは/bc melody xxxxで変更する。xxxxはiniファイルで設定されたメロディの名前。今歌っている歌を止める場合は、/bc melody stopを実行する。
#### 自動Selo's Sonata機能
グループ内のメンバーに常にこのバフがかかるように使用する機能。自動的に有効になる。
#### 楽器クリッキー使用
Lute of the Flowing waterなどいくつかのBRD向けクリッキーを自動的に使用する機能。所持していれば自動的に使用するものと、自動演奏で指定することで使用するものがある。
##### 所持していれば自動的に使用するもの
- Nature's Melody
- Lute of the Flowing Waters
##### 自動演奏で設定できるもの
Song=の後にアイテム名を指定する。フォーカスの楽器は設定された楽器を自動的に装備するため、/Drum等設定する必要はない。
- Lute of False Worship
- Aged Lyran's Mystical Lute
- Invocation Rune: Vulka's Chant of Lightning
### Cleric
#### 自動Divine Arbitration使用
グループの平均HPが50を切った場合に、自動的に使用する。使用後、Celestial Regenerationも自動的に使う。
#### 自動周囲蘇生機能
自動的に周囲の死体をあつめて適切な蘇生魔法で蘇生を行う。コマンドは以下の通り
```
/bc whocast aerez whorez
```
whocastには蘇生魔法を使うキャラクター名あるいは短い名称のクラスを指定する。キャラクター名のかわりにallを指定するとEQBCSに接続した全員が蘇生を試みる。指定されたキャラクターのクラスがCLRはその時点で使える最も強力な蘇生魔法を自動的に選択する。その他のクラスは蘇生アイテムである、Token of Ressurectionがあればそれを使う。ない場合は、SHMとDRUは戦闘外であればIncarnate Anewを選択し、PALはその時点での最も強力な蘇生魔法を選択する。
whorezには蘇生したいクラスを指定する。クラスは短い名称で指定する。allを指定した場合は周囲の死体を全部蘇生しようと試みる。
Token of Ressurectionを使用する場合は、蘇生は一度しか試さない。他の手段による蘇生の場合2回繰り返す。
以下、使用例
```
/bc CLR aerez all
```
EQBCSに接続したCLRが周囲の死体全部の蘇生を試みる。
```
/bc Smith aerez WAR CLR
```
EQBCSに接続したSmithという名前のキャラクターが周囲のWARとCLRの死体の蘇生を試みる。

#### 自動ペット武器渡し機能
/bc pet weaponか/tell xxxx pet weaponで、周囲のキャラクターのペットに設定された武器を配る。設定は以下の通り
```
[Cleric]
Summoned Pet Item=Hammer of Damnation
```
#### 自動Yaulp使用機能
AAのyaulpをもっている場合で、近接攻撃をするよう設定されている場合か、manastoneを使用する設定担っている場合、戦闘中自動的に使用する。
#### 自動Divine Avatar使用機能
AAのDivine Avatarをもっている場合で、近接攻撃をするよう設定されている場合、戦闘中自動的に使用する。

### Shaman
#### 自動Languid bite使用
戦闘中自動的にLanguid biteを使用する。
#### 自動Ancestral Aid使用
グループの平均HPが90を切った場合に、自動的に使用する。
#### Inconspicuous Totem自動使用
定期的にinconspicuous totemを使用する。
```
[Shaman]
Auto Totem(On/Off)=On
```

#### 自動Cannibalize使用
設定に従い、Cannibalizeを自動的に使用する。指定した呪文は、死んで裸でない状態で、フォローしておらず、透明でないときに使う。Heavy CannibalizationはPoK、Bazaar、Nexusでは使わない。/calm、/combatを指定可能
```
[Shaman]
Light Cannibalization=Ancient: Ancestral Calling/HPOver|70/ManaBelow|90
Heavy Cannibalization=Cannibalization/HPOver|70/ManaBelow|85
```
### Rogue
#### 自動Thief's eye使用
自動的に戦闘中の間Theif's eyeを使用する。
#### 自動Evade機能
戦闘中自動的にEvadeしヘイトを下げる。有効にするためには以下を設定する
```
[Rogue]
Auto-Evade (On/Off)=On
```
#### 自動Second Wind使用
戦闘外でEnduranceが7割を切った場合、あるいは戦闘中で3割切った場合にSecond WindあるいはThird Windを自動的に実行する。戦闘中でEnduranceが85%を超えた場合は自動的に停止する
#### 自動ハイド、スニークアタック使用
自動的にsneak+hideを利用し、戦闘中にスニークアタックを行う場合は以下の設定を行う。
```
[Rogue]
Auto-Hide (On/Off)=On
Sneak Attack Discipline=Razorarc
```
Auto-EvadeとSneak AttackはRogueのみの機能。Auto-Evadeはある程度ヘイトが高くなってきた場合に自動的にヘイト下げを使う機能。Sneak Attackはスニーク＆ハイド中に使える特殊なBackstab技を使うかどうかの設定。Rogueでない場合はこのふたつの項目を設定する必要はない。

### Ranger
#### 自動Trueshot使用
自動的にTrueshot Discを使用する。
#### 自動Guardian of the Forest使用
自動的にセルフ版、グループ版を重ならずに使用する。デフォルトで有効以下の設定で無効化することができる。
```
[Ranger]
Auto Guardian of the Forest(On/Off)=Off
```
#### 自動特殊矢使用
```
[Ranger]
;Arrow Buff=Frost Arrows
Arrow Buff=Flaming Arrows
```
AAの特殊な矢を使うバフを再使用時間に達しているときで戦闘外であるときに自動的にバフをオフし、マナの消費量を抑える機能。また戦闘中に自動的に使用する。/bc arrow arrownameを実行することで、arrownameに特殊矢の名称をしていすることで、マクロをリロードしなくとも変更することができる。
### Berserker
#### 自動Second Wind使用
戦闘外でEnduranceが7割を切った場合、あるいは戦闘中で3割切った場合にSecond WindあるいはThird Windを自動的に実行する。戦闘中でEnduranceが85%を超えた場合は自動的に停止する
#### 自動斧召喚
以下の設定を行うことで、斧の数が20より少なくなるか設定された数より少なくなった場合に斧を召喚する。触媒の設定は不要。
```
[Berserker]
Summon Axe=Tainted Axe of Hatred/Count|20
```
### Druid
#### 自動Spirit of Wood使用
グループの平均HPが90を切った場合に、自動的にPeaceful Spirit of the WoodあるいはSpirit of the Woodを使用する。
#### 自動Communion of Cheetah使用
グループ内のメンバーに常にこのバフがかかるように使用する機能。
#### 自動Spirit of the Black/White Wolf使用
自身のマナが90を切った場合に、Spirit of the Black/White Wolf変身を個人用、グループ用両方使用する。グループ用はBlack Wolf固定。個人用は以下で設定を行う。Blackを設定した場合は黒狼に、Whiteを設定した場合は白狼に、Offを設定した場合自動変身自体を無効とする。

```
[Druid]
Spirit Wolf=Off
;Spirit Wolf=Black
;Spirit Wolf=White
```
### Beast Lord
#### 自動Paragon使用
グループ内のManaとEnduranceの平均値が80を切った場合、Paragon of Spiritを使用する。グループ内のメンバーのManaあるいはEnduranceが70を切った場合、Focused Paragon of Spiritsを使用する。
### Monk
#### 自動Second Wind使用
戦闘外でEnduranceが7割を切った場合、あるいは戦闘中で3割切った場合にSecond WindあるいはThird Windを自動的に実行する。戦闘中でEnduranceが85%を超えた場合は自動的に停止する
### Magician
#### 自動molten orb召喚機能
戦闘外で6人でグループを組んでいるときに、Molten orbあるいはLava Orbがインベントリにない場合自動的に召喚する。Spell gemに自動召喚したい呪文を前もってセットしておくこと。
#### 自動ペット装備渡し機能
65以上で使えるSpectral系のアイテムを自動的に渡す機能。この機能を使うためにはslot10はか空いているか破壊してもかまわないpet召喚アイテムがある必要がある。

**/bc petitem armor**あるいは
**/tell magname petitem armor**

コマンドを入力したあるいはtellを行ったキャラクターのペットにSpectral HeirloomとSpectral Armor一式を召喚し渡す。Heirloomは2会渡し、もし、BerserkerのTainted Axe of Hatredをもっていた場合、それも渡す。

**/bc petitem weapon**あるいは
**/bc petitem weapon xxxx yyyy**

**/tell magname petitem weapon**あるいは
**/tell magname petitem weapon xxxx yyyy**

コマンドを入力したあるいはtellを行ったキャラクターのペットにSpectral Armamentsを召喚し、渡す。xxxxとyyyyには武器の種類を指定する。種類は以下の通り。xxxxが最初にわたすアイテムで、yyyyが二番目に渡すアイテムとなる。種類を指定しない場合、coldと扱われる。
```
fire : Summoned: Fist of Flame
cold : Summoned: Orb of Chilling Water
rune : Summoned: Buckler of Draining Defense
taunt : Summoned: Short Sword of Warding
malo : Summoned: Spear of Maliciousness
slow : Summoned: Mace of Temporal Distortion
abashi : Summoned: Wand of Dismissal
snare : Summoned: Tendon Carver
```

**/tell magname petitem all**あるいは
**/tell magname petitem all xxxx yyyy**

Spectral Hairloom x2, Armor, tainted axe, armamentsを渡す。

**/bc petitem dispose**

距離100以内にいる同じeqbcsに入っているメンバーにpetitem allと同等のアイテムを渡す

#### 旧自動ペット武器渡し機能
/bc pet weaponあるいは/tell xxxx pet weaponで、周囲のキャラクターのペットに設定された武器を配る。設定は以下の通り
```
[Magician]
Summoned Pet Item=Blazing Stone of Demise
```
### Necromancer
#### Lich自動切断機能
HPが20%を切っている場合、自動的にHPマナ変換呪文のバフを消す。
#### Mind Flay/Mind Wrack自動使用機能
Xtargetに距離200以内で視線が通っているmobがいる場合、戦闘指示がなくともMind FlayあるいはMind Wrackを詠唱する
#### Embalmer's Carapace自動使用機能
HPが50%を切った場合、自動的にEmbalmer's Carapaceを使用する。Embalmar's Carapaceの持続時間が残り10秒を切った場合、自動的にバフを切る
#### Orb自動生成機能
PoKなど安全なゾーンにいるときに/bc necorbを実行することで、Marr神殿へ移動し、適当なダミーNPCを使って4000個のorbを生成する。/bc necorb 2000と指定した場合2000個生成する。

### Enchanter
#### 自動Second Spire使用
グループ内のManaの平均値が85を切った場合、Fundament: Second Spire of Enchantmentを使用する。
```
[Enchanter]
Second Spire=On
```
#### Mind Crystal自動召喚機能
戦闘外で6人でグループを組んでいるときに、Mind Crystalがインベントリにない場合自動的に再召喚する。

#### Project Illusion使用機能
Buff=で、/projectを指定すると、指定した呪文を詠唱する前にProject Illusionを使用する

### Wizard
#### Pyromancy, Cryomancy, Arcomancy使用機能
Auto Skill=で、以下の設定を行うことで指定したスキルを使用する前にPyromancy, Cryomancy, Arcomancyをバフとしていれる。
```
/Fire: Pyromancyを詠唱する
/Cold: Cryomancyを詠唱する
/Magic: Arcomancyを詠唱する
```

## その他
### アイテムの自動格納
カーソルにアイテムがある場合、呪文、アイテムやスキルの使用にゲームのシステム上大きな制限がくわわる。場合によっては、LDする場合があるため、E4ではカーソルに30秒以上アイテムがある場合、自動的に袋にアイテムを格納する。ただし、MAGのマナ棒、オーブ、ENCのMind Crystalは30秒を待たずに自動的に格納する。もしこれらのアイテムを他のキャラクターに渡したい場合は/bc pauseを実行すること。90秒間自動格納機能が停止する。

### 自動瞑想
キャラクターはマナやEnduranceが95%を下回ると自動的に座る。フォローしていたり、戦闘中の近接、弓などによる遠隔攻撃中では座らない。自動的に有効になる。明示的に無効にする場合は以下の設定を行う。戦闘中にmanastoneを使用する設定を行っている場合、戦闘中の自動瞑想は無効となる。
```
[Misc]
Disable Auto-Medi(On/Off)=On
```
### 自動受け渡し
EQBCSに接続されたキャラクター同士であれば、トレードは片方がトレードボタンを押せば相手は自動的に押してくれる。EQBCS外のキャラクターで自動受け渡し機能を有効にしたければ、iniファイルに以下の設定を行う
```
[Misc]
Trader=On
```
### 自動CoH
LDoNのクリア報酬であるCoHアイテムを使ってキャラクターを呼び出すマクロ。

**/bc LazCoH**

グループ全員を実行者のいる場所にCoHで呼ぶ

**/bc CoHme toonname**

コマンド実行者をtoonnameがCoHする
### マナストーン自動使用
manastoneとcorrupted manastoneを持っている場合、自動的に使用する。戦闘中でも使う指定をしたい場合は、以下の設定を行う。戦闘中にマナストーンを使用する設定にしている場合、自動瞑想は無効となる。
```
[Misc]
Combat Manastone (On/Off)=On
```
マナストーンの使用を一時的に止めたい場合は以下のコマンドを使用する

**/bc pause**

90秒間マナストーンの使用を停止する
### 自動食事
食料と飲み物を自動的に取る機能。以下の食料と飲み物を自動的に使用する。上から順番に消費していく

**食べ物**
- Summoned: Black Bread
- Mushroom
- Roots
- Rabbit Meat
- Habanero Pepper
- Berries
- Loaf of Bread

**飲み物**
- Summoned: Globe of Water
- Glob of Slush Water
- Pod of Water
- Bottle of Milk
- Water Flask

### 飛び道具切り替え
自動的に設定された弓を装備し、矢を打つ機能。設定は以下
```
[Misc]
Stats Ranged=Totem of the Chimera
Ranged=Bow of Storms
Ammo=Mithril Champion Arrows
```
Stats Rangedは普段装備する飛び道具出ないステータスアイテム。Rangedはpullに使う飛び道具、Ammoはその弾を設定する。弓をうつコマンドは/bc pullmob。Ammoを設定しない場合は、CLASS 2 Wood Point Arrowを自動的に装備する。
自動自動戦闘を使う場合は、この機能を有効にする必要がある。
### 集合
戦闘終了時や、コマンドによる任意のタイミングにおいて、設定したキャラクターにコマンドを実行したキャラクター以外を集合させる機能。

**/bc Anchor toonname**

toonnameというキャラクターに集合させる。

**/bc Anchor**

設定されたキャラクターに集合させる。

**/bc Anchor off**

集合機能を無効にする
### 装備交換
Procでバフを発動させる武器をバフが入るまで装備する機能。
```
[Misc]
Main Weapon=normalweaponname
Main Shield=normalsheildname
Proc Weapon=procweaponname
Proc Shield=procsheildname
Proc Name=procspellname
Proc Name2=procspellname
```
両手武器を装備している場合は、Main ShieldあるいはProc Shieldを指定しないで、コメントアウトするか、最初からエントリを書かないでおく。procspellnameには発動するバフの名前を設定する。
### 自動採集
自動的にForageスキルを使用する。インベントリが一杯であるときにはForageは実行しない。
```
[Misc]
Auto-Forage (On/Off)=On
```
### レイドアラート
Lazarusで重要だと思われるアラートのみE3から移植。AnguishのOMMに関しては、スクラッチから作り直し。
#### Asylum of AnguishでのMirrored Mask自動装備
Anguish内では、mirrored maskを持っている場合自動的に装備する。Anguish外へ移動した場合、設定されたマスクを装備する。
```
[Misc]
Mask=Chailak Hide Mask
```
上記の設定をしている場合、Anguish外へゾーンした場合、Chailak Hide Maskを装備する。
### 自動アイテム拾い
E3と同等の機能。フォーマットはE3と同じ。以下の設定をキャラクターiniで設定することで有効になる。
```
[Misc]
Auto-Loot (On/Off)=On
Loot While Combat(On/Off)=Off
Silent Loot (On/Off)=Off
```
Auto-Loot (On/Off)=Onにすることで自動アイテム拾いが有効になる。offにするとアイテムは自動的に拾わない。Loot While Combat(On/Off)=をOnにすることで戦闘中でもアイテム拾いを行う。グループを組んでいるときにリーダーでかつEverquestのクライアントウインドウがフォアグラウンドにある場合は自動アイテム拾い機能は有効にならない。Silent Loot (On/Off)=をOnにすることで、拾えないアイテムを報告しなくなる。
デフォルトでe4_globalsettings/Loot Settings.iniで設定。以下の設定で別のファイルを指定することができる。
```
[Misc]
Loot Settings=otherlootsettings.ini
```
別ファイルもe4_globalsettings/下に置くこと

#### 自動アイテム拾いの制御
以下のコマンドでlootの制御を行う。

**/bc autoloot**

グループ内で、キャラクターiniでAuto-Loot=Onと設定されているキャラクター全員が周囲にあるアイテムを拾おうとする。戦闘中でも拾う。

**/bc autoloot off**

コマンドを実行したキャラクターの自動アイテム拾い機能を無効にする。

**/bc autoloot on**

コマンドを実行したキャラクターの自動アイテム拾い機能を有効にする。

**/bc autoloot botname1 botname2...**

botnameの部分にEQBCSに接続されたキャラクター名を設定する。Auto-Loot=Offであったとしてもこのコマンドで一度だけアイテム拾いを行う。

**/bc pause**

このコマンドを実行者は、死体を再表示し、透明状態を解除し、マナストーンの使用と自動アイテム拾い機能を90秒間無効にする。自動バフ機能も30秒間停止する。e4の機能を一時的に停止し、なにか特別に操作をしたいときに使用する。

#### 自動アイテム拾いフォーマット
フォーマットはE3と同じ。Keep指定でアイテムを拾い、Skip指定でアイテムは拾わない。Sell指定で、autosell時に販売。Destroy指定で拾うときとautosell時に破壊。E4では、さらにBreakを指定できる。Breakを指定した場合、autosell時には破壊しないが、拾う時に破壊する。Skip指定時にさらに/beepがあると音をならす
```
[U]
U is for=
Unkempt Wristbands of Apathy (L)=Keep
Undead Froglok Tongue 8p1c(1000)=Keep|1000,Sell
Uncut Rubellite 1p(1000)=Break|1000
Uncut Demantoid 1p(1000)=Skip|1000
Used Parchment (1000)=Destroy|1000
```

### 販売・自動破壊
/bc autosellで、グループ全体が近くのベンダにアイテムを売ろうとする。何を売り、何を破壊するかはE3の自動販売、自動破壊と同じ。
### 蘇生待ち
死んだのちに蘇生を受けると自動的に蘇生を受託し、死体を拾う。
### 自動Shrink
AAのGroup Shrinkを持っている場合、グループ全体を自動的に小さくする。
```
[Misc]
Auto-Shrink (On/Off)=On
```

### 自動馬降り機能
以下の設定を行うことで、戦闘中馬から自動的に降りる
```
[Misc]
Mount Name=Ornate Flying Carpet
Mount (On/Off)=On
```

### Lucky Coin自動使用
Hardcoreゾーンで手に入るLucky Coinを自動的に使用する機能。/bc flip coinで実行する。

### NPCアイテム自動渡し機能
NPCにアイテムを連続で渡す機能。Faction上げに便利。NPCをターゲットした状態で以下のコマンドを入力。

**/bc xx giveitem itemname**

xxに一度にわたすアイテムの数を設定。最大4。itemnameに渡すアイテムを設定。

### 自動tome of new beginningクエストアイテム渡し機能
Tacviで拾えるWritingsをAbysmal SeaにいるNPCに渡す機能。NPCの距離9以内に立ち、以下のコマンドを実行することで、必要な本を自動的に渡す。すべての本を渡したあと、De'Van Szostekの近くに立ち、再度このコマンドを実行することで、報酬も受け取れる。

**/bc tacvibook**

### 自動Focus of Arcunum使用機能
DRU,WIZ,MAG,ENC,NEC,SHMは、自動的にFocus of Arcanum, Empowered Focus of Arcanum, Enlightened Focus of Arcanum, Acute Focus of Arcanumを使用し、常にいずれかのバフが有効な状態となる。

### 自動シンボル交換機能
PoKのPoP/GoD Symbol商人にTime, GoDのアイテムを渡してシンボルを得る機能。NPCの近くで以下のコマンドを実行

**/bc symbol**

なにを渡すべきかは、/e4_globalsettings/donate.iniに記述。

### 自動アイテム寄付機能
PoKのEnva、Guild HallのMelodyに寄付したいアイテムを渡す機能。NPCの近くで以下のコマンドを実行

**/bc donate**

コマンドの実行者がアイテムを登録されたアイテムを寄付していく。登録されたアイテムはe4_globalsettings/donate.iniに記述。寄付したいアイテムの追加は、アイテムを手に持った状態で以下のコマンドを実行。
**/bc adddonate**

### 1グループレイド作成機能
1グループができている状態で以下のコマンドを実行すると1グループのレイドを作成する。レイドグループが必要なコンテンツで有効

**/bc makeraid**

### Gem8スロットにデフォルトの呪文をセットする機能
e4ではgemスロットの指定がなかったときにgemスロット8を使って呪文の記憶を行う。デフォルトでは最後に記憶をした呪文が設定されるが、以下の設定をしておくことで、戦闘外でバフを必要がないときに設定された呪文の記憶を行う。再使用が早いあまり重要でない呪文を設定しておくと、gemスロットをひとつ有効に活用できる。PoKやNexusなどの安全なゾーンにいるときは機能しない。
```
[Misc]
Default Gem8 Spell=Necrotic Pustules
```

### ヘイトトップになると設定されたスキル・アイテムを使用する機能
フォローをしていないときで、戦闘中において、XTarget内のmobが自分を攻撃しているときに使うスキルを設定することができる。HPが80%を切ると使用する。
```
[Misc]
Hate Clear Spell=Inconspicuous Totem
```

### 自動Ancient Gods変身使用
自動的にMetamorph Totem: Ancient Godsを使用し、ペットを指定の神に変身させる。この機能が有効なのはMAG, BST, NEC, SHD, SHM, ENCのみ。自分のマナが60%を超えているときのみ有効
```
[Misc]
Pet God Illusion=Rallos Zek
```
指定できる神の種類は複数設定可能（例: PetGod Illusion=Rallos Zek Tunare）。どの種類でもよいのであれば、anyを指定すること。
#### 指定できる神の種類
- Erollisi
- Bristlebane
- Innoruuk
- Cazic Thule
- Rallos Zek
- Solusek Ro
- Tribunal
- Tunare

### 自動Tome of Nife's Mercy使用
グループ内のManaの平均値が85を切った場合、Tome of Nife's Mercyを使用する。

### 自動死体召喚機能
以下のコマンドで、PoKにいる自分が死んでいる状態でかつ150pp以上を持っているキャラクターは「a priest of luclin」まで移動し、自身の死体を召喚する。

**/bc summonmycorpse**

## 生産
生産系の便利マクロ集
### アイテムバラマキ機能
コマンドの実行者が指定した数のアイテムをグループにくばる機能

**/bc number scatter xxxx**

numberに数、xxxxにアイテム名を指定する。例えば/bc 100 scatter soul orbと実行すると、グループメンバー全員に100個ずつSoul orbを配る。

### アイテム集め機能
コマンドの実行者に対してEQBCSに接続された近くの人が指定したアイテムを渡す機能

**/bc gather xxxx**

**/bc yyyy gather xxxx**

xxxxにアイテム名を指定する。pp、platinum、platを指定することで、プラチナ貨を集める。all dcと指定することで、alternative currencyに入っているdiamond coinを集める。yyyyにキャラクター名を指定することで、そのキャラクターからのみアイテムをあつめる。

### アイテムリスト集め機能
前もってe4_glabalsettings/collection.iniに記述したアイテムのリストをコマンド実行者にわたすコマンド。collection.iniは以下のようなフォーマット
```
[gems]
item=Blue Diamond
item=Diamond
item=Jacinth
item=Black Sapphire
item=Ruby
item=Sapphire
item=Star Ruby
item=Fire Emerald
item=Fire Opal
item=Opal
item=Black Pearl
```

**/bc collect gems**

gemsという項目にitem=で設定されたアイテムを集める。倉庫キャラにアイテムを一気に渡すときに便利。

### 自動コンバイン
生産ウインドウが開いている状態で、以下のコマンドを実行することで、材料がなくなるか、袋がほぼ一杯になるか、指定したスキルに達するまでコンバインをし続ける。

**/bc xxxx AutoCombine yyyy**

xxxxには終了するスキルレベルを指定する。yyyyには生産のスキルの名称を指定する。例えば、鍛冶スキル150まで上がったらコンバインを停止する場合は、/bc 150 AutoCombine blacksmithと指定する。仮にスキル値関係なく、ある材料全部コンバインする場合はスキル値を1000や999など達成できない数字に設定する。
### 自動エンチャント
ENCなどで、材料をエンチャントするのを自動化する。材料がなくなるまで続ける。コマンドは以下の通り。

**/bc CreateMats spell_name**

spell_nameに呪文の名前あるいはgem番号を指定する。スペルgemに前もって設定されている必要がある。
### 自動手動コンバイン
上の自動コンバインと動作は似ているが、生産ウインドウではなく、通常の袋などのコンバインボタンを利用するもの。生産ウインドウのない一部の生産品を使ってスキル上げをするためや、こちらのマクロのほうが圧倒的に早いため、生産品を一度に大量に作るときに有用。
e4_globalsettingsディレクトリ下にtradeskill.iniに材料などの情報を前もって記載しておく。
```
[Fletching4sub1]
Products=2
Skill Name=Fletching
Raise Until=500
Mats1=Mithril Working Knife
Mats2=Small Brick of Mithril
```
上記はFletchingでRNGの呪文の触媒を作るための例。Skill Name=に生産スキル名。Raise Until=に向上させたいスキル値を設定。もしスキル値に関係なくコンバインを続けたい場合は、300より大きな値を設定しておく。Products=には一回のコンバインでできるアイテムの種類を書く。この例では、最終生成物の他にMithril Working Knifeが戻ってくるため、2と指定している。Products=は設定しない場合は1として扱われる。MatsX=には材料を設定する。
これを設定した状態で、インベントリの10番目のスロットに生産用の袋をセットし、以下のコマンドで生産を開始する

**/bc StartCombine Fletching4sub1**

これにより、スロット1から9までの袋にある材料をスロット10へ写し、コンバインし、生成物を袋につめ、というのを繰り返す。材料がなくなるか、指定したスキル値まで上がるとマクロは終了する。
### 自動釣り機能
袋の中にfishing rodとFishing Baitを持った状態で、と魚がつれる水辺に立ち、以下のコマンドを実行することで延々と釣りを行う。終了させたい場合は同じコマンドを実行する。

**/bc control fishing**

自動食事機能は働かないため、効果な食料と飲料は銀行にしまっておくこと。また、釣れたもので価値のないものは自動的に破壊するため、このマクロを実行している最中に、手動で装備品やアイテムをもたないこと。
### 自動言語習得機能
前もって上げたい言語を設定し、以下のコマンドを実行することで定期的に設定した言語をしゃべる機能。

**/bc control language**

この機能を有効にしているときは基本的になにもしないこと。機能を停止するためには、/mac e4を登録したホットボタンを押す。
### 自動アルコール耐性向上機能
袋にHoney Meadを3000個ほどセットし、以下のコマンドを実行。定期的に酒を飲む。

**/bc control booze**

同じコマンドを再実行することで機能を停止する。
### 自動物乞いスキル向上機能
セーフゾーンで、NPCをターゲットして以下のコマンドを実行。定期的に物乞いを行う。

**/bc control beg**

8時間ほど放置して、60pp-100ppほど手に入る。そこそこ儲かる。電気代とは釣り合わない。

### 自動スリスキル向上機能
セーフゾーンで、NPCをターゲットして以下のコマンドを実行。定期的にスリを行う。

**/bc control pickpocket**

### AFKゾーン回避機能
Plane of KnowledgeとTemple of Marrで一定時間いる場合、AFKゾーンに飛ばされるが、それを避けるための機能。
```
[Misc]
Avoid AFK (On/Off)=On
```

## モジュールの追加のしかた
e4はe3と同様自作のモジュールを追加できる。template.incがモジュールのテンプレート。
```
SUB template_Setup
    /call Core_AddTask_LowMain template_LowMain
    /call Core_AddTask_ZoneChange template_ZoneChange
    /call Core_AddTask_MobSlain template_MobSlain
	/call Core_AddTask_EndCombat template_EndCombat
/RETURN

SUB template_Main
/RETURN

SUB template_LowMain
/RETURN

SUB template_ZoneChange
/RETURN

SUB template_MobSlain
/RETURN

SUB template_EndCombat
/RETURN
```
自作のモジュールにする場合は、上記の"template"部分を自分のモジュールの名前(以下xxxx)に変更すること。以下、各サブルーチンの説明。
- xxxx_Setup: マクロ起動時に一度だけ実行されるサブルーチン
- xxxx_Main: 毎手番時に呼び出されるルーチン
- xxxx_LowMain: 1.5秒ごとに呼び出されるルーチン
- xxxx_ZoneChange: ゾーンがかわると呼び出されるルーチン
- xxxx_MobSlain: mobが倒されるごとに呼び出されるルーチン
- xxxx_EndCombat: 戦闘が終わると呼び出されるルーチン

モジュールのe4への追加はcore.incを修正することで行う。
```
#include e4_codes\class.inc
#include e4_codes\trade.inc
| ********** add new module inc path here! **********
#include e4_codes\xxxx.inc

	/call ArrayAdd core_TaskMain Trade
	/call ArrayAdd core_TaskMain Basic
| ********** add new module name here! **********
	/call ArrayAdd core_TaskMain xxxx
```
上記のincludeとarrayaddに追加したいモジュールを追記する。

## 拡張機能
以下の機能は強力すぎるため、最終的なリリースでは取り外す予定。

### 自動移動機能
PoKのゲートアウト地点を起点にして、ベンダへのアイテム売り、tributeベンダへの寄付、Valiumを利用したゾーン移動を自動化できる。移動はグループ単位で行われる。使い方は以下の通り

**/bc autodonate**

MB内にいるTribute商人まで移動し、自動的に/bc donateコマンドを実行し、その後ゲートアウト地点まで移動する

**/bc zone xxxx**

xxxxにゾーン名を指定。Valium、Mavis、Magusまで移動し、指定したゾーンへ移動する。xxxxで指定するゾーン名は、NPCに指定するゾーン名と同じ。

**/bc zoneme xxxx**

/bc zoneと同じだが、グループにいるときはコマンドの実行者のみ移動する

**/bc vendor**

PoK内のベンダまで移動し、自動的に/bc autosellコマンドを実行する

**/bc marrdaily**

Marr神殿のGuard Queztinまで移動し、mpgのトライアルを巡るデイリークエストをうける

**/bc allgo**

現在ターゲットしている対象にグループ全員で移動する

**/bc igo**

/bc allgoと同じだが、グループにいるときはコマンドの実行者のみ移動する

### LDoN自動移動機能
PoKのゲートアウト地点を起点にして、指定したテーマのLDoNのキャンプ場所へ移動し、クエストを受けて、ダンジョンに入るところまでを自動化する。使い方は以下の通り、
#### LDoNグループ
**/bc ldon [tak|mir|ruj|mmc|guk] [hard|normal] [singleboss|collection|mobcount]**

#### LDoNレイド
**/bc ldon raid[a|b] [tak|mir|ruj|mmc|guk]**
- raida tak：Takish-Hiz : Within the Compact
- raida mir：Miragul's Menagerie: Frozen Nightmare
- raida ruj：Rujarkian Hills: Hidden Vale of Deceit
- raidb ruj：Rujarkian Hills: Prison Break
- raida mmc：Mistmoore's Catacombs: Struggles within the Progeny
- raida guk：Deepest Guk: The Curse Reborn
- raidb guk：Deepest Guk: Ritualist of Hate

e4_globalsettings/travel.iniでデフォルトの設定を変更することができる。コマンドライン上で、hardあるいはnormal、ミッションの種類を設定しない場合、travel.iniで設定されているものを使用する。
```
[travel Settings]
Risk (Normal/High)=Normal

[travel AdventureType]
Takish (collection/mobcount/singleboss)=collection
Miragul (collection/singleboss)=collection
Rujarkian (collection/mobcount/singleboss)=mobcount
Mistmoore (collection/singleboss)=singleboss
Guk (singleboss)=singleboss
```

### Idol of the Under King使用
グループの平均HPが96%を下回ったときに自動的にIdol of the Underkingを使う機能。有効にするために以下の設定を行う。Auto-Idol Heal Pct=はヒールを行うグループの平均HP量を示す。この設定をしなかった場合、96%に設定される。
```
[Zuru]
Auto-Idol(On/Off)=On
Auto-Idol Heal Pct=90
```
### Point Blank Shoot
設定は以下。Ranged Weapon Delayは弓を撃ち始めてから撃ち終わるまでの期間。Next Shoot Intervalは弓を撃ち終わって、次に近接攻撃を行うまでのインターバル。これらの設定は1ごとに100ms。10で一秒設定。
```
[Zuru]
Ranged Weapon Delay=30
Next Shoot Interval=3
```
pbshootを効率よくつかうためには、キャラクターの使っている近接武器、遠隔武器の性能、ヘイスト等によって設定を変更する必要がある。

**/bc testshoot xx yy**

xxにはNext Shoot Intervalを設定、yyにはRanged Weapon Delayを設定する。このコマンドによって、近接→遠隔→近接→遠隔という順番で攻撃を行う。これで効率よくDPSがでているかどうかをチェックする。最終的に求められた値をiniファイルに設定する。

### 自動死体隠蔽機能
/bc autohidec xxで、xxに数字を指定する。xxは100ms単位。例えば600と指定した場合、60秒に一回グループ全員が/hidec allを実行する。xxを指定せず/bc autohidecだけにすれば30秒に一回死体を隠蔽する。
### クールダウンAA・Item使用機能
戦闘中に有害な呪文を詠唱したのち、所持品にMolten Orbがあれば、スペルの詠唱後のグローバルクールダウン最中にこのアイテムを使おうとする。WIZの場合、Force of Will、DRUの場合、Storm Strikeが使えるのであれば使おうとする。NECの場合、Miragul's Greaves of Risen SoulsあるいはDemi Lich Skullcapを持っていれば使おうとする。ENCの場合、Mindreaver's Vest of Coercionを持っていれば使おうとする。
戦闘中に有益な呪文を詠唱したのち、キャスター、ヒーラークラスはSigil Earring of Veracityあるいは、DRUとRNG以外でGirdle of Living Thornsを持っている場合、このアイテムを使おうとする。
### Catacomb C Expedition自動受領機能
RCoDのexpeditionを受けられるNPCの近くで/bc taskcatacombcを実行することで、Catacomb Cのexpeditionを受ける。

# readme.mdの更新履歴
- 2021/05/25: 初版 v0.1
- 2021/05/26: /attackを/combatに置き換え。起動ファイルはe4.macではなく、e3.macであることを明記。自動言語スキル上げ機能を搭載。ペット武器を渡す機能を追加。
- 2021/05/27: /Gem機能を追加
- 2021/05/28: 戦闘時のスキル使用で/ReBを追加。拡張機能の説明を追記。song=に/raid機能を追加
- 2021/05/30: バードでmischeifを使う楽器の使用方法を変更した。/bodyを追加
- 2021/06/06: v0.2。Auto Idolで実行時のグループHP平均を設定可能に。
- 2021/06/09: /groupedを追加
- 2021/06/11: 生産マクロ追加、ペットのバフを消す機能を追加、ペットのバフを自動的に消す機能を追加
- 2021/06/13: BuffとHealに/NotDeadを追加
- 2021/06/15: /bc equipを追加。WARでAC上げバフを自動的に使う機能を無効にできるようにした。class特有の機能のiniはすべて[クラス名]という項目に統一した。buffに/endbelowを追加。/Disc指定を追加
- 2021/06/16: 戦闘で、近接攻撃方法と移動方法を分離した。レンジャーで自動矢バフ機能を追加した
- 2021/06/19: noteから移行
- 2021/06/20: auto-skillで/rebを廃止、かわりに/Rcheck1を追加。戦闘マクロでattack type、stickを設定するコマンド/bc stanceを追加
- 2021/06/24: stickのrangedを使いやすくした
- 2021/07/05: gatherとscatterコマンドを追加
- 2021/07/06: collectコマンドを追加
- 2021/07/10: autohidecコマンドを追加。バーンとコマンドリストの実行優先度を変更。attack typeに"AE"を追加。
- 2021/07/11: haltコマンドを追加
- 2021/07/12: pausemanastoneコマンドを追加。pauselootコマンドを追加。ルート機能に関して記述
- 2021/07/14: v0.4。ToggleAEコマンドを追加。自動ヒールに/Forceを追加
- 2021/07/16: Ranger用にarrowコマンドを追加
- 2021/07/20: 装備交換機能を変更
- 2021/07/21: Abashi rod自動使用機能を追加
- 2021/07/22: モジュールの追加のしかたを追加
- 2021/07/23: NECのHPマナ変換バフをHPが20%を切っているときに自動的に消す機能を追加
- 2021/07/24: Anguish内に入ると自動的にマスクを装備する。外へでると設定されたマスクに装備し直す機能を追加
- 2021/07/26: StartCombineコマンドを改善。auto-skillで/Tributeを追加
- 2021/08/01: 自動受け渡し機能でEQBCS外のキャラクターでも行えるようにした。Healに/Tributeを追加
- 2021/08/05: CLRにRezOnceコマンドを追加
- 2021/08/08: flip coinコマンドを追加。ENCでも自動Abashiを使えるようにした。
- 2021/08/12: 任意のloot settingsファイルを指定できるようにした
- 2021/08/15: proc weaponの設定方法を変更。giveitemコマンドを追加
- 2021/08/16: 自動Yaulpを追加。catacombcのexpeditionを自動的に受けるコマンドを追加。Divine Avatar自動使用機能を追加
- 2021/08/17: 自動focus of arcanum機能を追加
- 2021/08/21: follow allコマンドを追加
- 2021/08/22: buffやhealの/whoでクラスを指定するときはクラス名の前に@をつける(例、@WAR @PAL)
- 2021/08/29: 自動でtributeポイントへ変換する寄付機能を追加した(Yudedako氏作成)
- 2021/08/31: 自動スキルで/Trapを追加。/Dampenを追加。gatherにppとdcを集める機能を追加(Yudedako氏作成)
- 2021/09/05: makeraidコマンドを追加
- 2021/09/06: 自動buffと自動skillで/Reagentを追加(Yudedako氏作成)
- 2021/09/13: 自動Healに/donateを追加
- 2021/09/16: Berserkerに自動斧召喚機能を追加(Yudedako氏作成)。自動pickpocket訓練機能を追加
- 2021/09/18: /Reagentに数量を指定できるようにした。LDoNTravelerを追加(Yudedako氏作成)
- 2021/09/20: /Orbを追加
- 2021/09/23: pausemanastone、pauselootを廃止。/bc pauseで両方の機能を提供するようにした
- 2021/09/24: /gomarrを追加。/bc AE Rezを/bc AErezに変更。/bc AErez itemで蘇生棒をつかうようにした。spread機能追加
- 2021/09/25: travel ldonの機能を向上。Gather all dcを追加
- 2021/09/28: healに/combatと/notfollowingを追加
- 2021/09/30: gemスロット8にデフォルトの呪文を設定する機能を追加
- 2021/10/02: ヘイトトップのときに設定されたスキルを使用する機能を追加、/porttoを拡張
- 2021/10/03: NECはグローバルクールダウン中に、Sent of Terris、Encroaching Darknessを使用する
- 2021/10/10: ROGのauto-evade, auto-hide, sneak attackの設定方法を記述
- 2021/10/12: Auto-Skillにおいていくつかパラメータの名称を変更した。/MHPBelow, /MHPOver, /MLvBelow, /MLvOverは/MobXXXXに変更した。/SecondOver, /SecondBelowも/Hate2ndOver, /Hate2ndBelowに変更
- 2021/10/19: 自動buffと自動healに/onlyと/onlymeを追加。auto-skillに/avehpbelowを追加
- 2021/10/23: /queueを追加
- 2021/11/01: EQクライアントの設定を追加
- 2021/11/06: SHMはLanguid biteを自動的に使う
- 2021/11/07: v0.5
- 2021/11/08: Auto-Skillに/stackableを追加
- 2021/11/17: travelコマンドを三種追加。/loglog廃止、/bc waypoint writeを追加
- 2021/11/21: 呪文バラ撒き機能(Spread)を廃止。かわりにAuto Skillに/cycleを追加
- 2021/12/16: Auto-Skillに/Abashiを追加。自動Abashi機能を廃止
- 2021/12/17: Abysmal SeaでWritingsを自動的に渡す機能を追加
- 2021/12/24: 自動自動戦闘系の記述を削除
- 2021/12/28: 自動ペット装備渡し機能を追加
- 2021/12/29: 自動molten orb召喚機能、molten orbをグループに配る機能を追加
- 2022/01/01: travelコマンドを簡略化した。グローバルクールダウン中の自動行動に関して追記
- 2022/01/08: 自動buffとAuto-skillに/mgbを追加。EnchanterのMind Crystal自動召喚機能を追加。gatherに個人を指定した。
- 2022/01/16: non-tank=を追加
- 2022/01/23: BRDの自動演奏にmoblvを追加
- 2022/01/28: WIZはauto skill使用前にpyro, cryo arcoバフをいれる機能を追加
- 2022/01/31: ENCのbuff=に/projectを追加。自動アイテム拾いにbreakを追加
- 2022/02/05: 自動buffに/tributeを追加
- 2022/02/13: MAG棒は戦闘中でも使用する。クールダウン中に使うアイテムを変更
- 2022/02/21: ターゲットをとらないNoTargetを自動バフと自動ヒールに追加
- 2022/02/26: stickにKeepを追加
- 2022/03/16: symbolコマンドを追加
- 2022/03/26: v0.6 E3付属のMacroquest2では最終バージョン。以降は最新版のMacroquest2のみサポート
- 2022/04/11: 自動buffに/safeを追加
- 2022/05/02: MAGとENCの自動召喚の仕様を変更。BRDの自動アイテムクリッキーの説明を追記
- 2022/05/05: 自動的に馬を降りる機能を追加
- 2022/05/14: ペットを旧神へ変身させるアイテムの自動使用をサポート
- 2022/05/28: 自動バフ、ヒール、戦闘に/raid, /notraid, /notributeを追加。Rangerで自動Guardian of the Forest使用機能を追加
- 2022/06/04: Shamanで自動的にInconspicuous Totemを使うauto totem機能を追加
- 2022/06/08: Druidの自動Spirit of the black/white wolfの仕様を変更
- 2022/06/11: aerezの仕様を変更
- 2022/06/13: rezonceを削除
- 2022/06/22: attack typeからAEを削除
- 2022/08/14: auto-skillに/buffを追加
- 2022/08/25: 自動スキルに/caster、stick typeにsee、自動canniで/calm、/combatを付与できるようにした
- 2022/08/28: 自動buffに/unsafeを追加
- 2022/08/30: 自動book of nife使用、自動ENC second spire使用を追加
- 2022/09/02: 自動AFKゾーン回避機能を追加
- 2022/09/06: 自動buffに/hotを追加、自動スキルに/notnamedを追加
- 2022/09/08: 自動スキルに/faceを追加
- 2022/09/18: /bc summonmycorpseを追加, Reliable廃止、Trader=追加
- 2022/09/25: ペットタンクの説明を追記、/bc allgoと/bc igoを追加
- 2022/10/04: /stunをauto skillに追加
- 2022/10/08: /meleeをauto skillに追加
- 2022/10/12: 自動healに/safeと/unsafeを追加
- 2022/10/22: 自動ポーション使用機能を追加
- 2022/10/25: Bardの自動ソングに/dcheck1を追加
- 2022/11/05: 自動buff, heal, skillに/ispetを追加
- 2022/11/07: NECでEmbalmer's Carapace自動使用機能を追加
- 2022/11/21: 自動second wind機能を更新。WAR, ROGにも機能を追加
- 2022/11/24: ROGのThief's Eyes, MNKのFists of Wu, BERのCry Havoc自動使用機能の廃止
- 2022/11/25: クールダウン中のアイテム・AA自動使用を更新
- 2022/12/05: NECのオーブ自動生成機能を追加
- 2022/12/29: 自動buffにhate関連、Named関連を追加
- 2022/12/30: 自動lootにbeepを追加
- 2023/01/07: pullmobコマンドに飛び道具使用後に戻すステータスアイテムを設定可能にした
- 2023/01/14: 自動lootでsilent lootを追加
- 2023/01/26: MQnextでのみ動作する
- 2023/04/09: 自動馬降り機能を拡張
- 2023/04/19: Bardの自動ソングに/Forceを追加
- 2023/05/22: Bardの自動ソングに/calmを追加
- 2023/08/08: /fightを追加
- 2023/08/13: stickタイプにcowardを追加
- 2023/08/21: 完全自動ヒール機能を追加
- 2023/09/26: グループを組み直すコマンドを追加
