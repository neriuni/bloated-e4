SUB Cure_Setup

    | Control

    | Parameter

    | Alias

    | Internal
    /declare cure_Combat bool outer FALSE

    /declare cure_Enable bool outer FALSE
    /declare cure_SelfEnable bool outer FALSE
    /declare cure_curep bool outer FALSE
    /declare cure_cured bool outer FALSE
    /declare cure_curec bool outer FALSE
    /declare cure_curea bool outer FALSE
    /declare cure_curesp bool outer FALSE
    /declare cure_curesd bool outer FALSE
    /declare cure_curesc bool outer FALSE
    /declare cure_curesa bool outer FALSE

    /call iniToVarV "${CharacterIni},Cures,All" cure_All string outer
    /if (${Macro.Return}) {
        /varset cure_Enable TRUE
        /varset cure_curea TRUE
        /call Skill_CreateData CUREALL "${cure_All}"
    }
    /call iniToVarV "${CharacterIni},Cures,Poison" cure_Poison string outer
    /if (${Macro.Return}) {
        /varset cure_Enable TRUE
        /varset cure_curep TRUE
        /call Skill_CreateData CPOISON "${cure_Poison}"
    }
    /call iniToVarV "${CharacterIni},Cures,Disease" cure_Disease string outer
    /if (${Macro.Return}) {
        /varset cure_Enable TRUE
        /varset cure_cured TRUE
        /call Skill_CreateData CDISEASE "${cure_Disease}"
    }
    /call iniToVarV "${CharacterIni},Cures,Curse" cure_Curse string outer
    /if (${Macro.Return}) {
        /varset cure_Enable TRUE
        /varset cure_curec TRUE
        /call Skill_CreateData CCURSE "${cure_Curse}"
    }
    /call iniToVarV "${CharacterIni},Cures,Self" cure_Self string outer
    /if (${Macro.Return}) {
        /varset cure_SelfEnable TRUE
        /varset cure_Enable TRUE
        /varset cure_curesa TRUE
        /call Skill_CreateData CSSELF "${cure_Self}"
    }
    /call iniToVarV "${CharacterIni},Cures,Self Poison" cure_SelfPoison string outer
    /if (${Macro.Return}) {
        /varset cure_SelfEnable TRUE
        /varset cure_Enable TRUE
        /varset cure_curesp TRUE
        /call Skill_CreateData CSPOISON "${cure_SelfPoison}"
    }
    /call iniToVarV "${CharacterIni},Cures,Self Disease" cure_SelfDisease string outer
    /if (${Macro.Return}) {
        /varset cure_SelfEnable TRUE
        /varset cure_Enable TRUE
        /varset cure_curesd TRUE
        /call Skill_CreateData CSDISEASE "${cure_SelfDisease}"
    }
    /call iniToVarV "${CharacterIni},Cures,Self Curse" cure_SelfCurse string outer
    /if (${Macro.Return}) {
        /varset cure_SelfEnable TRUE
        /varset cure_Enable TRUE
        /varset cure_curesc TRUE
        /call Skill_CreateData CSCURSE "${cure_SelfCurse}"
    }

    |/call IniToArrayV "${CharacterIni},Cures,Mate" cure_MateList

    /call Core_AddTask_LowMain Cure_LowMain

/RETURN

SUB Cure_Main
    /if (!${cure_Enable}) /return
    /call Cure_Check
/RETURN

SUB Cure_LowMain
    /doevents CureToggleCombat
/RETURN



#EVENT CureToggleCombat "<#1#> CombatCure #2#"
SUB EVENT_CureToggleCombat(line, chatSender, params)
    /if (${params.Find[on]}) {
        /echo Cure: Combat cure ** ENABLED **
        /varset cure_Combat TRUE
    } else /if (${params.Find[off]}) {
        /echo Cure: Combat cure ** DISABLED **
        /varset cure_Combat FALSE
    }
/RETURN

| not calling now
SUB Cure_MateCheck
    /if (${Following}) /return
    /if (!${cure_Enable}) /return

    /if (${cure_MateList.Size} > 0) {
        /declare i int local
        /for i 1 to ${cure_MateList.Size}
            /declare mateid int local
            /varset mateid ${Raid.Member[${cure_MateList[${i}]}].Spawn.ID}
            /if (!${Bool[${mateid}]}) /continue
            /if (${InCombat} && !${cure_Combat}) /return

            /call MobTarget ${mateid}
            /call combat_waitbuffspopulated
            /if (!${Macro.Return}) /continue

            /if (${Target.Cursed}) {
                /if (${Defined[cure_All]}) {
                    /call Skill_Cast_Data ${mateid} CUREALL -1
                    /if (${Macro.Return} == ${CAST_SUCCESS}) /return
                }
                /if (${Defined[cure_Curse]}) {
                    /call Skill_Cast_Data ${mateid} CCURSE -1
                    /if (${Macro.Return} == ${CAST_SUCCESS}) /return
                }
            } else /if (${Target.Poisoned}) {
                /if (${Defined[cure_All]}) {
                    /call Skill_Cast_Data ${mateid} CUREALL -1
                    /if (${Macro.Return} == ${CAST_SUCCESS}) /return
                }
                /if (${Defined[cure_Poison]}) {
                    /call Skill_Cast_Data ${mateid} CPOISON -1
                    /if (${Macro.Return} == ${CAST_SUCCESS}) /return
                }
            } else /if (${Target.Diseased}) {
                /if (${Defined[cure_All]}) {
                    /call Skill_Cast_Data ${mateid} CUREALL -1
                    /if (${Macro.Return} == ${CAST_SUCCESS}) /return
                }
                /if (${Defined[cure_Disease]}) {
                    /call Skill_Cast_Data ${mateid} CDISEASE -1
                    /if (${Macro.Return} == ${CAST_SUCCESS}) /return
                }
                /if (${Macro.Return} == ${CAST_SUCCESS}) /return
            }
        /next i
    }
/RETURN

SUB Cure_Check
    /if (${Following}) /return
    /if (${InCombat} && !${cure_Combat}) /return
    /call Skill_AmIReady
    /if (${Macro.Return} != ${CAST_SUCCESS}) /return FALSE

    /if (${cure_SelfEnable}) {
        /if (${NetBots[${Me}].Cursed}) {
            /if (${cure_curesa}) {
                /call Skill_Cast_Data ${Me.ID} CSSELF -1
                /if (${Macro.Return} == ${CAST_SUCCESS}) /return
            }
            /if (${cure_curesc}) {
                /call Skill_Cast_Data ${Me.ID} CSCURSE -1
                /if (${Macro.Return} == ${CAST_SUCCESS}) /return
            }
        }
        /if (${NetBots[${Me}].Poisoned}) {
            /if (${cure_curesa}) {
                /call Skill_Cast_Data ${Me.ID} CSSELF -1
                /if (${Macro.Return} == ${CAST_SUCCESS}) /return
            }
            /if (${cure_curesp}) {
                /call Skill_Cast_Data ${Me.ID} CSPOISON -1
                /if (${Macro.Return} == ${CAST_SUCCESS}) /return
            }
        }
        /if (${NetBots[${Me}].Diseased}) {
            /if (${cure_curesa}) {
                /call Skill_Cast_Data ${Me.ID} CSSELF -1
                /if (${Macro.Return} == ${CAST_SUCCESS}) /return
            }
            /if (${cure_curesd}) {
                /call Skill_Cast_Data ${Me.ID} CSDISEASE -1
                /if (${Macro.Return} == ${CAST_SUCCESS}) /return
            }
        }
    }

    /declare i int local
    /if (${Math.Rand[1]} == 0) {
        /for i 1 to ${Member.Size}
            /if (!${NetBots[${Member[${i}]}].InZone}) /continue
            /call cure_Cast ${i} 150
            /if (${Macro.Return}) /return
        /next i
    } else {
        /for i ${Member.Size} downto 1
            /if (!${NetBots[${Member[${i}]}].InZone}) /continue
            /call cure_Cast ${i} 150
            /if (${Macro.Return}) /return
        /next i
    }
/RETURN

SUB cure_Cast(int cureindex, int maxcount)
    |/echo Cure: ${Member[${cureindex}]} c ${NetBots[${Member[${cureindex}]}].Cursed} p ${NetBots[${Member[${cureindex}]}].Poisoned} d ${NetBots[${Member[${cureindex}]}].Diseased}
    /if (${NetBots[${Member[${cureindex}]}].Cursed} > 0 && ${NetBots[${Member[${cureindex}]}].Cursed} <= ${maxcount}) {
        /if (${cure_curea}) {
            /call Skill_Cast_Data ${NetBots[${Member[${cureindex}]}].ID} CUREALL -1
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
        }
        /if (${cure_curec}) {
            /call Skill_Cast_Data ${NetBots[${Member[${cureindex}]}].ID} CCURSE -1
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
        }
    }
    /if (${NetBots[${Member[${cureindex}]}].Poisoned}) {
        /if (${cure_curea}) {
            /call Skill_Cast_Data ${NetBots[${Member[${cureindex}]}].ID} CUREALL -1
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
        }
        /if (${cure_curep}) {
            /call Skill_Cast_Data ${NetBots[${Member[${cureindex}]}].ID} CPOISON -1
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
        }
    }
    /if (${NetBots[${Member[${cureindex}]}].Diseased}) {
        /if (${cure_curea}) {
            /call Skill_Cast_Data ${NetBots[${Member[${cureindex}]}].ID} CUREALL -1
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
        }
        /if (${cure_cured}) {
            /call Skill_Cast_Data ${NetBots[${Member[${cureindex}]}].ID} CDISEASE -1
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
        }
    }
/RETURN FALSE
