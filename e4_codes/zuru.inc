SUB Zuru_Setup

	| hide corpse in PoAir when loading e4
	|/squelch /hidec ALLBUTGROUP

    | Control
	/varset IsZuru TRUE

    | Parameter

    | Alias

    | Internal

	/call iniToVarV "${CharacterIni},Zuru,Melee Weapon Delay" Rmeleedelaybase int outer
	/if (!${Macro.Return}) {
		/declare Rmeleedelaybase int outer 2
	}
	/call iniToVarV "${CharacterIni},Zuru,Ranged Weapon Delay" Rshootdelaybase int outer
	/call iniToVarV "${CharacterIni},Zuru,Next Shoot Interval" Rnextshootbase int outer

	/call iniToVarV "${CharacterIni},Zuru,Auto-Idol(On/Off)" zuru_autoidol bool outer
	/if (!${Macro.Return}) /declare zuru_autoidol bool outer FALSE
	/call iniToVarV "${CharacterIni},Zuru,Auto-Idol Heal Pct" zuru_idolhealpct int outer
	/if (!${Macro.Return}) /declare zuru_idolhealpct int outer 96
	/call iniToVarV "${CharacterIni},Zuru,Auto-Idol Safe Mode(On/Off)" zuru_safemode bool outer
	/if (!${Macro.Return}) /declare zuru_safemode bool outer TRUE


	/call iniToVarV "${CharacterIni},Zuru,Auto-Poison Vial(On/Off)" zuru_autopoison bool outer
	/if (!${Macro.Return}) {
		/declare zuru_autopoison bool outer FALSE
	} else /if (${Bool[${FindItem[=Volatile Venom Vial].ID}]}) {
		/call Skill_CreateData MAKEPOISON "Volatile Venom Vial"
	} else /if (${Bool[${FindItem[=Bottomless Venom Vial].ID}]}) {
		/call Skill_CreateData MAKEPOISON "Bottomless Venom Vial"
	} else {
		/varset zuru_autopoison FALSE
	}


	/declare zuru_hidecen int outer -1
	/declare zuru_hidectime timer outer 0

	/declare NoHeroesFlag int outer -1

	| multi-group
	/declare zuru_zfollowflag bool outer
	/declare zuru_zfollowtarget string outer

	| pbshoot
	/declare pb_testshoot bool outer FALSE
	/declare pb_waitnextattack timer outer
	/declare pb_tchange timer outer

	/declare pb_shootcount int outer
	/declare pb_shootswing bool outer

	| aftercast
	/if (!${IsNoMana} && !${IsSHD}) {
		/call Skill_CreateData AFTER_SPAM "Sigil Earring of Veracity"
		/if (!${IsDRU} && !${IsRNG}) {
			/if (${AFTER_SPAM_ok} < 1) /call Skill_CreateData AFTER_SPAM "Girdle of Living Thorns"
		}
	}

	| storage character
    /call iniToVarV "${CharacterIni},Misc,Avoid AFK (On/Off)" trade_storagemode bool outer
	/if (!${Macro.Return}) /declare trade_storagemode bool outer FALSE

    /call Core_AddTask_LowMain Zuru_LowMain
    /call Core_AddTask_ZoneChange Zuru_ZoneChange

	| check for instant item clicky stats for after cast
	/declare aftercast_pause timer outer
/RETURN

SUB Zuru_Main
	/doevents zfollow
	/doevents zstop
/RETURN

SUB Zuru_LowMain
	/if (${zuru_autopoison}) /call checkpoisonvial

	/doevents testshoot
	/doevents autohidec
	/doevents taskcatacomb

	/if (${zuru_autoidol}) {
		/if (!${IsBRD}) /call Zuru_Idol
	}
	/if (${zuru_hidecen} != -1) {
		/if (${zuru_hidectime} == 0) {
			/call Bind_NetGroupCommand "/hidec ALLBUTGROUP"
			/varset zuru_hidectime ${zuru_hidecen}
		}
	}

	|/call Buff_RemoveShortBuff "Feign Death in the Afternoon"

	| storage mode to avoid transfering to afk zone
	/if (${trade_storagemode}) {
		/call Zuru_Comebackhome
	}

/RETURN

SUB Zuru_Comebackhome()
	/if (${Zone.ID} == 202) {
		/if (${Bool[${Spawn[Exit Portal].ID}]}) {
			| looks like Im in AFK PoK
			/declare orgpos string local ${Me.Y} ${Me.X} ${Me.Z}
			/declare myid ${Me.ID}
			/call travel_go "Exit Portal"
			/if (!${Macro.Return}) {
				/echo \ar Trade: Cannot go to "Exit Portal". ${Spawn[Exit Portal].Name}
				/return
			}
			/while (${Bool[${Spawn[Exit Portal].ID}]}) {
				/echo Trade: Waiting to zone ${Spawn[Exit Portal].Name} ${Bool[${Spawn[Exit Portal].ID}]}
				/delay 1s
			}
			/call travel_goloc ${orgpos}
			/if (!${Macro.Return}) {
				/echo \ar Trade: Cannot go to original position
				/return
			}
		}
	}
/RETURN

SUB Zuru_ZoneChange
	/varset NoHeroesFlag -1

	/if (!${IsNoMana} && !${IsSHD}) {
		/if (${AFTER_SPAM_ok} < 1) {
			/call Skill_CreateData AFTER_SPAM "Sigil Earring of Veracity"
			/if (!${IsDRU} && !${IsRNG}) {
				/if (${AFTER_SPAM_ok} < 1) /call Skill_CreateData AFTER_SPAM "Girdle of Living Thorns"
			}
		}
	}

	/if (${zuru_zfollowflag}) {
		/call zuru_zstop
	}

	/if (!${Bool[${Me.Buff[Bottle of Shared Adventure IV]}]}) {
		/call Buff_RemoveBuff "Bottle of Shared Adventure I"
	}
	/call Buff_RemoveBuff "Blessing of the Beggar"
	/call Buff_RemoveBuff "It's Levitation, But Fancier!"

/RETURN

SUB Zuru_WhileCasting(string spellName)
	|/if (${IsRNG}) {
	|	/call Buff_RemoveShortBuff "Feign Death in the Afternoon"
	|}
/RETURN

| multi-group manipulation
#EVENT zFollow "<#1#> zfollow"
SUB EVENT_zFollow(line, chatSender)
    /if (${chatSender.Equal[${Me}]}) /return
    /if (!${NetBots[${chatSender}].InZone}) /return
	/if (${NetBots[${chatSender}].InGroup}) /return
	/if (!${Bool[${Raid.Member[${chatSender}].ID}]}) /return

    /doevents flush zFollow
	/bc [+g+] Zuru: Follow: Group following ${chatSender}
	/call zuru_zfollow ${chatSender}
/RETURN

#EVENT zstop "<#1#> zstop"
SUB EVENT_zstop(line, chatSender)
	/if (${zuru_zfollowflag}) {
	    /if (!${NetBots[${chatSender}].InZone}) /return
		/call zuru_zstop
	}
/RETURN

SUB zuru_zfollow(whotoFollow)
    /if (${Window[LootWnd].Open}) {
        /notify LootWnd DoneButton leftmouseup
    } else /if (${Bool[${Me.Casting.ID}]} && !${IsBRD}) {
        /call Skill_Interrupt
    }
    /if (${Me.Feigning}) {
        /stand
    }
    /if (${Stick.Active}) {
        /squelch /stick off
    }
    | Setting related to buff.inc
    /varset Buff_PausePeriod 2s
    /varset Buff_BegProgress 0

    /if (${Bool[${Spawn[${whotoFollow}].ID}]}) {
        /if (!${Spawn[${whotoFollow}].LineOfSight}) {
            /if (!${Window[LootWnd].Open}) {
                /call travel_goid ${Spawn[pc ${whotoFollow}].ID}
            }
        }
    }

    /squelch /afollow spawn ${NetBots[${whotoFollow}].ID} on nodoor

	/varset zuru_zfollowflag TRUE
	/varset zuru_zfollowtarget ${whotoFollow}
/RETURN

SUB zuru_zstop
    /if (${Stick.Active}) {
        /squelch /stick off
    }
    /if (${AdvPath.Following}) /squelch /afollow off
    /if (${Navigation.Active}) /squelch /nav stop
    /moveto off

	/netgroup "/rawfaststop ${Me.Name}"

	/varset zuru_zfollowflag FALSE
	/varset zuru_zfollowtarget NULL
/RETURN


#event iscatacomb "#*# known as #1#, or#*#"
SUB EVENT_iscatacomb(line, namedName)
	/if (${namedName.Equal[Dragorn Marshal Greshka]}) {
		/echo Zuru: Found Catacomb A
		/varset zuru_catacomb 1
	} else /if (${namedName.Equal[Elite Overlord Vijaz]}) {
		/echo Zuru: Found Catacomb B
		/varset zuru_catacomb 2
	} else /if (${namedName.Equal[the Master of the Watch]}) {
		/echo Zuru: Found Catacomb C
		/varset zuru_catacomb 3
	}
/RETURN

#event taskcatacomb "<#1#> taskcatacomb"
SUB EVENT_taskcatacomb(line, chatSender)
	/if (${chatSender.NotEqual[${Me}]}) /return

	/if (!${Defined[zuru_catacomb]}) {
		/declare zuru_catacomb int outer 0
	}

	/target Grand Historian Rygua
	/if (${Target.CleanName.Equal[Grand Historian Rygua]}) {
		/declare i int local

		/doevents flush iscatacomb
		/for i 1 to 8
			/call Bind_NetGroupCommand "/dzquit"
			/delay 5
			/say catacombs
			/delay 5

			/varset zuru_catacomb 0
			/doevents iscatacomb
			/if (${zuru_catacomb} == 3) {
				/echo Zuru: Catacomb C ready!!
				/return
			}
			/delay 5
		/next i
	} else {
		/echo Zuru: Cannot find NPC
	}
/RETURN


#event autohidec "<#1#> autohidec"
#event autohidec "<#1#> autohidec #2#"
SUB event_autohidec(line, ChatSender, period)
	/if (!${Me.Name.Equal[${ChatSender}]}) /return

	/if (${zuru_hidecen} != -1) {
		/echo Zuru: Auto-hide corpse disabled.
		/varset zuru_hidecen -1
	} else {
		/if (${Defined[period]}) {
			/varset zuru_hidecen ${period}
		} else {
			/varset zuru_hidecen 300
		}
		/echo Zuru: Auto-hide corpse enabled. Timer ${zuru_hidecen}
		/varset zuru_hidectime ${zuru_hidecen}
	}

/RETURN


| use bottomless poison vial
SUB checkpoisonvial
	/if (${IsSafe} && !${Me.Moving} && !${InCombat}) {
		/call Skill_Cast_Data ${Me.ID} MAKEPOISON -1
		/if (${Macro.Return} == ${CAST_SUCCESS}) {
			/delay 1s
			/call Basic_AutoInventory
			/delay 1s
			/call Basic_AutoInventory
			/delay 1s
			/call Basic_AutoInventory
		}
	}
/RETURN

#EVENT testshoot "<#1#> testshoot #2# #3#"
SUB EVENT_testshoot(line, ChatSender, int ival, int dly)
	/if (${ChatSender.NotEqual[${Me}]}) /return
	/if (!${Defined[Rnextshootbase]}) {
		/declare Rnextshootbase int outer
		/declare Rshootdelaybase int outer
	}
	/varset pb_testshoot TRUE
	/stand

	/declare oldshoot int local ${Rnextshootbase}
	/declare olddelay int local ${Rshootdelaybase}
	/varset Rnextshootbase ${ival}
	/varset Rshootdelaybase ${dly}

	/declare i int local
	/declare result[8] int local

	/varset Attacking TRUE
	/for i 1 to 8
		/call Zuru_pbshoot ${AttackingTarget}
		/varset result[${i}] ${Macro.Return}
		/while (${pb_waitnextattack} > 0) {
			/delay 1
		}
	/next i
	/varset Attacking FALSE

	/varset pb_testshoot FALSE
	/varset Rnextshootbase ${oldshoot}
	/varset Rshootdelaybase ${olddelay}
	/echo Testshoot: done ${result[1]} ${result[2]} ${result[3]} ${result[4]} ${result[5]} ${result[6]} ${result[7]} ${result[8]}
/RETURN

#EVENT pbshootcount "You hit #*# for #*# points of damage."
#EVENT pbshootcount "You try to hit #*#, but miss!"
SUB EVENT_pbshootcount
	/varcalc pb_shootswing TRUE	
	/varcalc pb_shootcount ${pb_shootcount}+1
/RETURN

#EVENT pbshootswing "You #*# for #*# points of damage."
#EVENT pbshootswing "Your target is too far away#*#"
SUB EVENT_pbshootswing
	/varcalc pb_shootswing TRUE
/RETURN


SUB Zuru_pbshoot_exp(mobid)
	/if (${Me.Feigning}) /stand
	/call Skill_AmIReady
	/if (${Macro.Return} != ${CAST_SUCCESS}) /return 0

	| enable trushot
	/if (!${IsDisc}) {
		/call Skill_Cast_Data -1 RNG_TRUESHOT -1
	}

	/declare pblog[100] int local
	/declare pbstr string local

	/declare mobstat bool
	/call Basic_CheckMob ${mobid}
	/varset mobstat ${Macro.Return}
	/if (!${Spawn[${mobid}].LineOfSight}) /varset mobstat FALSE
	/if (!${Macro.Return}) {
		| find different mob

		/if (${mobid} == -1) /return 0
	}

	/squelch /face fast id ${mobid}
	/call MobTarget ${mobid}
	/call Combat_Autofire FALSE
	/echo zuru: pb \ar${Time} Start Attacking ${Target.Name}(${Target.ID})
	/doevents flush pbshootswing
	/squelch /attack on
	/varset pb_shootswing FALSE
	/declare i int local
    /for i 1 to 20
		/doevents BackOff
		/if (!${Attacking} || ${Target.ID} == 0) {
			/echo pb: back off
			/squelch /attack off
			/return
		}
		/doevents pbshootswing
		|/if (${pb_shootswing}) /break
		/delay 1
    /next i

	/doevents flush pbshootcount
    /call Combat_Autofire TRUE
	/echo zuru: pb \ar${Time} Start Shooting ${Target.Name}(${Target.ID})
	/varset pb_shootcount 0
	/declare flagstart bool local FALSE
	/declare flagnotcome int local 0
	/declare shootcount int local 0
    /for i 1 to 60
        /delay 1
        /if (${Math.Calc[${i}%8].Int} == 0) /squelch /face fast id ${mobid}
		/varset pb_shootswing FALSE
		/doevents pbshootcount
		/if (${pb_shootswing}) {
			| start to attack
			/if (!${flagstart}) {
				| first attack
				/varset flagstart TRUE
				/varset flagnotcome 0
			} else {
				| pbshooting
				/varcalc shootcount ${shootcount}+1
			}
		} else /if (${flagstart}) {
			/if (${flagnotcome} < 3) {
				/varcalc flagnotcome ${flagnotcome}+1
			} else {
				| pbshoot end
				/break
			}
		}
		/varset pblog[${i}] ${pb_shootcount}
        /doevents backoff
        /if (!${Attacking}) {
            /echo pb: back off
			/break
        } else /if (${Target.ID} == 0 || ${Bool[${Target.Dead}]}) {
			/echo \atpb: target lost. stop shooting ${pb_shootcount}
			/break
		}
    /next i
    /call Combat_Autofire FALSE
	/for i 1 to 60
		/varset pbstr ${pbstr} ${pblog[${i}]}
	/next i
    /echo pb: ${Time} \at shooting done ${pbstr}
	/delay 15
/RETURN ${pb_shootcount}

| pb shoot
SUB Zuru_pbshoot(pbtmobid)
    /declare pbtt int local
	/declare i int local
	/declare g int local
	/declare pbclean bool local FALSE
	/declare pbisnamed bool local
	/declare mobstat bool local
	/declare pbhate int local -1

	/declare pborder string local
	/declare hhp int local -1

	/if (${Me.Stunned}) /return
	/if (${Me.Feigning}) {
		/stand
	}

	| enable trushot
	/if (!${IsDisc}) {
		/call Skill_Cast_Data -1 RNG_TRUESHOT -1
	}

	/if (!${pb_testshoot}) {

		/call Basic_CheckMob ${pbtmobid}
		/varset mobstat ${Macro.Return}
		/if (!${Spawn[${pbtmobid}].LineOfSight}) /varset mobstat FALSE
		/if (!${Macro.Return}) {
			| if you are in non-tribute zone, that means zone is not hard. Changing to AE friendly target
			| find the highest hp mob
			/if (!${basic_tribute} && ${AutoBattling}) {
				/call Alert_CycleTarget xxxx
				/if (${Macro.Return} != -1) {
					/varset pbtmobid ${Macro.Return}
				} else {
					/if (${Math.Rand[1]} == 0) {
						/varset pborder i 1 to 13
					} else {
						/varset pborder i 13 downto 1
					}
					/for ${pborder}
						/call Basic_CheckXTMob ${i}
						/if (!${Macro.Return}) /continue

						/if (${autobattle_rolecode} == ${AB_ROLE_TANK}) /continue
						/if (!${Me.XTarget[${i}].LineOfSight}) /continue

						/if (${Me.XTarget[${i}].PctHPs} > ${hhp} && ${Me.XTarget[${i}].Distance} < 50) {
							/call combat_IsNamed ${Me.XTarget[${i}].ID}
							/if (${Macro.Return}) /continue
							/varset hhp ${Me.XTarget[${i}].PctHPs}
							/varset pbtmobid ${Me.XTarget[${i}].ID}
							/if (${hhp} > 60) /break
						}
					/next i

					/if (${pbtmobid} == -1) {
						/varset hhp -1
						/for ${pborder}
							/call Basic_CheckXTMob ${i}
							/if (!${Macro.Return}) /continue

							/if (${autobattle_rolecode} == ${AB_ROLE_TANK}) /continue
							/if (!${Me.XTarget[${i}].LineOfSight}) /continue
							/if (${Me.XTarget[${i}].PctHPs} > ${hhp} && ${Me.XTarget[${i}].Distance} < 200) {
								/call combat_IsNamed ${Me.XTarget[${i}].ID}
								/if (${Macro.Return}) /continue
								/varset hhp ${Me.XTarget[${i}].PctHPs}
								/varset pbtmobid ${Me.XTarget[${i}].ID}
								/if (${hhp} > 60) /break
							}
						/next i
					}
				}
			}
		}

		/if (${pbtmobid} == -1) {
			| no mob to attack
			/return 0
		}
	} else {
		/varset pbtmobid ${Target.ID}
	}

	| do hate clear even if pbshoot is in cooldown
    /if (${pb_waitnextattack} > 0) /return 0

    /call MobTarget ${pbtmobid}

    /echo pbpbpb: ${Time} attempt to attack ${Target.Name}(${Target.ID}) h ${Me.PctAggro}
    /squelch /face fast id ${pbtmobid}
    /declare nowtarget int local ${Target.ID}
    /declare pbthp int local

	/varset pb_shootcount 0

    /echo pbpbpb: \ar ${Time} start attacking
    /call Combat_Autofire FALSE
	/doevents flush pbshootswing
	/varset pb_shootswing FALSE
    /squelch /attack on
    /for i 1 to ${Rmeleedelaybase}
		/doevents BackOff
		/if (!${Attacking}) {
			/echo pbpbpb: back off
			/squelch /attack off
			/varcalc pb_waitnextattack ${Rnextshootbase}
			/return ${pb_shootcount}
		} else /if (${Target.ID} == 0) {
			/varset pbtmobid ${AttackerNextTarget[-1,200]}
			/if (${pbtmobid} != -1) {
				/echo pbpbpb: \ay Target lost(A)! next target ${Spawn[${pbtmobid}].Name}(${pbtmobid})
				/call MobTarget ${pbtmobid}
				/varset AttackingTarget ${pbtmobid}
				/squelch /face fast id ${pbtmobid}
			} else {
				/echo pbpbpb: no target(A)
				/squelch /attack off
				/varcalc pb_waitnextattack ${Rnextshootbase}
				/return ${pb_shootcount}
			}
		}
		/doevents pbshootswing
		|/if (${pb_shootswing} && ${i} > 4) {
		|	/echo pbpbpb: swing detected ${i}
		|	/break
		|}
		/delay 1
    /next i
	|/if (!${pb_shootswing}) {
	|	/echo pbpbpb: +++++ no swing message +++++
	|}

    /echo pbpbpb: ${Time} start shooting ${Rshootdelaybase}
	/doevents flush pbshootcount
    /call Combat_Autofire TRUE
    /for g 1 to ${Rshootdelaybase}
		|/call Buff_RemoveShortBuff "Feign Death in the Afternoon"
        /delay 1
        /if (${Math.Calc[${g}%8].Int} == 0) /squelch /face fast id ${pbtmobid}

		/if (!${pb_testshoot}) {
			/if (${Me.PctAggro} > 60) {
				/if (${Me.AltAbilityTimer[Cover Tracks]} == 0) {
					/call combat_IsNamed ${pbtmobid}
					/varset pbisnamed ${Macro.Return}
					/if ((${pbisnamed} && ${Target.Level} > 72) || ${Target.Level} > 73 || ${basic_tribute}) {
						| clearing hate
						/call Skill_Cast_Data -1 RNG_COVER -1
						/if (${Macro.Return} == ${CAST_SUCCESS}) {
							/echo pbpbpb: Cover Tracks casted ${Macro.Return}
						}
					}
				}
			}
		}
		/doevents pbshootcount
        /doevents backoff
        /if (!${Attacking}) {
            /echo pbpbpb: back off
			/break
        } else /if (${Target.ID} == 0 || ${Bool[${Target.Dead}]}) {
			/echo \at pbpbpb: target lost. stop shooting ${pb_shootcount}
			/break
		}

    /next g
    /call Combat_Autofire FALSE
    /echo pbpbpb: ${Time} \at shooting done ${pb_shootcount}
	/if (${pb_shootcount} < 5) {
	    /varset pb_waitnextattack 20
	} else {
	    /varset pb_waitnextattack ${Rnextshootbase}
	}
/RETURN ${pb_shootcount}

SUB Zuru_aftercast(int casttype, int targetID, bool force)
    /if (${IsSafe}) /return
	/if (${aftercast_pause} > 0) /return
	/call Skill_AmIReady
	/if (${Macro.Return} != ${CAST_SUCCESS}) /return

	| using necro orb
	/call Heal_UseOrb

	/if (${casttype} == ${CAST_DETRIMENTAL}) {
		/if (${Bool[${FindItem[=Molten Orb].ID}]}) {
			/if (${FindItem[=Molten Orb].TimerReady} == 0) {
				/call Skill_Cast ${targetID} "Molten Orb"
			}
		} else /if (${Bool[${FindItem[=Lava Orb].ID}]}) {
			/if (${FindItem[=Lava Orb].TimerReady} == 0) {
				/call Skill_Cast ${targetID} "Lava Orb"
			}
		}
	}

	/if (${IsNoMana} || ${IsSHD}) /return

	| Pet Tank check
	/if (${combat_pettank}) {
		/if (${AutoBattling} || ${Attacking}) /call Combat_PetTank ${targetID}
	}

	/if (!${force}) {
		/if (${Attacking} && !${Me.SpellInCooldown}) /return
	}

	/declare i int local
	/if (${class_aftercast}) {
		/if (${casttype} == ${CAST_DETRIMENTAL} && ${Attacking}) {
			/declare tthp int local -1
			/declare ttar int local -1

			/if (${CLASS_AFTERCAST_ok} < 1) {
				/call Skill_CreateData CLASS_AFTERCAST ${CLASS_AFTERCAST_spellname}
				/if (${Macro.Return}) {
					/return
				}
			}

			/declare nid int local
			:startaftercast
			/varset ttar -1
			/if (${Bool[${Spawn[${targetID}].ID}]}) {
				/if (${Spawn[${targetID}].PctHPs} < 100) {
					/varset ttar ${targetID}
				}
			}
			/if (${ttar} == -1) {
				/for i 1 to 13
					/call Basic_CheckXTMob ${i}
					/if (!${Macro.Return}) /continue

					/varset nid ${Me.XTarget[${i}].ID}
					/if (${Spawn[${nid}].Distance} > 220) /continue
					/if (!${Spawn[${nid}].LineOfSight}) /continue
					/if (${Spawn[${nid}].PctHPs} >= 99) /continue
					/if (${IsNEC} || ${IsENC} || ${IsDRU}) {
						/if (${Defined[combat_dot_${nid}_${CLASS_AFTERCAST_spellid}]}) {
							/if (${combat_dot_${nid}_${CLASS_AFTERCAST_spellid}} > 0) {
								/if (${ttar} == -1) {
									/varset ttar ${nid}
								}
								/continue
							}
						}
					}
					/if (${Spawn[${nid}].PctHPs} > ${tthp}) {
						/varset ttar ${nid}
						/varset tthp ${Spawn[${nid}].PctHPs}
						/if (${tthp} >= 98) {
							/break
						}
					}
				/next i
			}

			/if (${Bool[${Spawn[${ttar}].ID}]}) {
				/call Skill_Cast_Data ${ttar} CLASS_AFTERCAST -1
				/if (${Macro.Return} == ${CAST_SUCCESS}) {
					/if (${IsNEC} || ${IsENC} || ${IsDRU}) {
						/if (!${Defined[combat_dot_${ttar}_${CLASS_AFTERCAST_spellid}]}) {
							/declare combat_dot_${ttar}_${CLASS_AFTERCAST_spellid} timer outer
							/call ArrayAdd combat_dot combat_dot_${ttar}_${CLASS_AFTERCAST_spellid}
						}
						/varset combat_dot_${ttar}_${CLASS_AFTERCAST_spellid} ${Math.Calc[${Spell[${CLASS_AFTERCAST_spellname}].Duration}*60+${combat_dotextend}].Int}
						/if (${IsENC}) {
							/if (${Me.SpellInCooldown}) /goto :startaftercast
						}
					}
				}
			}
		}
	}

	/if (${InCombat} && ${combat_attacktypecode} == ${COMBAT_ATK_OFF}) {
		/if (!${force}) {
			/if (${Attacking} && !${Me.SpellInCooldown}) /return
		}
		/if (${basic_CombatManastone}) {
			/if (${basic_iscorruptstone} || ${basic_ismanastone}) {
				|/if (${basic_manastoneused} == 0) {
					/if (${combat_attacktypecode} == ${COMBAT_ATK_OFF} || ${combat_attacktypecode} == ${COMBAT_ATK_RANGED} || ${combat_attacktypecode} == ${COMBAT_ATK_PBSHOOT}) {
						/while (${Me.PctMana} < 80 && ${Me.PctHPs} > 75 && !${Me.FeetWet}) {
							/call basic_domanastone 2 TRUE
							/if (${IsHealer} || ${IsSubHealer}) {
								/call Heal_IsInjured
								/if (${Macro.Return}) /return
							}
							/if (${combat_attacktypecode} == ${COMBAT_ATK_PBSHOOT}) {
								/if (${pb_waitnextattack} < 6) /return
							}
							/if (${force}) {
								/break
							} else {
								/if (${Attacking} && !${Me.SpellInCooldown}) /return
							}
						}
					}
				|}
			}
		}
		/if (!${force}) {
			/if (${Attacking} && !${Me.SpellInCooldown}) /return
		}
		/if (${AFTER_SPAM_ok} > 0) {
			/call Skill_GoMCheck
			/if (!${Macro.Return}) {
				/if (${IsCLR} && ${Group.Members} > 0) {
					/if (${NetBots[${Group.Leader.Name}].InZone}) {
						/varset targetID ${Group.Leader.ID}
					} else {
						/varset targetID ${Me.ID}
						/for i 0 to ${Group.Members}
							/if (!${NetBots[${Group.Member[${i}].Name}].InZone}) /continue
							/if (!${NetBots[${Group.Member[${i}].Name}].Buff.Find[${AFTER_SPAM_spellid}]}) {
								/varset targetID ${Group.Member[${i}].ID}
								/break
							}
						/next i
					}
					/call Skill_Cast_Data ${targetID} AFTER_SPAM 70
					/if (${Macro.Return} == ${CAST_SUCCESS}) {
						/varset aftercast_pause 5
					}
				} else /if (${combat_attacktypecode} == ${COMBAT_ATK_OFF}) {
					/call Skill_Cast_Data ${Me.ID} AFTER_SPAM -1
					/if (${Macro.Return} == ${CAST_SUCCESS}) {
						/varset aftercast_pause 5
					}
				} else {
					/call Skill_Cast_Data -1 AFTER_SPAM -1
					/if (${Macro.Return} == ${CAST_SUCCESS}) {
						/varset aftercast_pause 5
					}
				}
			}
		}
	}
/RETURN


SUB Zuru_Idol
	/if (!${Defined[zuru_brdidoltimer]}) {
		/declare zuru_brdidoltimer timer outer
	}

	|/echo timer ${zuru_brdidoltimer} aru ${FindItem[=Fabled Idol of the Underking].ID}

    | Using Idol of the underking
    |/if (${zuru_brdidoltimer} == 0 && ${FindItem[=Fabled Idol of the Underking].ID}) {
    |/if (${zuru_brdidoltimer} == 0 && ${FindItem[=Fabled Idol of the Underking].ID} && ${FindItem[=Fabled Idol of the Underking].TimerReady} == 0 && ${InCombat} && ${Group.Members} && !${IsSafe}) {
    /if (${zuru_brdidoltimer} == 0 && ${FindItem[=Fabled Idol of the Underking].ID} && ${InCombat} && ${Group.Members} > 0 && !${IsSafe}) {
        /call CheckGroupHP ${zuru_idolhealpct}
        /if (${Macro.Return} >= 3) {
            /if (${IsBRD}) {
				/call bard_stopSong
			} else {
				/call skill_stopmoving "Fabled Idol of the Underking"
				/if (!${Macro.Return}) /return
			}

            /doevents flush underkingland
            /doevents flush underkingstop
            /varset Basic_UKstat 0
            /nomodkey /ItemNotify "Fabled Idol of the Underking" rightmouseup
            /bc [+r+]Idol: Word of Replenishment(AE Heal)

            /declare ukcasttime timer local 85
            /while (${ukcasttime}) {
                /if (${ukcasttime} > 55) {
                    /delay 1
                    /doevents underkingstop
                    /if (${Basic_UKstat} == 2) {
                        /bc [+r+]Idol: AE Heal cast interrupted. estimated remain: ${ukcasttime}
			            /if (${IsBRD}) /call bard_stopSong
						/varset zuru_brdidoltimer 15
                        /return
                    }
                }
                /if (${ukcasttime} < 15) {
                    /doevents underkingstop
                    |/doevents underkingland
                    /if (${Basic_UKstat} == 1) {
                        /bc [+r+]Idol: AE Heal cast done. estimated remain: ${ukcasttime}
			            /if (${IsBRD}) /call bard_stopSong
						/varset zuru_brdidoltimer 25s
                        /return
                    } else /if (${Basic_UKstat} == 2) {
                        /bc [+r+]Idol: AE Heal cast interrupted. estimated remain: ${ukcasttime}
			            /if (${IsBRD}) /call bard_stopSong
						/varset zuru_brdidoltimer 15
                        /return
                    }
                }
            }
            /bc [+r+]Idol: AE Heal cast probably done
			/if (${zuru_safemode}) {
				/varset zuru_brdidoltimer 25s
			} else {
				/varset zuru_brdidoltimer 15s
			}
            /if (${IsBRD}) /call bard_stopSong
        }
    }
/RETURN

