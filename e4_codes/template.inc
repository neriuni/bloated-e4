|**
Template module

Instruction to add module:
1. Rename "template" word to your module name.
2. add module to core.inc

**|
SUB template_Setup

    |**
    /call Core_AddTask_LowMain template_LowMain
    /call Core_AddTask_ZoneChange template_ZoneChange
    /call Core_AddTask_MobSlain template_MobSlain
	/call Core_AddTask_EndCombat template_EndCombat
    **|
/RETURN

SUB template_Main
/RETURN

|**
SUB template_LowMain
/RETURN

SUB template_ZoneChange
/RETURN

SUB template_MobSlain
/RETURN

SUB template_EndCombat
/RETURN
**|
