SUB Loot_Setup

    | Control

    | Parameter
    /call iniToVarV "${CharacterIni},Misc,Loot Settings" Loot_Ini string outer
    /if (!${Macro.Return}) {
        /declare Loot_Ini string outer e4_globalsettings\Loot Settings.ini
    } else {
        /varset Loot_Ini e4_globalsettings\\${Loot_Ini}
    }

	/call iniToVarV "${CharacterIni},Misc,Silent Loot (On/Off)" loot_silent bool outer
    /if (!${Macro.Return}) {
        /declare loot_silent bool outer FALSE
    }

    /declare Loot_SeekRadius int outer 120

    | Alias

    | Internal
    /declare loot_PauseLoot timer outer
    /declare lootSetting string outer
    /declare cantLootCorpseCount int outer 0

    /declare canLootCorpse bool outer

	| setting for autoloot
	/call iniToVarV "${CharacterIni},Misc,Auto-Loot (On/Off)" loot_loot bool outer
    /if (!${Macro.Return}) {
        /declare loot_loot bool outer FALSE
    }
	/call iniToVarV "${CharacterIni},Misc,Loot While Combat(On/Off)" loot_combat bool outer
    /if (!${Macro.Return}) {
        /declare loot_combat bool outer FALSE
    }

    /call Core_AddTask_LowMain Loot_LowMain
    /call Core_AddTask_ZoneChange Loot_ZoneChange

    /squelch /hidec looted

/RETURN

SUB Loot_Main
/RETURN

SUB Loot_LowMain
    /doevents tryLoot

    /if (!${loot_loot}) /return
    /if (!${loot_combat}) {
        /if (${InCombat}) /return
    }
    /if (${Me.Invis}) /return
    /if (${IsWindowOpen}) /return
    /if (${loot_PauseLoot} == 0) {
        /if (${Me.Moving}) {
            /if (${Basic_ManastoneSickness} == 0) {
                /return
            }
        }
        /if (!${Following} && !${Me.Moving}) {
            /if (!${IsDead} && !${Rez_wait4rez}) {
                /if (${MacroQuest.Foreground} && ${cruise_state} != 1) {
                    /if (${Group.Members} > 0) {
                        /if (${Group.Leader.Name.Equal[${Me}]}) {
                            /return
                        }
                    }
                }
                /call loot_area
                /if (${cruise_state} != 0) {
                    /if (${Macro.Return} && !${Following} && !${march_enable}) /call Movement_AnchorMove
                }
            }
        }
    }
/RETURN

SUB Loot_ZoneChange
    /squelch /hidec looted
/RETURN

| loot area
#EVENT tryLoot "<#1#> AutoLoot#2#"
SUB EVENT_tryLoot(line, ChatSender, Command)
	/if (${ChatSender.Equal[${Me.CleanName}]}) {
		/if (${Command.Find[battle]} && ${loot_loot}) {
			/if (${Command.Find[on]}) {
                /varset loot_loot TRUE
				/varset loot_combat TRUE
				/bc Auto Loot: Loot while in combat
			} else /if (${Command.Find[off]}) {
				/varset loot_combat FALSE
				/bc Auto Loot: Does not Loot while in combat
			}
			/return
		} else /if (${Command.Find[on]}) {
			/varset loot_loot TRUE
			/bc Auto Loot: Looting Enabled
		} else /if (${Command.Find[off]}) {
			/varset loot_loot FALSE
			/bc Auto Loot: Looting Disabled
			/return
		}
	}
	/if ((${Command.Find[me]} && ${ChatSender.Equal[${Me.CleanName}]}) || (${Command.Length} == 0 && ${loot_loot}) || ${Command.Find[all]} || ${Command.Find[${Me.CleanName}]}) {
		/if (${SpawnCount[npccorpse zradius 50 radius 100]}) {
            /call Movement_Stop
			/call loot_area
		}
	}
/RETURN

SUB loot_area
	/declare NumCorpses int local ${SpawnCount[npccorpse zradius 50 radius ${Loot_SeekRadius}]}

    /if (${NumCorpses} == 0) {
        /varset loot_PauseLoot 2s
        /return FALSE
    }
	/declare ClosestCorpse int local
	/declare ClosestCorpseDist int local
	/declare TMPClosestCorpse int local
	/declare TMPClosestCorpseDist int local
	/declare CorpseList string local
	/declare CorpseList_Looted string local
	/declare i int local
	/declare c int local
	/declare CorpsesLeft int local
    /declare ActualCorpse int local 0
    /declare tdist int local
    /declare tempid int local
    /declare lootorder string local

	/varset cantLootCorpseCount 0

    /if (${Math.Rand[1]} == 0) {
        /varset lootorder i 1 to ${NumCorpses}
    } else {
        /varset lootorder i ${NumCorpses} downto 1
    }

    /for ${lootorder}
        /varset tempid ${NearestSpawn[${i},npccorpse zradius 50 radius ${Loot_SeekRadius}].ID}
        /if (!${Bool[${tempid}]}) /continue
        /if (${Spawn[${tempid}].Type.Find[Untargetable]}) /continue
        /if (${Spawn[${tempid}].Name.Find[Prizes]}) /continue
        /if (${Spawn[${tempid}].Distance} > 200) /continue
        /varset ClosestCorpse ${tempid}
        /varset CorpseList ${CorpseList}${ClosestCorpse}#
        /varcalc ActualCorpse ${ActualCorpse}+1
    /next i
    /varset CorpsesLeft ${ActualCorpse}

    /if (${ActualCorpse} == 0) {
        /varset loot_PauseLoot 2s
        /return FALSE
    }

	/echo Auto Loot: Start looting (${ActualCorpse})

	/for i 1 to ${ActualCorpse}
		|iterate list of corpses
		/if (${CorpsesLeft} == 0) /break
		|recalculate the closest corpse from current position
        /varset ClosestCorpse -1
        /varset ClosestCorpseDist 1000
		/for c 1 to ${CorpsesLeft}
			/varset TMPClosestCorpse ${CorpseList.Arg[${c},#]}
			/varset TMPClosestCorpseDist ${Math.Distance[${Spawn[id ${TMPClosestCorpse}].Y},${Spawn[id ${TMPClosestCorpse}].X}]}
            /if (${ClosestCorpseDist} > ${TMPClosestCorpseDist}) {
                /varset ClosestCorpse ${TMPClosestCorpse}
                /varset ClosestCorpseDist ${TMPClosestCorpseDist}
                /if (${ClosestCorpseDist} <= 10) {
                    /break
                }
            }
		/next c
        /if (${ClosestCorpse} == -1) /break

        /if (${Bool[${Spawn[${ClosestCorpse}].ID}]}) {
            /if (${Spawn[${ClosestCorpse}].DistanceZ} < 60) {
                /if (${Spawn[${ClosestCorpse}].Distance} > 10) {
                    /call travel_distance ${ClosestCorpse}
                    /varset tdist ${Macro.Return}
                    /if (${Bool[${Spawn[${ClosestCorpse}].LineOfSight}]} && ${tdist} < 250) {
                        /call travel_goidex ${ClosestCorpse} 8
                    } else /if (!${Bool[${Spawn[${ClosestCorpse}].LineOfSight}]} && ${tdist} < 200) {
                        /call travel_goidex ${ClosestCorpse} 8
                    }
                }
                /if (${Bool[${Spawn[${ClosestCorpse}].ID}]}) {
                    /if (${Spawn[${ClosestCorpse}].Distance} <= 10 && ${Spawn[${ClosestCorpse}].DistanceZ} < 40) {
                        /call loot_Corpse ${ClosestCorpse}
                    } else {
                        /echo Corpse ${Spawn[${ClosestCorpse}].Name} is > ${Spawn[${ClosestCorpse}].Distance}|${Spawn[${ClosestCorpse}].DistanceZ}|${tdist} distance, skipping
                    }
                } else {
                    /echo Auto Loot: Cannot locate corpse ${Spawn[${ClosestCorpse}].Name} ${Spawn[${ClosestCorpse}].Distance}|${Spawn[${ClosestCorpse}].DistanceZ} distance, skipping
                }
            } else {
                /echo Auto Loot: Cannot locate corpse ${Spawn[${ClosestCorpse}].Name} ${Spawn[${ClosestCorpse}].Distance}|${Spawn[${ClosestCorpse}].DistanceZ} distance, skipping
            }
        } else {
            /echo Auto Loot: Corpse lost
        }

		/varset CorpseList ${CorpseList.Replace[${ClosestCorpse}#,]}
		/varcalc CorpsesLeft ${CorpsesLeft}-1

        /if (${cruise_state} == 1) {
            /call cruise_getnearestaggro
            /if (${Macro.Return} != -1 && !${loot_combat}) {
                /break
            }
        }

		/doevents Follow
        /doevents RaidAccept

        /call Basic_CheckGroupInvite

        /call Core_checkcombat
		/if (${Following} || ${InCombat}) {
			/break
		}
    /next i
	/echo Auto Loot: done
    /varset loot_PauseLoot 1s
/RETURN TRUE

SUB loot_MyFreeInventory
	/declare fe int local
	/varset fe ${Me.FreeInventory}
	/if (${IsRNG}) {
        /if (${Bool[${FindItem[=Fleeting Quiver].ID}]}) {
            /if (${FindItem[=Fleeting Quiver].ItemSlot} >= 23 && ${FindItem[=Fleeting Quiver].ItemSlot} <= 32) {
                /varcalc fe ${fe}-(6-${FindItem[=Fleeting Quiver].Items})
            }
        }
	}
    /varcalc fe ${fe}-1
    /if (${fe} < 0) /return 0
/RETURN ${fe}

#EVENT NotYourKill "Someone is already looting that corpse."
#EVENT NotYourKill "You may not loot this corpse at this time."
#EVENT NotYourKill "You are too far away to loot that corpse."
#EVENT NotYourKill "You must first target a corpse to loot!"
SUB EVENT_NotYourKill(line)
    /varset canLootCorpse FALSE
/RETURN

SUB loot_Corpse(int lootid)
    /declare i int local
    /declare j int local
    /declare w int local
    /declare itemName string local
    /declare itemSlot int local

    /declare lorecheck bool local FALSE

    /declare freeinv int local 0

    /varset canLootCorpse TRUE


    /call Skill_Interrupt
    /if (!${Bool[${Spawn[${lootid}].ID}]}) {
        | target is lost
        /varcalc cantLootCorpseCount ${cantLootCorpseCount}+1
        /return FALSE
    }
    /if (${Target.ID} != ${lootid}) {
        /call MobTarget ${lootid}
    }
    /doevents flush NotYourKill
    /loot
    /for w 1 to 30
        /doevents NotYourKill
        /if (!${canLootCorpse}) /break
        /if (${Target.ID} != ${lootid}) {
            /varset canLootCorpse FALSE
            /break
        }
        /if (${Window[LootWnd].Open}) /break
        /delay 1
    /next w

    /if (!${canLootCorpse}) {
        /varcalc cantLootCorpseCount ${cantLootCorpseCount}+1
        /goto :skipLoot
    }
    /if (!${Bool[${Corpse.Items}]}) /goto :skipLoot

    /declare numItems int local ${Corpse.Items}
    /declare argresult string local

    |must use numItems, ${Corpse.Items} recalcs every iteration
    /for i 1 to ${numItems}
        /if (!${Window[LootWnd].Open}) {
            /echo Auto Loot: loot windows is not open. Skipping...
            /goto :skipLoot
        }

        /call loot_MyFreeInventory
        /varset freeinv ${Macro.Return}

        /if (${freeinv} == 0) {
            | try destroying garbage
            /if (!${MacroQuest.Foreground}) {
                /call Sell_DestroyGarbage
            }
            /echo My inventory is full!  I will continue to link items on corpses, but cannot loot anything else.
        }

        /call get_lootSetting ${i} 0
        /if (${lootSetting.Find[Error]}) {
            /goto :skipLoot
        } else /if (${lootSetting.Find[Skip]}) {
            /if (${lootSetting.Find[beep]}) {
                /beep sounds/mail1
            }
            /goto :skipItem
        }

        /if (!${Bool[${Corpse.Item[${i}].Name}]}) /goto :skipLoot
        /echo Auto Loot: attempt to loot ${Corpse.Item[${i}].Name} c ${numItems} s ${i} bi ${FindItemBank[=${Corpse.Item[${i}].Name}]} bc ${FindItemBankCount[=${Corpse.Item[${i}].Name}]} fb ${FindItemCount[=${Corpse.Item[${i}].Name}]}
        /varset itemName ${Corpse.Item[${i}].Name}

        | Destroy the item
        /if (${lootSetting.Find[Destroy]} || ${lootSetting.Find[Break]}) {
            /if (!(${Corpse.Item[${i}].Lore} && ${FindItemCount[=${Corpse.Item[${i}].Name}]} > 0)) {
                /echo Loot: Destroying [${Corpse.Item[${i}].Name}]
                /call loot_Handle ${i} destroy
                /if (!${Macro.Return}) /goto :skipLoot
            } else {
                /echo Loot: Lore item [${Corpse.Item[${i}].Name}]
            }
        | Keep the item
        } else /if (${lootSetting.Find[Keep]}) {
            | Check for name
            /if (${lootSetting.Find[NoLoot]}) {
                /call argueString NoLoot| "${lootSetting}"
                /varset argresult ${Macro.Return}
                /if (${argresult.Find[${Me}]}) {
                    /echo Loot: I am not looting [${Corpse.Item[${i}].Name}].
                    /goto :skipItem
                }
            }
            | Check for a max stock
            /call argueString keep| "${lootSetting}"
            /varset argresult ${Macro.Return}
            /if (${Bool[${argresult}]} && ${Int[${argresult}]} < ${Math.Calc[${FindItemCount[=${Corpse.Item[${i}].Name}]} + 1].Int}) {
                /echo Loot: Fully stocked on [${Corpse.Item[${i}].Name}].
                /goto :skipItem
            }
            | If the item is lore, make sure I don't have one.  If I do, skip it.
            /if (${Corpse.Item[${i}].Lore}) {
                /varset lorecheck FALSE
                /if (${FindItemCount[=${Corpse.Item[${i}].Name}]} == 2 || ${FindItemCount[=${Corpse.Item[${i}].Name}]} == 1 || ${FindItemBankCount[=${Corpse.Item[${i}].Name}]} == 2) {
                    /echo Loot: lore. You already have items
                    /varset lorecheck TRUE
                } else /if (${FindItemBankCount[=${Corpse.Item[${i}].Name}]} == 1) {
                    /varset lorecheck TRUE
                    /for w 1 to 10
                        /if (${Me.Bank[25].Item[${w}].Name.Equal[${Corpse.Item[${i}].Name}]}) /varset lorecheck FALSE
                        /if (${Me.Bank[26].Item[${w}].Name.Equal[${Corpse.Item[${i}].Name}]}) /varset lorecheck FALSE
                        |/echo a ${Corpse.Item[${i}].Name} c1 ${Me.Bank[25].Item[${w}].Name} c2 ${Me.Bank[25].Item[${w}].Name} r ${lorecheck}
                        /if (!${lorecheck}) {
                            |/echo found in shared bank!
                            /break
                        }
                    /next w
                    /if (${lorecheck}) {
                        /echo Loot: lore. 1 item in bank
                    } else {
                        /echo Loot: lore. 1 item in shared bank. Attempt to loot
                    }
                }
                |/if (${FindItemCount[=${Corpse.Item[${i}].Name}]} || ${FindItemBankCount[=${Corpse.Item[${i}].Name}]}) {
                /if (${lorecheck}) {
                    /echo Loot: [${Corpse.Item[${i}].Name}] is lore, and I already have one${If[${FindItemBankCount[=${Corpse.Item[${i}].Name}]}, in the bank,]}.
                    /goto :skipItem
                }
            }
            | Check that I have an inventory space large enough for the item
            | If
            | 1) No open slots
            | 2) The item on the corpse is stackable
            | 3) I have one of the stackable items in my inventory already
            | Then check to see if there is room within the stack to loot it.
            /if (${freeinv} > 0) {
            } else /if (${freeinv} == 0 && ${Corpse.Item[${i}].Stackable} && ${Bool[${FindItem[=${itemName}]}]}) {
                /if (!${Bool[${FindItem[=${itemName}].ItemSlot}]}) /goto :skipItem
                /varset itemSlot ${FindItem[=${itemName}].ItemSlot}

                /if (${Bool[${Me.Inventory[${itemSlot}].Items}]}) {
                    /for j 1 to 10
                        /if (${Me.Inventory[${itemSlot}].Item[${j}].Name.Equal[${itemName}]}) {
                            /if (!${Me.Inventory[${itemSlot}].Item[${j}].FreeStack} ) {
                                /goto :skipItem
                            } else {
                            }
                        }
                    /next j
                } else {
                    /goto :skipItem
                }
            } else /if (${Me.LargestFreeInventory} < ${Corpse.Item[${i}].Size}) {
                /echo Loot: I don't have a free inventory space large enough to hold [${itemName}].
                /goto :skipItem
            }  else /if (${freeinv} == 0) {
                /goto :skipItem
            }
            /call loot_Handle ${i} keep
            /if (!${Macro.Return}) /goto :skipLoot
            /delay 1
        }
    :skipItem
    /next i
    /delay 1
    |looting done, if items leftover link them

    /if (!${loot_silent}) {
        /if (${Bool[${Corpse.Open}]}) {
            /if (${Corpse.Items} > 0) {
                /if (${Target.ID} != 0) {
                    /keypress /
                    /delay 1
                    /if (${Target.ID} == 0) {
                        /bc [+r+]unknown corpse:b ${Corpse.Open} ${Corpse.Items} ${Target.Name} ${Window[LootWnd].Open}
                        /call Type "error"
                    } else {
                        /call Type "say ${Target.ID}-"
                    }
                    /delay 1
                    /notify LootWnd BroadcastButton leftmouseup
                    /delay 1
                    /keypress enter chat
                |} else {
                |    /bc [+r+]unknown corpse:a ${Corpse.Open} ${Corpse.Items} ${Target.Name} ${Window[LootWnd].Open}
                }
            }
        }
    }

:skipLoot
    /for i 1 to 20
        /if (!${Window[LootWnd].Open}) /break
        /notify LootWnd DoneButton leftmouseup
        /delay 1
    /next i
    /while (${Window[LootWnd].Open}) {
        /bc [+r+]Loot: Cannot close corpse! Attempt to rewind!! Please wait a while.
        /notify LootWnd DoneButton leftmouseup
        /delay 1
        /call Movement_Stop
        /rewind
        /delay 5s
    }
/return TRUE

SUB get_lootSetting(invSlot, itemSlot)
    /varset lootSetting Error
    /if (!${Bool[${Corpse.Items}]} && !${Window[MerchantWnd]}) /return
	/declare itemName string local
	/declare itemValue string local
	/declare iniEntryVariables string local
	| If the item is not in a bag
	/if (!${Bool[${itemSlot}]}) {
		|/echo invSlot variable: ${invSlot} 
		| Reset itemName if it contains a ':'.  ':'s cause errors when reading from the ini, because they act as a delimiter, just like '='s
        |/echo ${Corpse.Item[${invSlot}].Name}		
		/varset itemName ${Corpse.Item[${invSlot}].Name}
        /if (!${Bool[${itemName}]}) /return
		/if (${itemName.Find[:]}) /varset itemName ${itemName.Replace[:,;]}
		/if (${itemName.Find[,]}) {
            /varset itemName ${itemName.Token[1,,]}#${itemName.Token[2,,]}
        }
		| Set item value
		/varset itemValue ${Corpse.Item[${invSlot}].Value}
		/varset itemValue ${If[${Bool[${itemValue.Left[${Math.Calc[${itemValue.Length} - 3].Int}]}]},${itemValue.Left[${Math.Calc[${itemValue.Length} - 3].Int}]}p,]}${If[${Bool[${itemValue.Mid[${Math.Calc[${itemValue.Length} - 2].Int}]}]},${itemValue.Mid[${Math.Calc[${itemValue.Length} - 2].Int}]}g,]}${If[${Bool[${itemValue.Mid[${Math.Calc[${itemValue.Length} - 1].Int}]}]},${itemValue.Mid[${Math.Calc[${itemValue.Length} - 1].Int}]}s,]}${If[${Bool[${itemValue.Right[1]}]},${itemValue.Right[1]}c,]}
		
		| Set ini variables like stack size, (C), (ND) etc.
		/varset iniEntryVariables ${If[${Corpse.Item[${invSlot}].Stackable},(${Corpse.Item[${invSlot}].StackSize}),]}${If[${Corpse.Item[${invSlot}].NoDrop},(ND),]}${If[${Corpse.Item[${invSlot}].Lore},(L),]}${If[${Corpse.Item[${invSlot}].Container},(C),]}
		| Check for a Loot_Ini entry
		|/echo /if (!{Ini[${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}].Length})
		/if (!${Ini[${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}].Length}) /echo ifsuccess
		/if (!${Ini[${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}].Length}) {
			|/echo /call WriteToIni "${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}" ${If[${Corpse.Item[${invSlot}].Container},Container,${If[${Corpse.Item[${invSlot}].NoDrop},Skip,Keep${If[${Corpse.Item[${invSlot}].Stackable},|${Corpse.Item[${invSlot}].StackSize},]}]}]}
			/call WriteToIni "${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}" ${If[${Corpse.Item[${invSlot}].Container},Container,${If[${Corpse.Item[${invSlot}].NoDrop},Skip,Keep${If[${Corpse.Item[${invSlot}].Stackable},|${Corpse.Item[${invSlot}].StackSize},]}]}]}
			/echo Added: [${Corpse.Item[${invSlot}].Name}(${Ini[${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}]})] to [${Loot_Ini}].
		}
	| If the item is in a bag
	} else {
		| Reset itemName if it contains a ':'.  ':'s cause errors when reading from the ini, because they act as a delimiter, just like '='s
		/varset itemName ${Me.Inventory[${invSlot}].Item[${itemSlot}].Name}
        /if (!${Bool[${itemName}]}) /return
		/if (${itemName.Find[:]}) /varset itemName ${itemName.Replace[:,;]}
		/if (${itemName.Find[,]}) {
            /varset itemName ${itemName.Token[1,,]}#${itemName.Token[2,,]}
        }
		| Set item value
		/varset itemValue ${Me.Inventory[${invSlot}].Item[${itemSlot}].Value}
		/varset itemValue ${If[${Bool[${itemValue.Left[${Math.Calc[${itemValue.Length} - 3].Int}]}]},${itemValue.Left[${Math.Calc[${itemValue.Length} - 3].Int}]}p,]}${If[${Bool[${itemValue.Mid[${Math.Calc[${itemValue.Length} - 2].Int}]}]},${itemValue.Mid[${Math.Calc[${itemValue.Length} - 2].Int}]}g,]}${If[${Bool[${itemValue.Mid[${Math.Calc[${itemValue.Length} - 1].Int}]}]},${itemValue.Mid[${Math.Calc[${itemValue.Length} - 1].Int}]}s,]}${If[${Bool[${itemValue.Right[1]}]},${itemValue.Right[1]}c,]}
		| Set ini variables like stack size, (C), (ND) etc.
		/varset iniEntryVariables ${If[${Me.Inventory[${invSlot}].Item[${itemSlot}].Stackable},(${Me.Inventory[${invSlot}].Item[${itemSlot}].StackSize}),]}${If[${Me.Inventory[${invSlot}].Item[${itemSlot}].NoDrop},(ND),]}${If[${Me.Inventory[${invSlot}].Item[${itemSlot}].Lore},(L),]}${If[${Me.Inventory[${invSlot}].Item[${itemSlot}].Container},(C),]}
		| Check for a Loot_Ini entry
		/if (!${Ini[${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}].Length}) {
			/call WriteToIni "${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}" ${If[${Me.Inventory[${invSlot}].Item[${itemSlot}].Container},Container,${If[${Me.Inventory[${invSlot}].Item[${itemSlot}].NoDrop},Skip,Keep${If[${Me.Inventory[${invSlot}].Item[${itemSlot}].Stackable},|${Me.Inventory[${invSlot}].Item[${itemSlot}].StackSize},]}]}]}
			/echo Added: [${Me.Inventory[${invSlot}].Item[${itemSlot}].Name}(${Ini[${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}]})] to [${Loot_Ini}].
			| Add scripts to import old loot settings from ninjaloot.ini, and old e3 loot inis
		}
	}
	/varset lootSetting ${Ini[${Loot_Ini},${itemName.Left[1]},${itemName} ${itemValue}${iniEntryVariables}]}
/RETURN

SUB loot_Handle(int slotNum, handle)
    /declare i int local
    /declare s int local
    /declare k int local
    /declare looted string local

    | clean your hands
    /for i 1 to 4
        /if (${Bool[${Cursor.ID}]}) {
            /echo Auto Loot: ${Cursor.Name} in my hand. Attemp to clean
            /call Basic_AutoInventory
            /delay 1
        }
    /next i
    /if (${Bool[${Cursor.ID}]}) {
        /echo Auto Loot: Cannot clear hands. Skipping loot
        /return FALSE
    }

    /for s 1 to 4
        /if (!${Window[LootWnd].Open}) /return FALSE
        | Try to loot the specified item from the corpse.
        /nomodkey /itemnotify loot${slotNum} leftmouseup
        /for i 1 to 20
            /delay 1
            /if (${Bool[${Cursor.ID}]} || ${Window[ConfirmationDialogBox].Open} || ${Window[QuantityWnd].Open}) /break
        /next i
        /varset looted ${Cursor.Name}
        | If the item has been looted, decide what to do with it.
        /if (${Bool[${Cursor.ID}]}) {
            | Destroy the item.
            /if (${handle.Equal[destroy]}) {
                /for k 1 to 4
                    /if (${looted.Equal[${Cursor.Name}]}) {
                        /destroy
                    } else {
                        /bc [+r+]Auto Loot: item mismatch detected ${looted} vs ${Cursor.Name}
                        /return FALSE
                    }
                    /delay 1
                    | If the destroy attempt was unsuccessful, try again.	
                    /if (!${Bool[${Cursor.ID}]}) /break
                /next k
                /echo Auto Loot: Destroyed ${slotNum} ${looted} wait: ${i} ${k}
                /return TRUE
            | Else, keep the item.
            } else {
                /call Basic_AutoInventory
                /echo Auto Loot: Succesfully looted ${slotNum} ${looted} wait: ${i}
                /return TRUE
            }
        | Else, if loot attempt was unsuccessful, try again.		
        } else {
            | Click yes on confirmation box.
            /if (${Window[ConfirmationDialogBox].Open}) {
                /notify ConfirmationDialogBox Yes_Button leftmouseup
            } else /if (${Window[QuantityWnd].Open}) {
                /notify QuantityWnd QTYW_Accept_Button leftmouseup
            }
        }
    /next s
    /echo Auto Loot: Tried 4 times to loot item. Cannot loot it
/RETURN FALSE
