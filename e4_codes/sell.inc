SUB Sell_Setup

    | Control

    | Parameter

    | Alias
    /squelch /alias /autosell /echo Auto Sell

    | Internal

    /call Core_AddTask_LowMain Sell_LowMain

/RETURN

SUB Sell_Main

/RETURN

SUB Sell_LowMain
    /doevents sellItems
/RETURN

#EVENT sellItems "<#1#> AutoSell"
SUB EVENT_sellItems(line, chatSender)
    /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return
	/if (!${chatSender.Equal[${Me}]}) {
		/if (!${NetBots[${chatSender}].Distance} > 200) /return
	}
	| Record starting location.
	/declare startingLoc string local ${Me.Y} ${Me.X} ${Me.Z}

	/declare mycash int local ${Me.Cash}

	| Open trade with the nearest merchant.
	/call openMerchant -1
		
	/if (${Window[MerchantWnd].Open}) {
		| Open bags.
		/keypress OPEN_INV_BAGS
	
		| Sell Items.
		/call sellItems
		
		| Move back to starting location.	
        /call Movement_ToLoc_Blocking ${startingLoc}
		
		| Close merchant.
		/if (${Window[MerchantWnd].Open}) /call closeMerchant
	
		| Destroy bad items.
		/call destroyItems
	
		| Close bags.
		/keypress CLOSE_INV_BAGS
		
		/varcalc mycash ${Me.Cash}-${mycash}

		/bc [+g+]AutoSell: Done. ${Math.Calc[${mycash}/1000]}pp earned
	} else {
		/bc [+r+]AutoSell: I cannot find merchant near by.
	}
/RETURN

SUB openMerchant(merchantID)

	/declare merchantTotal int local -1
    /declare s int local

	/if (${merchantID} == -1) {
		/for s 1 to 10
			/if (${Bool[${NearestSpawn[${s}, Merchant].ID}]}) {
				/varset merchantID ${NearestSpawn[${s}, Merchant].ID}
				/if (${NearestSpawn[${s}, Merchant].LineOfSight}) {
					/break
				}
			}
		/next s
	}
	/if (${merchantID} == -1) {
		/echo AutoSell: There are no merchants nearby!
		/return
	}
	| Move to the merchant.
	/call MobTarget ${merchantID}
	/call Movement_ToID_Blocking ${merchantID}

	/invoke ${Window[enviro].DoClose}
	/invoke ${Window[BazaarSearchWnd].DoClose}

	| Open Trade with merchant.
	/if (${Target.Distance} < 25) {
	
		| Right click merchant, and wait for window to open.
		/for s 1 to 4
			/if (!${Window[MerchantWnd].Open}) /click right target
			/delay 5
			/if (${Window[MerchantWnd].Open}) /break
		/next s
		/if (!${Window[MerchantWnd].Open}) {
			/echo AutoSell: Failed to open trade with [${Target.CleanName}].
		}
		
		| Wait for merchant's item list to populate.
		/for s 1 to 10
			/varset merchantTotal ${Window[MerchantWnd].Child[ItemList].Items}
			/delay 5
			/if (${merchantTotal} == ${Window[MerchantWnd].Child[ItemList].Items}) /break
		/next s
	} else {
		/echo AutoSell: Merchat ${Target.CleanName} is far ${Target.Distance}
	}
/RETURN

SUB closeMerchant
    /declare i int local

    /if (!${Window[MerchantWnd].Open}) /return

    /for i 1 to 10
    	/notify MerchantWnd MW_Done_Button leftmouseup
    	/delay 5
    	/if (!${Window[MerchantWnd].Open}) /break
    /next i

/RETURN

SUB sellItems
	/declare i int local
	/declare e int local
	
	/declare s int local
	/declare t int local

	/declare destroyUnsold bool local FALSE
	/declare selldone bool local FALSE

	/moveto off
	/while (${i} < 20) {
		/if (!${Me.Moving}) {
			/break
		}
		/delay 2
		/varcalc i ${i}+1
	}
	/if (${i} > 20) {
		/echo Sell: Moving detected. Abort selling ${i}
		/return
	}

	/call ArrayDestroy DestroyableItems
	/call ArrayCreate DestroyableItems

	| Scan inventory for items to sell
	/for i 1 to ${numInventorySlots}
		/if (${Me.Moving}) {
			/echo Sell: Moving detected. Abort selling.
			/return
		}
		/if (${Bool[${Me.Inventory[pack${i}]}]}) {
			| If the item in pack slot 'i' IS a container
			/if (${Me.Inventory[pack${i}].Container}) {
				| Look at each bag slot for items to sell
				/for e 1 to ${Me.Inventory[pack${i}].Container}				
					/if (${Bool[${Me.Inventory[pack${i}].Item[${e}]}]}) {

						| Get the loot setting for the item in pack slot 'i' slot 'e'
						/call get_lootSetting "pack${i}" "${e}"
					
						| Destroy the item
						/if (${lootSetting.Find[Destroy]}) {
							/echo Flagging [${Me.Inventory[pack${i}].Item[${e}]}] to be destroyed...
							/call ArrayAdd DestroyableItems "${Me.Inventory[pack${i}].Item[${e}]}"
						
						| Sell the item
						} else /if (${lootSetting.Find[Sell]}) {
					
							| Check that the item has value.
							/if (!${Me.Inventory[pack${i}].Item[${e}].Value}) {
								/if (${destroyUnsold}) {
									/echo [${Target.CleanName}] will not buy [${Me.Inventory[pack${i}].Item[${e}]}]. Flagging it to be destroyed.
									/call ArrayAdd DestroyableItems "${Me.Inventory[pack${i}].Item[${e}]}"
								}
							} else {
					
								| Select the item to sell
								/varset s 1
								:retrysell
								
								|/delay ${retryTimer} ${Window[MerchantWnd].Child[MW_SelectedItemLabel].Text.Equal[${Me.Inventory[pack${i}].Item[${e}]}]}
								/for t 1 to 40
									/nomodkey /itemnotify in pack${i} ${e} leftmouseup
									/delay 1
									/if (${Window[MerchantWnd].Child[MW_SelectedItemLabel].Text.Equal[${Me.Inventory[pack${i}].Item[${e}]}]}) /break
								/next t
					
								| If the item was not selected
								/if (!${Window[MerchantWnd].Child[MW_SelectedItemLabel].Text.Equal[${Me.Inventory[pack${i}].Item[${e}]}]}) {
									/echo ERROR: Failed to select [${Me.Inventory[pack${i}].Item[${e}]}], retry ${s} ${t}
								} else {
									| Check that the buy button is enabled
									/if (!${Window[MerchantWnd].Child[MW_Sell_Button].Enabled}) {
										/if (${destroyUnsold}) {
											/echo [${Target.CleanName}] will not buy [${Me.Inventory[pack${i}].Item[${e}]}]. Flagging it to be destroyed.
											/call ArrayAdd DestroyableItems "${Me.Inventory[pack${i}].Item[${e}]}"
											/goto :selldone
										}
									} else {							
										/echo Selling [${Me.Inventory[pack${i}].Item[${e}]}].
										
										|/delay ${retryTimer} ${Window[QuantityWnd].Open} || !${Bool[${Me.Inventory[pack${i}].Item[${e}]}]}
										/for t 1 to 40
											/shiftkey /notify MerchantWnd MW_Sell_Button leftmouseup
											/delay 1
											/if (!${Bool[${Me.Inventory[pack${i}].Item[${e}]}]}) /break
										/next t

										| If the item is still in my inventory
										/if (${Bool[${Me.Inventory[pack${i}].Item[${e}]}]}) {
											/echo ERROR: Failed to sell [${Me.Inventory[pack${i}].Item[${e}]}], retry ${s} ${t}
										} else {
											/goto :selldone
										}
									}
								}
								/varcalc s ${s}+1
								/if (${s} < 5) /goto :retrysell
								:selldone
							}
						}
					}
				/next e

			| If the item in pack slot 'i' is NOT a container
			} else {
			
				| Get the loot setting for the item in pack slot 'i'
				/call get_lootSetting "pack${i}" 0
			
				| Destroy the item
				/if (${lootSetting.Find[Destroy]}) {
					/echo Flagging [${InvSlot[pack${i}].Item.Name}] to be destroyed...
					/call ArrayAdd DestroyableItems "${InvSlot[pack${i}].Item.Name}"
				
				| Sell the item
				} else /if (${lootSetting.Find[Sell]}) {
				
					| Check that the item has value
					/if (!${Me.Inventory[pack${i}].Item.Value}) {
						/if (${destroyUnsold}) {
							/echo [${Target.CleanName}] will not buy [${Me.Inventory[pack${i}].Item.Name}]. Flagging it to be destroyed.
							/call ArrayAdd DestroyableItems "${Me.Inventory[pack${i}].Item.Name}"
						}
					} else {
				
						| Select the item to sell
						/varset selldone FALSE

						/for s 1 to 4
							/if (${selldone}) /break
							
							|/delay ${retryTimer} ${Window[MerchantWnd].Child[MW_SelectedItemLabel].Text.Equal[${Me.Inventory[pack${i}]}]}
							/for t 1 to 40
								/nomodkey /itemnotify in pack${i} leftmouseup
								/delay 1
								/if (${Window[MerchantWnd].Child[MW_SelectedItemLabel].Text.Equal[${Me.Inventory[pack${i}]}]}) /break
							/next t
						
							| If the item was not selected
							/if (!${Window[MerchantWnd].Child[MW_SelectedItemLabel].Text.Equal[${Me.Inventory[pack${i}]}]}) {
								/echo ERROR: Failed to select [${Me.Inventory[pack${i}]}], retry ${s} ${t}
							} else {
								| Check that the buy button is enabled
								/if (!${Window[MerchantWnd].Child[MW_Sell_Button].Enabled}) {
									/if (${destroyUnsold}) {
										/echo [${Target.CleanName}] will not buy [${Me.Inventory[pack${i}]}].  Flagging it to be destroyed.
										/call ArrayAdd DestroyableItems "${Me.Inventory[pack${i}]}"
										/varset selldone TRUE
									}
								} else {
									| Sell the item
									/echo Selling [${Me.Inventory[pack${i}]}].
									
									|/delay ${retryTimer} ${Window[QuantityWnd].Open} || !${Bool[${Me.Inventory[pack${i}]}]}
									/for t 1 to 40
										/shiftkey /notify MerchantWnd MW_Sell_Button leftmouseup
										/delay 1
										/if (!${Bool[${Me.Inventory[pack${i}]}]}) /break
									/next t
					
									| If the item is still in my inventory
									/if (${Bool[${Me.Inventory[pack${i}]}]}) {
										/echo ERROR: Failed to sell [${Me.Inventory[pack${i}]}], retry ${s} ${t}
									} else {
										/varset selldone TRUE
									}
								}
							}
						/next s
					}
				}
			}
		}
		
	/next i

/RETURN

SUB destroyItems
	
	/declare i int local
	/declare e int local
	/declare y int local
	/declare t int local
	
	/declare retryTimer timer local
	/declare badItem string local
	
	/declare itemsize int local
	/call ArrayLength DestroyableItems
	/varset itemsize ${Macro.Return}

    /if (${itemsize} < 1) /return
	/for i 1 to ${itemsize}
		/call ArrayGet DestroyableItems ${i}
		/varset badItem ${Macro.Return}

		/for e 1 to ${numInventorySlots}
		
			/if (!${Me.Inventory[pack${e}].Container}) {
				
				/if (${badItem.Equal[${Me.Inventory[pack${e}]}]}) {
					/varset retryTimer 50
					
					:retry_SlotPickup
					|/delay 5 ${Cursor.ID} || ${Window[QuantityWnd].Open}
					/for t 1 to 5
						/nomodkey /itemnotify pack${e} leftmouseup
						/delay 1
						/if (${Bool[${Cursor.ID}]} || ${Window[QuantityWnd].Open}) /break
					/next t
					
					| If the quantity window is open.
					/if (${Window[QuantityWnd].Open}) {
						|/delay 10 !${Window[QuantityWnd].Open}
						/for t 1 to 10
							/notify QuantityWnd QTYW_Accept_Button leftmouseup
							/delay 1
							/if (!${Window[QuantityWnd].Open}) /break
						/next t
					}
					
					|/delay 5 ${Cursor.ID}
					/for t 1 to 5
						/if (${Bool[${Cursor.ID}]}) /break
						/delay 1
					/next t
					
					/if (${badItem.Equal[${Cursor}]}) {
						/echo Destroying [${Cursor}].
						/destroy
					} else {
						/if (${retryTimer}) {
							/goto :retry_SlotPickup
						} else {
							/echo ERROR: I failed to destroy [${badItem}], skipping.
						}
					}
				}
			} else {
			
				/for y 1 to ${Me.Inventory[pack${e}].Container}
				
					/if (${badItem.Equal[${Me.Inventory[pack${e}].Item[${y}]}]}) {
					
						/varset retryTimer 50
					
						:retry_BagPickup
						|/delay 5 ${Cursor.ID} || ${Window[QuantityWnd].Open}
						/for t 1 to 5
							/nomodkey /itemnotify in pack${e} ${y} leftmouseup
							/delay 1
							/if (${Bool[${Cursor.ID}]} || ${Window[QuantityWnd].Open}) /break
						/next t
						
						| If the quantity window is open.
						/if (${Window[QuantityWnd].Open}) {
							|/delay 10 !${Window[QuantityWnd].Open}
							/for t 1 to 10
								/notify QuantityWnd QTYW_Accept_Button leftmouseup
								/delay 1
								/if (!${Window[QuantityWnd].Open}) /break
							/next t
						}
						
						|/delay 5 ${Cursor.ID}
						/for t 1 to 5
							/if (${Bool[${Cursor.ID}]}) /break
							/delay 1
						/next t
						
						/if (${badItem.Equal[${Cursor}]}) {
							/echo Destroying [${Cursor}].
							/destroy
						} else {
							/if (${retryTimer}) {
								/goto :retry_BagPickup
							} else {
								/echo ERROR: I failed to destroy [${badItem}], skipping.
							}
						}
					}
				/next y
			}
		
		/next e
		
	/next i

/RETURN

SUB Sell_DestroyGarbage
	/call ArrayDestroy DestroyableItems
	/call ArrayCreate DestroyableItems

	/declare i int local
	/declare e int local

	/for i 1 to ${numInventorySlots}
		/if (${Me.Moving}) {
			/echo Sell: Moving detected. Abort Destroying
			/return
		}
		/if (${Bool[${Me.Inventory[pack${i}]}]}) {
			| If the item in pack slot 'i' IS a container
			/if (${Me.Inventory[pack${i}].Container}) {
				| Look at each bag slot for items to sell
				/for e 1 to ${Me.Inventory[pack${i}].Container}				
					/if (${Bool[${Me.Inventory[pack${i}].Item[${e}]}]}) {

						| Get the loot setting for the item in pack slot 'i' slot 'e'
						/call get_lootSetting "pack${i}" "${e}"
					
						| Destroy the item
						/if (${lootSetting.Find[Destroy]}) {
							/echo Flagging [${Me.Inventory[pack${i}].Item[${e}]}] to be destroyed...
							/call ArrayAdd DestroyableItems "${Me.Inventory[pack${i}].Item[${e}]}"
						}
					}
				/next e

			| If the item in pack slot 'i' is NOT a container
			} else {
			
				| Get the loot setting for the item in pack slot 'i'
				/call get_lootSetting "pack${i}" 0
			
				| Destroy the item
				/if (${lootSetting.Find[Destroy]}) {
					/echo Flagging [${InvSlot[pack${i}].Item.Name}] to be destroyed...
					/call ArrayAdd DestroyableItems "${InvSlot[pack${i}].Item.Name}"				
				}
			}
		}
	/next i
	/if (${Me.Moving}) {
		/echo Sell: Moving detected. Abort Destroying
		/return
	}
	/call destroyItems

/RETURN
