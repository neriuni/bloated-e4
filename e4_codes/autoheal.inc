SUB AutoHeal_Setup

    /declare autoheal_enable bool outer
    /call iniToVarV "${CharacterIni},AutoHeal,Auto-Heal (On/Off)" autoheal_enable bool outer
    /if (!${Macro.Return}) {
        /return
    }
    /if (!${autoheal_enable}) /return
    | Auto Heal is enabled. Disable Heal.inc
    /varset heal_enable FALSE

    /varset autoheal_enable TRUE

    | ultimate heal
    /declare i int local
    /call IniToArrayV "${CharacterIni},AutoHeal,Fast#" autoheal_fastlist
    /if (${Defined[autoheal_fastlist]}) {
        /if (${autoheal_fastlist.Size} > 0) {
            /for i 1 to ${autoheal_fastlist.Size}
                /call Skill_CreateData autoheal_fast${i} "${autoheal_fastlist[${i}]}"
            /next i
        }
    }
    | self ult
    /call IniToArrayV "${CharacterIni},AutoHeal,Self#" autoheal_selflist
    /if (${Defined[autoheal_selflist]}) {
        /if (${autoheal_selflist.Size} > 0) {
            /for i 1 to ${autoheal_selflist.Size}
                /call Skill_CreateData autoheal_self${i} "${autoheal_selflist[${i}]}"
            /next i
        }
    }

    | high priority member to heal
    /declare autoheal_priorityen bool outer
    /call IniToArrayV "${CharacterIni},AutoHeal,Priority#" autoheal_tanklist
    /if (${Defined[autoheal_tanklist]}) {
        /if (${autoheal_tanklist.Size} > 0) {
            /varset autoheal_priorityen TRUE
        }
    }

    | high priority pet to heal
    /declare autoheal_peten bool outer
    /call IniToArrayV "${CharacterIni},AutoHeal,Pet#" autoheal_petlist
    /if (${Defined[autoheal_petlist]}) {
        /if (${autoheal_petlist.Size} > 0) {
            /varset autoheal_peten TRUE
        }
    }

    | low priority self pet heal
    /call IniToArrayV "${CharacterIni},AutoHeal,MyPet#" autoheal_mypetlist
    /if (${Defined[autoheal_mypetlist]}) {
        /if (${autoheal_mypetlist.Size} > 0) {
            /for i 1 to ${autoheal_mypetlist.Size}
                /call Skill_CreateData autoheal_mypet${i} "${autoheal_mypetlist[${i}]}"
            /next i
        }
    }

    | short heal
    /declare autoheal_shorten bool outer
	/call iniToVarV "${CharacterIni},AutoHeal,Short" autoheal_shortraw string outer
    /if (${Macro.Return}) {
        /call SKill_CreateData autoheal_short "${autoheal_shortraw}"
        /varset autoheal_shorten ${Macro.Return}
    }

    | HoT
    /declare autoheal_timeen bool outer
	/call iniToVarV "${CharacterIni},AutoHeal,HoT" autoheal_timeraw string outer
    /if (${Macro.Return}) {
        /call SKill_CreateData autoheal_time "${autoheal_timeraw}"
        /varset autoheal_timeen ${Macro.Return}
    }

    | Group
    /declare autoheal_groupen bool outer
	/call iniToVarV "${CharacterIni},AutoHeal,Group" autoheal_groupraw string outer
    /if (${Macro.Return}) {
        /call SKill_CreateData autoheal_group "${autoheal_groupraw}"
        /varset autoheal_groupen ${Macro.Return}
    }

    | Group HoT
    /declare autoheal_gtimeen bool outer
	/call iniToVarV "${CharacterIni},AutoHeal,GHoT" autoheal_gtimeraw string outer
    /if (${Macro.Return}) {
        /call SKill_CreateData autoheal_gtime "${autoheal_gtimeraw}"
        /varset autoheal_gtimeen ${Macro.Return}
    }


    /declare autoheal_singledistance int outer 200
    /declare autoheal_groupdistance int outer 70

    /declare autoheal_hard bool outer FALSE

    /call Core_AddTask_LowMain AutoHeal_LowMain

/RETURN FALSE

SUB AutoHeal_Main
    /if (!${autoheal_enable}) /return
    /call AutoHeal_Check
/RETURN

SUB AutoHeal_LowMain
    /if (!${autoheal_enable}) /return
    /if (${InCombat}) {
        /declare i int local
        /for i 1 to 13
            /call Basic_CheckXTMob ${i}
            /if (${Macro.Return}) {
                /if (${Me.XTarget[${i}].Level} > 70) {
                    /varset autoheal_hard TRUE
                    /varset heal_threshould 90
                } else {
                    /varset autoheal_hard FALSE
                    /varset heal_threshould 70
                }
                /break
            }
        /next i
    }
/RETURN

SUB AutoHeal_Check
    |/varset heal_error
    /declare i int local
    /declare g int local

    /declare gcount int local 0
    /declare ghp int local 0
    /declare gtemp int local

    /if (${Bool[${Group}]}) {
        /if (${InCombat}) {
            | check tank
            /if (${autoheal_priorityen}) {
                /for i 1 to ${autoheal_tanklist.Size}
                    /if (${NetBots[${autoheal_tanklist[${i}]}].InZone}) {
                        /if (${Spawn[${NetBots[${autoheal_tanklist[${i}]}].ID}].Distance} <= ${autoheal_singledistance}) {
                            /if (${autoheal_hard}) {
                                /call AutoHeal_SingleTry ${NetBots[${autoheal_tanklist[${i}]}].ID} 92 70
                            } else {
                                /call AutoHeal_SingleTry ${NetBots[${autoheal_tanklist[${i}]}].ID} 75 50
                            }
                            /if (${Macro.Return}) /return TRUE
                        }
                    }
                /next i
            }
            | check tank pet
            /if (${autoheal_peten}) {
                /for i 1 to ${autoheal_petlist.Size}
                    /if (${NetBots[${autoheal_petlist[${i}]}].InZone}) {
                        /if (${Bool[${Group.Member[${autoheal_petlist[${i}]}].Pet.ID}]}) {
                            /if (${Group.Member[${autoheal_petlist[${i}]}].Pet.Distance} <= ${autoheal_singledistance}) {
                                /if (${autoheal_hard}) {
                                    /call AutoHeal_SingleTry ${Group.Member[${autoheal_petlist[${i}]}].Pet.ID} 92 70
                                } else {
                                    /call AutoHeal_SingleTry ${Group.Member[${autoheal_petlist[${i}]}].Pet.ID} 75 50
                                }
                                /if (${Macro.Return}) /return TRUE
                            }
                        }
                    }
                /next i
            }
        }

        | try group heal
        /for i 0 to ${Group}
            /if (${NetBots[${Group.Member[${i}].Name}].InZone}) {
                /if (${Group.Member[${i}].Distance} >= ${autoheal_groupdistance}) /continue
                /varcalc gtemp 100-${Group.Member[${i}].PctHPs}
                /if (${gtemp} > 7) {
                    /varcalc gcount ${gcount}+1
                    /varcalc ghp ${ghp}+${gtemp}
                }
            }
        /next i
        /if (${autoheal_groupen}) {
            /if (${gcount} > 1) {
                /if (${ghp} > 65) {
                    /for i 1 to 5
                        /call Skill_Cast_Data ${Me.ID} autoheal_group -1
                        /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                        /if (${Macro.Return} == ${CAST_INTERRUPTED}) /continue
                        /if (${Macro.Return} > ${CAST_INTERRUPTED}) /break
                    /next i
                }
            }
        }
        | try short heal to group member
        /for i 0 to ${Group}
            /if (${NetBots[${Group.Member[${i}].Name}].InZone}) {
                /if (${Group.Member[${i}].Distance} > ${autoheal_singledistance}) /continue
                /if (${InCombat}) {
                    /call AutoHeal_SingleTry ${Group.Member[${i}].ID} 70 45
                } else {
                    /call AutoHeal_SingleTry ${Group.Member[${i}].ID} 50 -1
                }
                /if (${Macro.Return}) /return TRUE
            }
        /next i

        | my pet heal
        /if (${Bool[${Me.Pet.ID}]}) {
            /if (${Defined[autoheal_mypetlist]}) {
                /if (${Me.Pet.Distance} <= ${autoheal_singledistance}) {
                    /for i 1 to ${autoheal_mypetlist.Size}
                        /if (${autoheal_hard}) {
                            /call AutoHeal_SingleTry ${Me.Pet.ID} 92 60
                        } else {
                            /call AutoHeal_SingleTry ${Me.Pet.ID} 75 50
                        }
                        /if (${Macro.Return}) /return TRUE
                    /next i
                }
            }
        }

        | try group HoT
        /if (${autoheal_gtimeen}) {
            /if (${gcount} > 2) {
                /if (${ghp} > 10) {
                    /if (!${NetBots[${Me}].ShortBuff.Find[${autoheal_gtime_spellid}]}) {
                        /for i 1 to 5
                            /call Skill_Cast_Data ${Me.ID} autoheal_gtime -1
                            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                            /if (${Macro.Return} == ${CAST_INTERRUPTED}) /continue
                            /if (${Macro.Return} > ${CAST_INTERRUPTED}) /break
                        /next i
                    }
                }
            }
        }

        /if (${autoheal_priorityen}) {
            | cast single hot to tank
            /for i 1 to ${autoheal_tanklist.Size}
                /if (${NetBots[${autoheal_tanklist[${i}]}].InZone}) {
                    /if (${autoheal_timeen}) {
                        /if (${autoheal_time_spellid} == 15236) {
                            /if (!${InCombat}) /goto :skiphot
                        }
                        /call heal_HoTCheck ${autoheal_time_spellid} "${NetBots[${autoheal_tanklist[${i}]}].ShortBuff}"
                        /if (${Macro.Return}) {
                            /for g 1 to 5
                                /call Skill_Cast_Data ${NetBots[${autoheal_tanklist[${i}]}].ID} autoheal_time -1
                                /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                                /if (${Macro.Return} >= ${CAST_INTERRUPTED}) /break
                            /next g
                        }
                    }
                    :skiphot
                    | do force heal
                    /if (${autoheal_hard}) {
                        /if (${autoheal_shorten}) {
                            /if (!${Following}) {
                                /if (${InCombat}) {
                                    /for g 1 to 5
                                        /call Skill_Cast_Data ${NetBots[${autoheal_tanklist[${i}]}].ID} autoheal_short 90
                                        /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                                        /if (${Macro.Return} >= ${CAST_INTERRUPTED}) /break
                                        /return FALSE
                                    /next g
                                }
                            }
                        }
                    }
                }
            /next i
        }
    } else {
        | only me
        /if (${InCombat}) {
            /if (${autoheal_hard}) {
                /call AutoHeal_SingleTry ${Me.ID} 90 60
            } else {
                /call AutoHeal_SingleTry ${Me.ID} 75 50
            }
        } else {
            /call AutoHeal_SingleTry ${Me.ID} 50 -1
        }
        /if (${Macro.Return}) /return TRUE

        | try HoT
        /if (${autoheal_timeen}) {
            /if (${Me.PctHPs} < 80) {
                /call heal_HoTCheck ${autoheal_time_spellid} "${NetBots[${Me}].ShortBuff}"
                /if (${Macro.Return}) {
                    | cast single hot to tank
                    /for i 1 to 5
                        /call Skill_Cast_Data ${Me.ID} autoheal_time -1
                        /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                        /if (${Macro.Return} >= ${CAST_INTERRUPTED}) /break
                    /next i
                }
            }
        }
    }

/RETURN FALSE

SUB AutoHeal_SingleTry(int id, int hhp, int lhp)
    /if (${Spawn[${id}].PctHPs} >= 100) /return FALSE

    /declare i int local
    /declare fasttried bool local FALSE
    /declare msdone bool local

    /if (${Spawn[${id}].Dead}) /return FALSE
    /if (${Spawn[${id}].PctHPs} <= ${hhp}) {
        :retryfast
        /if (${InCombat}) {
            /if (${Spawn[${id}].PctHPs} <= ${lhp}) {
                | cast healer class specific heal
                /if (${IsHealer}) {
                    /if (${Bool[${NetBots[${Spawn[${id}].Name}].ID}]}) {
                        /call AutoHeal_${Me.Class.ShortName} ${id} ${Spawn[${id}].PctHPs}
                        /if (${Macro.Return}) /return TRUE
                    }
                }
                | cast fast heal
                /if (${id} == ${Me.ID}) {
                    /if (${Defined[autoheal_selflist]}) {
                        /for i 1 to ${autoheal_selflist.Size}
                            /if (${autoheal_self${i}_ok} > 0) {
                                /call Skill_Cast_Data ${id} autoheal_self${i} -1
                                /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                                /if (${Macro.Return} == ${CAST_INTERRUPTED}) {
                                    /if (${Spawn[${id}].PctHPs} > ${lhp}) /return FALSE
                                    /continue
                                }
                                /if (${Macro.Return} > ${CAST_INTERRUPTED}) /break
                            }
                        /next i
                    }
                } else /if (${id} == ${Me.Pet.ID}) {
                    /if (${Defined[autoheal_mypetlist]}) {
                        /for i 1 to ${autoheal_mypetlist.Size}
                            /if (${autoheal_mypet${i}_ok} > 0) {
                                /call Skill_Cast_Data -1 autoheal_mypet${i} -1
                                /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                                /if (${Macro.Return} == ${CAST_INTERRUPTED}) {
                                    /if (${Spawn[${id}].PctHPs} > ${lhp}) /return FALSE
                                    /continue
                                }
                                /if (${Macro.Return} > ${CAST_INTERRUPTED}) /break
                            }
                        /next i
                    }
                }
                /if (${Defined[autoheal_fastlist]}) {
                    /for i 1 to ${autoheal_fastlist.Size}
                        /if (${autoheal_fast${i}_ok} > 0) {
                            /call Skill_Cast_Data ${id} autoheal_fast${i} -1
                            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                                /if (${Macro.Return} == ${CAST_INTERRUPTED}) {
                                    /if (${Spawn[${id}].PctHPs} > ${lhp}) /return FALSE
                                    /continue
                                }
                            /if (${Macro.Return} > ${CAST_INTERRUPTED}) /break
                        }
                    /next i
                }
                /varset fasttried TRUE
            }
        }
        /if (!${autoheal_shorten}) /return FALSE
        | check still in global cooldown
        /while (${Me.SpellInCooldown}) {
            /if (${basic_ismanastone}) {
                /if (${basic_qswaptimer} == 0) {
                    /if (!${IsSafe}) {
                        /if (${Me.PctMana} < 95) {
                            /if (!${InCombat} && ${Me.PctHPs} > 50 || ${Me.PctHPs} > 75) {
                                /nomodkey /ItemNotify "Manastone" rightmouseup
                                /varset msdone TRUE
                            }
                        }
                    }
                }
            }
        }
        /if (${msdone}) {
            /varset Basic_ManastoneSickness 5
            /doevents flush basic_manastonemsg
        }

        | last hp check
        /if (${InCombat}) {
            /if (!${fasttried} && ${Spawn[${id}].PctHPs} <= ${lhp}) /goto :retryfast
        }
        /if (${Spawn[${id}].PctHPs} > ${hhp}) {
            /return FALSE
        }
        | cast short heal
        /for i 1 to 5
            /call Skill_Cast_Data ${id} autoheal_short 95
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
            /if (${Macro.Return} == ${CAST_INTERRUPTED}) {
                /if (${Spawn[${id}].PctHPs} > ${lhp}) /return FALSE
                /if (${InCombat}) {
                    /if (!${fasttried} && ${Spawn[${id}].PctHPs} <= ${lhp}) /goto :retryfast
                }
                /continue
            }
            /return FALSE
        /next i
    }
/RETURN FALSE

SUB AutoHeal_CLR(int id, int hp)
    /if (${hp} < 60) {
        /declare i int local
        /for i 1 to 2
            /if (${Defined[CLR_DIVARB${i}_ok]}) {
                /if (${CLR_DIVARB${i}_ok} > 0) {
                    /call Skill_Cast_Data -1 CLR_DIVARB${i} -1
                    /if (${Macro.Return} == ${CAST_SUCCESS}) {
                        /bc [+y+]Cleric: ${CLR_DIVARB${i}_spellname} to ${Spawn[${id}].Name} ${hp}
                        /call Skill_Cast_Data -1 CLR_CREGENE -1
                        /return TRUE
                    }
                }
            }
        /next i

        /call Skill_Cast_Data ${id} CLR_QMIRA -1
        /if (${Macro.Return} == ${CAST_SUCCESS}) {
            /bc [+y+]Cleric: ${CLR_QMIRA_spellname} to ${Spawn[${id}].Name} ${hp}
            /return TRUE
        }
    }
/RETURN FALSE

SUB AutoHeal_SHM(int id, int hp)
/RETURN TRUE

SUB AutoHeal_DRU(int id, int hp)
/RETURN TRUE
