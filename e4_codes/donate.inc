SUB donate_Setup
    /declare donate_maxitem int outer 1000

    /call Core_AddTask_LowMain donate_LowMain
/RETURN

SUB donate_Main
/RETURN

SUB donate_LowMain
    /if (${IsSafe}) {
        /doevents autodonate
        /doevents symbolexchange
    }
    /doevents addAutoDonateRelay
/RETURN

#event symbolexchange "<#1#> Symbol"
SUB EVENT_symbolexchange(line, chatSender)
    /if (${Me.Name.NotEqual[${chatSender}]}) /return

    /if (!${Bool[${Ini["e4_globalsettings\donate.ini,symbol items,item1"]}]}) {
        /echo Symbol: no symbol item list
        /return
    }

    /if (${Bool[${Cursor.ID}]}) {
        /echo Symbol: ${Cursor.Name} in my hand. Aborting...
        /return
    }

    /call moveToTributeMaster TRUE
    /if (${Macro.Return} == -1) {
        /return
    }

    /keypress OPEN_INV_BAGS
    /delay 3

    /declare npcid int local
    /if (${Zone.ID} == 202) {
        /varset npcid ${Spawn[Klorg].ID}
    } else {
        /echo Symbol: Unknown starting zone
        /return
    }

    /declare i int local
    /declare s int local
    /declare aug bool local FALSE

    /declare gave int local

    /declare itemName string local
    /for i 1 to 250
        /varset itemName ${Ini["e4_globalsettings\donate.ini,symbol items,item${i}"]}
        /if (!${Bool[${itemName}]}) /continue
        |/echo debug --- ${itemName}, ${Bool[${FindItem[=${itemName}].ID}]}
        /if (!${Bool[${FindItem[=${itemName}].ID}]}) {
            /echo Symbol: Cannot find ${itemName}
            /continue
        } else /if (${FindItem[=${itemName}].NoDrop}) {
            /echo Symbol: ${itemName} is NoDrop
            /continue
        } else {
            /varset aug FALSE
            /for s 1 to 5
                /if (${Bool[${FindItem[=${itemName}].Item[${s}].ID}]}) {
                    /echo Symbol: ${itemName} has aug ${FindItem[=${itemName}].Item[${s}].Name} in slot ${s}
                    /varset aug TRUE
                    /break
                }
            /next s
            /if (${aug}) /continue
        }
        /if (${FindItem[=${itemName}].ItemSlot} < 23) {
            /echo Symbol: You are equipping ${itemName}
            /continue
        }
        /if (${Bool[${Cursor.ID}]}) {
            /echo Symbol: ${Cursor.Name} in my hand. end auto donate.
            /return
        }

        /echo Symbol: trying to give ${itemName}
        /ctrlkey /itemnotify in pack${Math.Calc[${FindItem[=${itemName}].ItemSlot}-22].Int} ${Math.Calc[${FindItem[=${itemName}].ItemSlot2} + 1].Int} leftmouseup
        /delay 1

        /call MobTarget ${npcid}
        /click left target
        /for s 1 to 40
            /if (${Window[GiveWnd].Open}) /break
            /delay 1
        /next s
        /if (!${Window[GiveWnd].Open}) {
            /call Basic_AutoInventory
            /echo Symbol: Cannot open trade window
            /return
        }
		/notify GiveWnd GVW_Give_Button leftmouseup
        /for s 1 to 40
            /if (!${Window[GiveWnd].Open}) /break
            /delay 1
        /next s
		/if (${Window[GiveWnd].Open}) {
            /echo Symbol: Cannot close trade window
			/return
		}
        /for s 1 to 8
            /call Basic_AutoInventory
            /delay 2
            /if (!${Bool[${Cursor.ID}]}) /break
        /next s
		/echo Giving ${itemName} success
        /varcalc gave ${gave}+1
    /next i

    /keypress CLOSE_INV_BAGS
    /bc [+g+]Symbol: done. Got ${gave} symbol(s)

/RETURN


| autodonate
#event autodonate "<#1#> Donate"
SUB EVENT_autodonate(line, chatSender)
    /if (${Me.Name.NotEqual[${chatSender}]}) /return

    /if (!${Bool[${Ini["e4_globalsettings\donate.ini,donate items,item1"]}]}) {
        /echo AutoDonate: no donation item list
        /return
    }

    /if (${Bool[${Cursor.ID}]}) {
        /echo AutoDonate: ${Cursor.Name} in my hand. Aborting...
        /return
    }

    /call moveToTributeMaster FALSE
    /if (${Macro.Return} == -1) {
        /return
    }
    /call openTributeMasterWindow ${Macro.Return}

    /keypress OPEN_INV_BAGS
    /delay 3

    /declare totalvalue int local 0

    /declare i int local
    /declare itemName string local
    /for i 1 to ${donate_maxitem}
        /varset itemName ${Ini["e4_globalsettings\donate.ini,donate items,item${i}"]}
        /if (!${Bool[${itemName}]}) /continue
        |/echo debug --- ${itemName}, ${Bool[${FindItem[=${itemName}].ID}]}
        /if (!${Bool[${FindItem[=${itemName}].ID}]}) {
            |/echo AutoDonate: Cannot find ${itemName}
            /continue
        }
        /if (${FindItem[=${itemName}].ItemSlot} < 23) {
            /continue
        }
        /if (${Bool[${Cursor.ID}]}) {
            /echo AutoDonate: ${Cursor.Name} in my hand. end auto donate.
            /return
        }
        /if (!${Window[TributeMasterWnd].Open}) {
            /echo AutoDonate: Tribute Master Window is not open
            /return
        }
        /call donateSelectedItem "${itemName}"
        /varcalc totalvalue ${totalvalue}+${Macro.Return}
    /next i

    /keypress CLOSE_INV_BAGS
    /if (${Window[TributeMasterWnd].Open}) {
        /windowstate TributeMasterWnd close
    }
    /bc [+g+]AutoDonate: done. Total tribute gain: ${totalvalue}
/RETURN

SUB donateSelectedItem(string itemName)
    /declare tvalue ${FindItem[=${itemName}].Tribute}
    /if (${tvalue} == 0) /return 0
    /declare i int local
    /for i 1 to 4
        /nomodkey /itemnotify in pack${Math.Calc[${FindItem[=${itemName}].ItemSlot}-22].Int} ${Math.Calc[${FindItem[=${itemName}].ItemSlot2} + 1].Int} leftmouseup
        /delay 3
        /if (${Window[TributeMasterWnd].Child[TMW_DonateButton].Enabled}) /break
    /next i
    /if (${Window[TributeMasterWnd].Child[TMW_ValueLabel].Text} > 0) {
        /echo AutoDonate: <${itemName}> --> value: ${Window[TributeMasterWnd].Child[TMW_ValueLabel].Text}
        /nomodkey /notify TributeMasterWnd TMW_DonateButton leftmouseup
        /echo AutoDonate: <${itemName}> donated.
        /delay 2
    } else {
        /echo AutoDonate: cannot donate ${itemName}
        /varset tvalue 0
    }
/RETURN ${tvalue}

SUB openTributeMasterWindow(tid)
    /invoke ${Spawn[${tid}].RightClick}
    /declare i int local
    /for i 1 to 30
        /delay 1
        /if (${Window[TributeMasterWnd].Open}) /return
    /next i
/RETURN

SUB moveToTributeMaster(bool symbol)
    /declare whotodonate string local
    /if (${Zone.ID} == 345) {
        /varset whotodonate Melody the Singer of Great Deeds
    } else /if (${Zone.ID} == 386) {
        /varset whotodonate Fento
    } else /if (${Zone.ID} == 202) {
        /if (${symbol}) {
            /varset whotodonate Klorg
        } else {
            /varset whotodonate Enva
        }
    } else {
        /echo AutoDonate: Unknown zone ${Zone.Name} (${Zone.ID})
        /return -1
    }
    /if (${Spawn[${whotodonate}].Distance} > 10 && !${Spawn[${whotodonate}].LineOfSight}) {
        /echo AutoDonate: ${whotodonate} is too far away or out of sight ${Spawn[${whotodonate}].Distance} ${Spawn[${whotodonate}].LineOfSight}
        /return -1
    }
    /target ${whotodonate}
    /if (!${Target.CleanName.Find[${whotodonate}]}) {
        /echo AutoDonate: Cannot target ${whotodonate}. ${Target.CleanName}
        /return -1
    }
    /declare tid int local ${Target.ID}
    /delay 2
    /call Movement_ToID_Blocking ${tid}
/RETURN ${tid}

#event addAutoDonateRelay "<#1#> AddDonate"
SUB event_addAutoDonateRelay(line, ChatSender)
    /if (${ChatSender.NotEqual[${Me}]}) /return
    /if (!${Bool[${Cursor.ID}]}) /return

    /declare itemName string local ${Cursor.Name}
    /if (${FindItem[=${itemName}].Tribute} == 0) {
        /echo AutoDonate: No tribute points for ${itemName}
    }

    /declare dataName string local
    /declare i int local
    /for i 1 to ${donate_maxitem}
        /varset dataName ${Ini["e4_globalsettings\donate.ini,donate items,item${i}"]}
        /if (!${Bool[${dataName}]}) {
            /break
        }
        /if (${dataName.Equal[${itemName}]}) {
            /echo AutoDonate: ${itemName} Already registered
            /call Basic_AutoInventory
            /return
        }
    /next i

    /echo AutoDonate: Adding ${itemName}(${FindItem[=${itemName}].Tribute}) to donation list
    /ini "e4_globalsettings\donate.ini" "donate items" "item${i}" "${itemName}"
    /delay 2
    /call Basic_AutoInventory
/RETURN
