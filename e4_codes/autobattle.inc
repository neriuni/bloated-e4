SUB AutoBattle_Setup

    | Control
    /declare AutoBattling bool outer
    /declare AutoBattle_AllowControl bool outer

    | Parameter
    /declare autobattle_distance int outer 120

    | Alias

    | Internal
	/declare autobattle_pause timer outer
	/if (!${Defined[ABtotal]}) /declare ABtotal int global 0
	/declare ababdebug bool outer FALSE

	/declare ABwatchdog timer outer

    /call iniToVarV "${CharacterIni},Auto Battle,Role (Tank/Attacker/Off)" autobattle_role string outer
    /if (!${Macro.Return}) /declare autobattle_role string outer off
    /echo AB: My role is ${autobattle_role}
	/if (${IsTank}) {
		/if (${autobattle_role.NotEqual[Tank]}) {
			/varset IsTank FALSE
		}
	}
	/declare AB_ROLE_OFF int outer 0
	/declare AB_ROLE_TANK int outer 1
	/declare AB_ROLE_ATTACKER int outer 2
	/declare autobattle_rolecode int outer
	/if (${autobattle_role.Equal[off]}) {
		/varset autobattle_rolecode ${AB_ROLE_OFF}
	} else /if (${autobattle_role.Equal[tank]}) {
		/varset autobattle_rolecode ${AB_ROLE_TANK}
	} else /if (${autobattle_role.Equal[attacker]}) {
		/varset autobattle_rolecode ${AB_ROLE_ATTACKER}
	}

    /call iniToVarV "${CharacterIni},Auto Battle,Non-Tank" autobattle_nontank string outer
    /if (!${Macro.Return}) /declare autobattle_nontank string outer

    /call Core_AddTask_LowMain AutoBattle_LowMain
    /call Core_AddTask_ZoneChange AutoBattle_ZoneChange
    /call Core_AddTask_MobSlain AutoBattle_MobSlain
	/call Core_AddTask_EndCombat AutoBattle_EndCombat

/RETURN

SUB AutoBattle_Main
    /doevents AutoBattle
    /doevents stopBattle
    /doevents reTarget
    /if (${AutoBattling}) /call autobattle_check
/RETURN

SUB AutoBattle_LowMain
    /doevents RoleChange
/RETURN

SUB AutoBattle_ZoneChange
	/if (${AutoBattling}) {
		/varset AutoBattling FALSE
		/varset AutoBattle_AllowControl FALSE
		/echo AB: End of combat.

		/varset cruise_battle FALSE
		/call Combat_BackOff
	}
/RETURN


SUB AutoBattle_MobSlain(Killed, Killer)
	/if (${Bool[${NetBots[${Killer}].ID}]}) {
		/varcalc ABtotal ${ABtotal}+1
		/echo \ag Mob(${Killed}) Killed by ${Killer}: ${ABtotal}
	}

	/if (${AutoBattling}) {
		/call Core_checkcombat
		/if (${InCombat}) {
			/if (!${Bool[${Spawn[${AttackingTarget}].ID}]}) {
				/call autobattle_check
			} else /if (${IsPetClass}) {
				/if (!${Bool[${Pet.Target.ID}]}) {
					/call Core_checkcombat
					/if (${InCombat}) {
						/if (${combat_pettank}) {
							/call Combat_PetTank ${AttackingTarget}
						} else {
							/call Combat_PetAttack ${AttackingTarget}
						}
					}
				}
			}
		} else {
			| battle ends
			/call Combat_BackOff
			/varset AutoBattling FALSE
			/varset AutoBattle_AllowControl FALSE

			/varset cruise_battle FALSE
			/echo AB: End of Combat(2)
			/return
		}
	}

/RETURN

SUB AutoBattle_EndCombat
	/if (${AutoBattling}) {
		/varset AutoBattling FALSE
		/varset AutoBattle_AllowControl FALSE
		/echo AB: End of combat.
		/if (${IsTank}) {
			/varset Buff_PausePeriod 1s
			/if (${Group.Leader.Name.Equal[${Me}]}) {
				/call Bind_NetGroupButMeCommand "/echo <${Me.Name}> StopBattle"
			}
		}

		/varset cruise_battle FALSE
		/call Combat_BackOff
		/doevents flush AutoBattle
	}
/RETURN

|**

            caller callee target
Combat:     -      auto   1
Battle:     auto   auto   1

AutoCombat: -      auto   all
AutoBattle: auto   auto   all

**|

| autocombat (with ${Target})
|	tank and attacker both attacks ${Target} first
| autocombat (w/o ${Target})
|	tank attacks the nearest mob
| autocombat mobid
|	tank and attacker both attacks mobid first
| autocombat NULL
|	tank attacks the nearest mob
#EVENT AutoBattle "<#1#> AutoBattle"
#EVENT AutoBattle "<#1#> AutoBattle #2#"
#EVENT AutoBattle "<#1#> AutoCombat"
#EVENT AutoBattle "<#1#> AutoCombat #2#"
#EVENT AutoBattle "<#1#> CruiseBattle #2#"
SUB EVENT_AutoBattle(line, chatSender, targetID)
	/if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return

	/if (${AutoBattling}) {
		/if (${Defined[targetID]}) {
			/if (${Attacking} && ${AttackingTarget} == ${targetID}) /return
		} else {
			/return
		}
	}

    /declare i int local
    /declare tid int local -1

	/if (${autobattle_rolecode} == ${AB_ROLE_TANK}) {
		/if (!${Defined[targetID]}) {
			/declare targetID int local -1
			/if (${Target.ID} != 0) {
				/varset targetID ${Target.ID}
			}
		} else {
			/if (${Int[${targetID}]} == 0 || ${Spawn[${targetID}].Type.Equal[PC]}) {
				/varset targetID -1
			}
		}

		/if (${targetID} != -1) {
			/call Basic_CheckMob ${targetID}
			/if (!${Macro.Return}) /varset targetID -1
		}

		/if (${targetID} == -1) {
			/call NextTargetA -1
			/if (${Macro.Return} < 0) {
				/call NextTargetB -1
			}
			/varset targetID ${Macro.Return}
		}

		/if (${targetID} == -1) {
			/echo \ar AB: No mob around to fight!!!!
			/return
		}

		/varset tid ${targetID}

	} else {
		/if (!${Defined[targetID]}) {
			/declare targetID int local -1
		} else {
			/if (${Int[${targetID}]} == 0) {
				/varset targetID -1
			}
		}
		/if (${targetID} != -1) {
			/call Basic_CheckMob ${targetID}
			/if (!${Macro.Return}) /varset targetID -1
		}

		/if (${targetID} == -1) {
			/call AttackerNextTarget -1 ${autobattle_distance}
			/if (${Macro.Return} == -1) {
				/echo \ar AB: No mob around to fight!!!!
				/return
			}
			/varset tid ${Macro.Return}
		} else {
			/varset tid ${targetID}
		}
	}
	/if (${line.Find[Cruise]}) {
		/if (${Following}) /call Movement_Stop
		/varset Buff_Pause FALSE
		/varset cruise_battle TRUE
	}
    /if (${autobattle_rolecode} == ${AB_ROLE_TANK}) {

		/echo AB: Attacking targetting mob ${Spawn[id ${tid}].Name}
		/if (!${line.Find[Combat]}) {
			/call Combat_tryAttack ${tid} TRUE
			/varset AutoBattle_AllowControl TRUE
			/varset AutoBattling TRUE
			/varset ABwatchdog 5s
			/varset autobattle_pause 1s
		} else {
			/call Combat_tryAttack ${Target.ID} FALSE
			/varset AutoBattle_AllowControl FALSE
			/varset AutoBattling FALSE
			/varset ABwatchdog 5s
			/varset autobattle_pause 1s
		}
    } else /if (${autobattle_rolecode} == ${AB_ROLE_ATTACKER}) {
		/if (${combat_stick} == ${COMBAT_STICK_OFF} || ${combat_stick} == ${COMBAT_STICK_RANGED}) {
			/varset autobattle_distance 200
		}
		/echo AB: Attacking targetting mob ${tid}
		/call Combat_tryAttack ${tid} TRUE
		/varset AutoBattle_AllowControl TRUE
		/varset AutoBattling TRUE
		/varset autobattle_pause 1s
    }
/RETURN

#bind ABwatch /abwatchdog
SUB Bind_ABwatch(string chatSender, int tid, bool cb)
	/if (!${E4_Loaded}) /return
	/if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return
	/if (${chatSender.Equal[${Me}]}) /return

	/if (${autobattle_rolecode} != ${AB_ROLE_TANK} && ${autobattle_rolecode} != ${AB_ROLE_OFF}) {
		/if (!${AutoBattling}) {

			/if (${Bool[${Me.Mount.ID}]}) /dismount

			/echo AB: Attacking targetting mob ${tid}
			/if (!${Spawn[${tid}].LineOfSight}) {
				/call travel_seeid ${tid}
			}
			/call Combat_tryAttack ${tid} TRUE
			/varset AutoBattle_AllowControl TRUE
			/varset AutoBattling TRUE
			/varset autobattle_pause 1s

			/varset cruise_battle ${cb}
		} else /if (${InCombat}) {
			/declare ABmobID int local ${AttackerNextTarget[-1,${autobattle_distance}]}
			/if (${ABmobID} < 0) {
				/if (!${Group.Leader.LineOfSight}) {
					/call travel_seeid ${Group.Leader.ID}
				}
			}
		}
	}
/RETURN

SUB autobattle_check
	/if (${autobattle_pause}) /return
	/declare i int local
	/declare ABmobID int local
	/declare ABoldID int local
	/declare tcls int local

	| Check attacking PC's pet
	/if (${Attacking}) {
		/if (${Bool[${Spawn[${AttackingTarget}].Master.Type.Equal[PC]}]}) {
			/varset Attacking FALSE
			/varset AttackingTarget -1
		}
	}

	| watch dog
	/if (${Attacking}) {
		/if (${Group.Leader.Name.Equal[${Me.Name}]}) {
			/if (${ABwatchdog} == 0 && ${Target.ID} != 0) {
				| watch dog
				/call Bind_NetGroupButMeCommand "/ABwatchdog ${Me} ${Target.ID} ${cruise_battle}"
				/varset ABwatchdog 10s
			}
		}
	}

	| Tank
	/if (${autobattle_rolecode} == ${AB_ROLE_TANK}) {
		/if (${Attacking}) {
			/if (${Target.ID} && ${Me.PctAggro} < 100 && ${Target.PctHPs} > 0) {
				/varset tcls ${Target.AggroHolder.Class.ID}
				/if (${tcls} == ${ID_CLASS_WAR} || ${tcls} == ${ID_CLASS_SHD} || ${tcls} == ${ID_CLASS_PAL}) {
					/varset ABoldID ${Target.ID}
					/varset ABmobID ${NextTargetA[${Target.ID}]}
					/if (${ABmobID} < 0) /varset ABmobID ${NextTargetB[${Target.ID}]}
					/if (${ABmobID} != ${Target.ID} && ${ABmobID} >= 0 && !${autobattle_nontank.Find[${Target.AggroHolder.Name}]}) {
						/echo AB: \at Other tank is working ${Target.ID}. Changing to ${ABmobID} ${Spawn[${ABmobID}].Name}
						/call Combat_tryAttack ${ABmobID} TRUE
						/varset autobattle_pause 5
					} else {
						/varset autobattle_pause 5
					}
				} else {
					/varset autobattle_pause 5
				}
			} else {
				/varset ABmobID ${NextTargetA[-1]}
				/if (${ABmobID} >= 0) {
					/echo AB: \ay Found lower hate mob. Changing to ${ABmobID} ${Spawn[${ABmobID}].Name}
					/call Combat_tryAttack ${ABmobID} TRUE
					/varset autobattle_pause 5
				} else {
					/if (${Me.SecondaryPctAggro} < 75) {
						/if (${cruise_state} > 0) {
							/if (${wander_enable} && !${cruise_passivepull}) {
								| attempt to pull another mob
								/call cruise_getnearestcalm
								/if (${Macro.Return} != -1) {
									/if (${Stick.Active}) /squelch /stick off
									/call Cruise_Pull ${Macro.Return}
								}
							}
						}
						/varset ABmobID ${NextTargetB[-1]}
						/if (${ABmobID} >= 0  && ${Target.ID} != ${ABmobID}) {
							/echo AB: Found higher HP mob. Changing to ${ABmobID} ${Spawn[${ABmobID}].Name}
							/call Combat_tryAttack ${ABmobID} TRUE
							/varset autobattle_pause 5
						} else {
							/varset autobattle_pause 5
						}
					} else {
						/varset autobattle_pause 5
					}
				}
			}
		} else {
			/varset ABmobID ${NextTargetA[-1]}
			/if (${ABmobID} < 0) /varset ABmobID ${NextTargetB[-1]}
			/if (${ABmobID} >= 0) {
				/echo AB: \at Found new mob. Tanking ${ABmobID}
				/call Combat_tryAttack ${ABmobID} TRUE
				/varset autobattle_pause 5
			} else /if (${Target.ID} && ${Target.Aggressive} && ${Target.PctHPs} > 0 && !${Bool[${Target.Dead}]} && ${Target.Distance} < ${autobattle_distance}) {
				/echo AB: \am Tanking current target: ${Target.ID}, mobID: ${ABmobID}
				/call Combat_tryAttack ${Target.ID} FALSE
				/varset autobattle_pause 5
			} else /if (${InCombat}) {
				| try to attack mob out of AB distance
				/if (${cruise_state} > 0) {
					/declare tdist int local 1000
					/varset ABmobID -1
					/for i 1 to 13
						/call Basic_CheckXTMob ${i}
						/if (!${Macro.Return}) /continue
						|/if (!${Me.XTarget[${i}].LineOfSight}) /continue
						|/if (${Me.XTarget[${i}].Moving}) /continue
						/if (${Spawn[${Me.XTarget[${i}].ID}].Distance} < ${tdist}) {
							/varset ABmobID ${Me.XTarget[${i}].ID}
							/varset tdist ${Me.XTarget[${i}].Distance}
						}
					/next i
					/if (${ABmobID} != -1) {
						/echo AB: \at Found new far mob. Tanking ${ABmobID}
						/if (${Spawn[${ABmobID}].Distance} > 200) {
							/call travel_goidex ${ABmobID} 150
						}
						/call Combat_tryAttack ${ABmobID} TRUE
						/varset autobattle_pause 5
					} else {
						/echo AB: No mob to fight around me. Waiting...
						/varset autobattle_pause 15
					}
				} else {
					/echo AB: Mob is not near. Waiting...
					/varset autobattle_pause 15
				}
			} else {
				/echo AB: end of combat
				/varset AutoBattling FALSE
				/varset AutoBattle_AllowControl FALSE

				/varset cruise_battle FALSE
				/call Combat_BackOff
			}
		}

	| Attacker
	} else /if (${autobattle_rolecode} == ${AB_ROLE_ATTACKER}) {
		/if (!${Attacking}) {
			/varset ABmobID ${AttackerNextTarget[-1,${autobattle_distance}]}
			/if (${ABmobID} >= 0) {
				/echo AB: \at Found new mob. Attacking ${ABmobID}
				/call Combat_tryAttack ${ABmobID} TRUE
				/varset autobattle_pause 15
			} else /if (${InCombat}) {
				/if (${cruise_battle}) {
					/if (${Group.Leader.Distance} > 100) {
						/echo AB: Moving to Tank position
						/call travel_close ${Group.Leader.ID} 100
						/if (!${Macro.Return}) {
							/varset autobattle_pause 10
						}
					}
				} else {
					/echo AB: Mob is not near. Waiting... distance: ${autobattle_distance}
					/varset autobattle_pause 15
				}
			} else {
				/echo AB: end of combat
				/varset AutoBattling FALSE
				/varset AutoBattle_AllowControl FALSE

				/varset cruise_battle FALSE
				/call Combat_BackOff
			}
		} else {
			/if (${combat_attacktypecode} == ${COMBAT_ATK_OFF}) {
				/if (${Math.Rand[1]} == 1 && !${Skill_NoAE}) {
					/if (${Target.PctHPs} < 50) {
						/call AttackerNextTarget -1 ${autobattle_distance}
						/if (${Macro.Return} != ${AttackingTarget}) {
							/varset AttackingTarget ${Macro.Return}
						}
						/varset autobattle_pause 15
					}
				}
			}
		}
	}
/RETURN

#EVENT stopBattle "<#1#> StopBattle"
SUB EVENT_stopBattle(line, ChatSender)
    /if (${Group.Members}) {
    	/if (${Group.Leader.Name.NotEqual[${ChatSender}]}) {
            /return
        }
    } else /if (${ChatSender.NotEqual[${Me}]}) {
		/return
	}

	/if (${AutoBattling}) /echo AB: Aborting
	/varset AutoBattling FALSE
    /varset AutoBattle_AllowControl FALSE

	/varset cruise_battle FALSE

    /call Combat_EndCombat
    /call Combat_BackOff
/RETURN

#EVENT reTarget "<#*#> Retarget"
SUB EVENT_retarget
    /call Combat_EndCombat
	/if (${autobattle_rolecode} == ${AB_ROLE_ATTACKER}) {
		/if (${AutoBattling}) /varset Attacking FALSE
	}
/RETURN

#EVENT RoleChange "<#1#> Role #2# #3#"
SUB EVENT_RoleChange(line, chatSender, whoBot, newRole)
    /if (!${whoBot.NotEqual[${Me}]}) /return
    /if (${newRole.Find[Tank]}) {
        /varset autobattle_role Tank
		/varset autobattle_rolecode ${AB_ROLE_TANK}
    } else /if (${newRole.Find[Attacker]}) {
        /varset autobattle_role Attacker
		/varset autobattle_rolecode ${AB_ROLE_ATTACKER}
    } else /if (${newRole.Find[Off]}) {
        /varset autobattle_role Off
		/varset autobattle_rolecode ${AB_ROLE_OFF}
    }
/RETURN

SUB NextTargetA(int ignoretarget)
	/declare i int local
	/declare nextMobId int local -1

	| variables for lowest hate target
	/declare LHmobid int local -1
	/declare LHmobhp int local -1
	/declare LHmobdist int local 1000

	/for i 1 to 13
		/call Basic_CheckXTMob ${i}
		/if (!${Macro.Return}) /continue

		/if (${Me.XTarget[${i}].ID} == ${ignoretarget}) /continue
		/if (${Me.XTarget[${i}].PctAggro} >= 100) /continue
		/if (${Me.XTarget[${i}].Distance} > ${autobattle_distance}) /continue

		/if (${Me.XTarget[${i}].Distance} > 50) {
			/if (!${Me.XTarget[${i}].LineOfSight}) /continue
		}
		/if (${Me.XTarget[${i}].Moving}) /continue
		/if (${Me.XTarget[${i}].Distance} > ${LHmobdist}) /continue
		/if (${LHmobhp} < ${Me.XTarget[${i}].PctHPs}) {
			/varset LHmobdist ${Me.XTarget[${i}].Distance}
			/varset LHmobid ${Me.XTarget[${i}].ID}
			/varset LHmobhp ${Me.XTarget[${i}].PctHPs}
		}
	/next i

/RETURN ${LHmobid}

SUB NextTargetB(int ignoretarget)
	/declare i int local
	/declare g int local

	/declare HPmobid int local
	/declare HPmobhp int local
	/declare HPmobdst int local


	/declare HPmobcount int local

	/varset HPmobid -1
	/varset HPmobhp -1
	/varset HPmobdst 1000

	/declare tmobid int local -1

	/varset HPmobcount ${SpawnCount[npc radius 200]}
	/for i 1 to ${HPmobcount}
		/varset tmobid ${NearestSpawn[${i},npc].ID}
		/call Basic_CheckMob ${tmobid}
		/if (!${Macro.Return}) /continue

		/if (${tmobid} == ${ignoretarget}) /continue
		/if (!${Spawn[${tmobid}].LineOfSight}) /continue
		/if (!${Spawn[${tmobid}].Aggressive}) /continue
		| do not tank pet
		/if (${Bool[${Spawn[${tmobid}].Master.ID}]}) /continue
		/if (${Spawn[${tmobid}].PctHPs} < ${HPmobhp}) /continue

		/if (${Spawn[${tmobid}].Distance} < 50) {
			/if (${HPmobhp} > 90) {
				/return ${Spawn[${tmobid}].ID}
			}
			/varset HPmobid ${Spawn[${tmobid}].ID}
			/varset HPmobhp ${Spawn[${tmobid}].PctHPs}
			/varset HPmobdst ${Spawn[${tmobid}].Distance}
		} else /if (${HPmobdst} > ${Spawn[${tmobid}].Distance}) {
			/varset HPmobid ${Spawn[${tmobid}].ID}
			/varset HPmobhp ${Spawn[${tmobid}].PctHPs}
			/varset HPmobdst ${Spawn[${tmobid}].Distance}
		}
	/next i
	/if (${HPmobid} != -1) {
		/return ${HPmobid}
	}

/RETURN -1

| returns back spawn ID which Attacker should target next
SUB AttackerNextTarget(int ignoretarget, int antdistance)
	/declare i int local

	/declare WKmoblv int local 1000
	/declare WKmobId int local -1
	/declare WKmobhp int local 100
	/declare WKmobdist int local 1000

	/if (${combat_attacktypecode} == ${COMBAT_ATK_OFF}) {
		/varset WKmobhp -1
		/for i 1 to 13
			/call Basic_CheckXTMob ${i}
			/if (!${Macro.Return}) /continue

			/if (${Me.XTarget[${i}].ID} == ${ignoretarget}) /continue
			/if (${Me.XTarget[${i}].Distance} > 50) /continue
			/if (${Me.XTarget[${i}].PctHPs} > 70) {
				/varset WKmobId ${Me.XTarget[${i}].ID}
				/break
			} else /if (${Me.XTarget[${i}].PctHPs} > 50 && ${Me.XTarget[${i}].PctHPs} > ${WKmobhp}) {
				/varset WKmobId ${Me.XTarget[${i}].ID}
				/varset WKmobhp ${Me.XTarget[${i}].PctHPs}
			} else /if (${WKmobId} == -1) {
				/varset WKmobId ${Me.XTarget[${i}].ID}
				/varset WKmobhp ${Me.XTarget[${i}].PctHPs}
			}
		/next i

		/if (${WKmobId} != -1) /return ${WKmobId}

		/varset WKmobhp -1
		/for i 1 to 13
			/call Basic_CheckXTMob ${i}
			/if (!${Macro.Return}) /continue

			/if (${Me.XTarget[${i}].ID} == ${ignoretarget}) /continue
			/if (${Me.XTarget[${i}].Distance} > ${antdistance}) /continue
			/if (${Me.XTarget[${i}].Level} < ${WKmoblv}) {
				/varset WKmoblv ${Me.XTarget[${i}].Level}
				/varset WKmobhp ${Me.XTarget[${i}].Distance}
				/varset WKmobId ${Me.XTarget[${i}].ID}
			} else /if (${Me.XTarget[${i}].Level} == ${WKmoblv} && ${Me.XTarget[${i}].Distance} < ${WKmobdist}) {
				/varset WKmoblv ${Me.XTarget[${i}].Level}
				/varset WKmobhp ${Me.XTarget[${i}].Distance}
				/varset WKmobId ${Me.XTarget[${i}].ID}
			}
		/next i
	} else {
		/for i 1 to 13
			/call Basic_CheckXTMob ${i}
			/if (!${Macro.Return}) /continue

			/if (${Me.XTarget[${i}].Distance} > 50) /continue
			/if (${Me.XTarget[${i}].ID} == ${ignoretarget}) /continue
			/if (${Me.XTarget[${i}].PctHPs} <= 20) {
				/varset WKmobId ${Me.XTarget[${i}].ID}
				/break
			} else /if (${Me.XTarget[${i}].Level} < ${WKmoblv}) {
				/varset WKmoblv ${Me.XTarget[${i}].Level}
				/varset WKmobhp ${Me.XTarget[${i}].PctHPs}
				/varset WKmobId ${Me.XTarget[${i}].ID}
			} else /if (${Me.XTarget[${i}].Level} == ${WKmoblv} && ${Me.XTarget[${i}].PctHPs} < ${WKmobhp}) {
				/varset WKmoblv ${Me.XTarget[${i}].Level}
				/varset WKmobhp ${Me.XTarget[${i}].PctHPs}
				/varset WKmobId ${Me.XTarget[${i}].ID}
			}
		/next i

		/if (${WKmobId} != -1) /return ${WKmobId}

		/for i 1 to 13
			/call Basic_CheckXTMob ${i}
			/if (!${Macro.Return}) /continue

			/if (${Me.XTarget[${i}].ID} == ${ignoretarget}) /continue
			/if (${Me.XTarget[${i}].Distance} > ${antdistance}) /continue
			/if (${Me.XTarget[${i}].Moving}) /continue
			|/if (!${Spawn[id ${Me.XTarget[${i}]}].LineOfSight}) /continue

			/if (${Me.XTarget[${i}].PctHPs} <= 20) {
				|/varset WKmoblv ${Me.XTarget[${i}].Level}
				|/varset WKmobhp ${Me.XTarget[${i}].PctHPs}
				/varset WKmobId ${Me.XTarget[${i}].ID}
				/break
			} else /if (${Me.XTarget[${i}].Level} < ${WKmoblv}) {
				/varset WKmoblv ${Me.XTarget[${i}].Level}
				/varset WKmobhp ${Me.XTarget[${i}].PctHPs}
				/varset WKmobId ${Me.XTarget[${i}].ID}
			} else /if (${Me.XTarget[${i}].Level} == ${WKmoblv} && ${Me.XTarget[${i}].PctHPs} < ${WKmobhp}) {
				/varset WKmoblv ${Me.XTarget[${i}].Level}
				/varset WKmobhp ${Me.XTarget[${i}].PctHPs}
				/varset WKmobId ${Me.XTarget[${i}].ID}
			}
		/next i
	}
/RETURN ${WKmobId}

