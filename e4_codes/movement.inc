SUB Movement_Setup
    | Global
	/if (!${Defined[Following]}) /declare Following bool global FALSE
    /varset Following FALSE
	/declare FollowingTarget string outer


    | Parameter
    /declare movement_moveto_distance int outer 5

    /declare movement_bark_tries int outer 4

    | Alias
	/noparse /squelch /alias /rtz /netgroup "/echo <${Me.Name}> rtz ${Me.Heading.Degrees} ${Me.Y} ${Me.X} ${Me.Z}"
    /noparse /squelch /alias /followme /netgroup "/echo <${Me.Name}> Follow"
    /noparse /squelch /alias /stop /netgroup "/echo <${Me.Name}> Stop"
    /noparse /squelch /alias /clickit /netgroup "/echo <${Me.Name}> ClickIt"
    /noparse /squelch /alias /bark /netall "/echo <${Me.Name}> ${Target.ID} RawBark"
    /noparse /squelch /alias /opendoor /echo <${Me.Name}> OpenDoor
    /noparse /squelch /alias /mtm /netgroup "/echo <${Me.Name}> Move to ${Me.ID}"
    /noparse /squelch /alias /movetome /netgroup "/echo <${Me.Name}> Move to ${Me.ID}"
    /noparse /squelch /alias /mtt /netgroup "/echo <${Me.Name}> Move to ${Target.ID}"
    /noparse /squelch /alias /movetotarget /netgroup "/echo <${Me.Name}> Move to ${Target.ID}"
    /noparse /squelch /alias /PortTo /netgroup "/echo <${Me.Name}> port"
    /noparse /squelch /alias /Anchor /netgroup "/echo <${Me.Name}> Anchor"
    /noparse /squelch /alias /Evac /netgroup "/echo <${Me.Name}> Evac"
    /noparse /squelch /alias /fastevac /netgroup "/rawfastevac"
    /noparse /squelch /alias /faststop /netgroup "/rawfaststop ${Me.Name}"
    /noparse /squelch /alias /fastfollow /netgroup "/rawfastfollow ${Me.Name}"
    /noparse /squelch /alias /fasthalt /netgroup "/rawfasthalt ${Me.Name}"


    | Internal
    /declare movement_barkName string outer
    /declare movement_anchorName string outer NULL

    /declare Movement_StopCalled bool outer FALSE

    /call Core_AddTask_LowMain Movement_LowMain
    |/call Core_AddTask_MobSlain Movement_MobSlain
    /call Core_AddTask_EndCombat Movement_EndCombat

/RETURN

SUB Movement_Main
    /doevents Follow
    /doevents Stop
    /doevents OpenDoor
    /doevents Anchor
    /doevents Evacuate
    /doevents MoveHere
    /doevents MoveLoc
    /doevents MakeMeFast
    /doevents Halt
/RETURN

SUB Movement_LowMain
    /doevents RTZ
    /doevents Bark
    /doevents PortTo
    /doevents GoHome
    /doevents CreateExpedition

    /if (${Following}) {
        /if (${Me.Feigning}) /stand
        /if (${Me.Ducking}) /stand
        /if (${AdvPath.Status} != 1) {
            /if (${Spawn[${NetBots[${FollowingTarget}].ID}].LineOfSight}) {
                /bc [+y+]Follow: Follow stopped. Try to refollow
                /call Movement_Follow ${FollowingTarget}
            } else /if (${NetBots[${FollowingTarget}].InZone}) {
                /bc [+y+]Follow: Target(${FollowingTarget}) lost. Abort following
                /call Movement_Stop
            } else {
                /bc [+r+]Follow: I am stuck! Come back and refollow!
                /call Movement_Stop
            }
        }
    }
/RETURN

SUB Movement_EndCombat
    /if (!${Following}) /call Movement_AnchorMove
/RETURN

|SUB Movement_MobSlain(Killed, Killer)
|/RETURN

#EVENT CreateExpedition "<#1#> Expedition"
SUB EVENT_CreateExpedition(line, chatSender)
    /if (!${InGroup[${chatSender}]}) /return
    /if (!${Group.Leader.Name.Equal[${Me}]}) /return

    /if (!${Defined[movement_iscreated]}) {
        /declare movement_iscreated bool outer FALSE
    }

	/target Rokuth
	/doevents flush CreateExpedition
	/if (${Target.CleanName.Equal[Rokuth]}) {
        /call Bind_NetGroupCommand "/dzquit"
        /delay 5
        /say create
        /delay 5
	} else {
		/echo CreateExpedition: Cannot find NPC
	}

/RETURN

| back off and stop and anchor
#bind FastHalt /rawfasthalt
SUB Bind_FastHalt(string chatSender, string WhotoAnchor)
	/if (!${E4_Loaded}) /return
    /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return

    /call Combat_BackOff

    /call Movement_Stop

    /if (${chatSender.Equal[${Me}]}) /return

    /if (${Defined[WhotoAnchor]}) {
        /if (${NetBots[${WhotoAnchor}].ID}) {
            /echo Movement: Anchor: set to ${WhotoAnchor}
            /varset movement_anchorName ${WhotoAnchor}
            /call Movement_AnchorMove
        }
    } else {
        /if (${movement_anchorName.Length} > 0) /call Movement_AnchorMove
    }
/return


#EVENT Halt "<#1#> Halt"
#EVENT Halt "<#1#> Halt #2#"
SUB EVENT_Halt(line, chatSender, WhotoAnchor)
    /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return
    /call Combat_BackOff

    /call Movement_Stop

    /if (${chatSender.Equal[${Me}]}) /return

    /if (${Me.Feigning}) /stand

    /if (${Defined[WhotoAnchor]}) {
        /if (${NetBots[${WhotoAnchor}].ID}) {
            /echo Movement: Anchor: set to ${WhotoAnchor}
            /varset movement_anchorName ${WhotoAnchor}
            /call Movement_AnchorMove
        }
    } else {
        /if (${movement_anchorName.Length} > 0) /call Movement_AnchorMove
    }

/RETURN

| Make me fast
#Event MakeMeFast "<#1#> makemefast"
SUB EVENT_MakeMeFast(line, chatSender)
    /if (${Me.Invis}) /return
    /call Travel_MakeMeFast TRUE
/RETURN

| Port to
#EVENT PortTo "#1# tells the group, 'port #2#'"
#EVENT PortTo "<#1#> port #2#"
SUB EVENT_PortTo(line, chatSender, whereTo)
    /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return

    /call travel_meetup

    /declare spellName string local
    /declare spellType string local

    /if (${whereTo.Equal[seru]}) {
        /if (${Bool[${FindItem[=Arx Key].ID}]}) {
            /varset spellName Arx Key
            /varset spellType Item
        }
    } else /if (${whereTo.Equal[time]}) {
        /if (${Bool[${FindItem[=Secrets of the Planes].ID}]}) {
            /varset spellName Secrets of the Planes
            /varset spellType Item
        }
    } else /if (${IsDRU}) {
        /if (${whereTo.Equal[aoa]} || ${whereTo.Equal[anguish]}) {
            /if (${Bool[${FindItem[=Everspring Slippers of the Tangled Briars].ID}]}) {
                /varset spellName Everspring Slippers of the Tangled Briars
                /varset spellType Item
            }
        }
    } else /if (${IsWIZ}) {
        /if (${whereTo.Equal[aoa]} || ${whereTo.Equal[anguish]}) {
            /if (${Bool[${FindItem[=Academic's Slippers of the Arcanists].ID}]}) {
                /varset spellName Academic's Slippers of the Arcanists
                /varset spellType Item
            }
        } else /if (${whereTo.Equal[all]}) {
            /varset spellName Teleport
            /varset spellType Spell
        }
    }
    /if (${IsWIZ} || ${IsDRU}) {
        /if (!${Bool[${spellName}]}) {
            /if (${whereTo.Equal[bind]}) {
                /varset spellName Teleport Bind
                /varset spellType AA
            } else {
                /varset spellName ${Ini[e4_globalsettings\Spell Aliases.ini,${Me.Class},Port ${whereTo}]}
                /varset spellType Spell
            }
        }
    }

    /if (${Bool[${spellName}]}) {
        /bc [+g+]Movement: Port to: ${whereTo}(${spellName})
        /if (${Me.Gem[${spellName}]}) {
            /while (${Me.SpellInCooldown}) {
                /delay 1
            }
        }
        /varset Buff_PausePeriod 30s
        /while (${Me.Moving}) {
            /delay 1
        }
        /while (${Me.SpellInCooldown}) {
            /delay 1
        }
        /delay 5
        /varset Skill_Verbal TRUE
        /call Skill_Cast ${Me.ID} "${spellName}"
        /varset Skill_Verbal FALSE
        /if (${Macro.Return} == ${CAST_SUCCESS}) {
            /echo \ag Movement: Port Done(${Macro.Return})
        } else {
            /echo \ag Movement: Port Failed(${Macro.Return})
        }
        /doevents flush PortTo
    }
/RETURN

| Move to
#EVENT MoveHere "<#1#> Move to #2#"
SUB EVENT_MoveHere(line, chatSender, targetID)
    /if (!${Int[${targetID}]}) /return
    /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return

    /if (!${Spawn[${targetID}].ID}) /return
    /doevents flush MoveHere

    /call Movement_Stop

    /call Movement_ToID_Blocking ${targetID}
/RETURN

#Event MoveLoc "<#1#> MoveLoc #2#"
SUB EVENT_MoveLoc(line, chatSender, toLoc)
    /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return

    /doevents flush MoveLoc

    /call Movement_Stop

    /call Movement_ToLoc_Blocking ${toLoc}
/RETURN

| Follow
#bind FastFollow /rawfastfollow
SUB Bind_FastFollow(string chatSender)
	/if (!${E4_Loaded}) /return
    /if (${chatSender.Equal[${Me}]}) /return
    /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return

    /if (${Spawn[${chatSender}].Distance} > 400 && !${Spawn[${chatSender}].LineOfSight}) {
        /bc [+r+] Movement: Follow: ${chatSender} is too far away(${Spawn[${chatSender}].Distance}). Abort following
        /return
    }

    /call Movement_Follow ${chatSender}
/return

#EVENT Follow "<#1#> Follow"
#EVENT Follow "<#1#> Follow #2#"
SUB EVENT_Follow(line, chatSender, eventParams)
|    /if (${Following}) /return
    /if (${chatSender.Equal[${Me}]}) /return
    /if (!${NetBots[${chatSender}].InZone}) /return

    /doevents flush Follow

    /declare flagfollow bool local FALSE
    /if (!${Defined[eventParams]}) {
        /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return

        /varset flagfollow TRUE
    } else {
        /if (${eventParams.Find[${Me}]}) /varset flagfollow TRUE
        /if (${eventParams.Find[All]}) /varset flagfollow TRUE
    }
    /if (${flagfollow}) {
        /if (${Spawn[${chatSender}].Distance} > 400 && !${Spawn[${chatSender}].LineOfSight}) {
            /bc [+r+] Movement: Follow: ${chatSender} is too far away(${Spawn[${chatSender}].Distance}). Abort following
            /return
        }
        /call Movement_Follow ${chatSender}
    }
/RETURN

SUB Movement_Follow(whotoFollow)
    /if (${Window[LootWnd].Open}) {
        /notify LootWnd DoneButton leftmouseup
    } else /if (${Bool[${Me.Casting.ID}]} && !${IsBRD}) {
        /call Skill_Interrupt
    }

    /if (${Me.Feigning}) {
        /stand
    }
    /if (${Stick.Active}) {
        /squelch /stick off
    }

    | Setting related to buff.inc
    /varset Buff_PausePeriod 2s
    /varset Buff_BegProgress 0

    /if (${Bool[${Spawn[${whotoFollow}].ID}]}) {
        /if (!${Spawn[${whotoFollow}].LineOfSight}) {
            /if (!${Window[LootWnd].Open}) {
                /call travel_goid ${Spawn[pc ${whotoFollow}].ID}
            }
        }
    }

    |/call MobTarget ${NetBots[${whotoFollow}].ID}
    /squelch /afollow spawn ${NetBots[${whotoFollow}].ID} on nodoor
    /varset Following TRUE
    /varset FollowingTarget ${whotoFollow}
/RETURN


| Stop
#bind FastStop /rawfaststop
SUB Bind_FastStop
	/if (!${E4_Loaded}) /return
    /call Movement_Stop
/return

#EVENT Stop "<#1#> Stop"
SUB EVENT_Stop(line, chatSender)
    /if (!${NetBots[${chatSender}].InZone}) /return
    /call Movement_Stop
/RETURN

SUB Movement_Stop
    /if (${Stick.Active}) {
        /squelch /stick off
    }
    /if (${AdvPath.Following}) /squelch /afollow off
    /if (${Navigation.Active}) /squelch /nav stop
    /moveto off
    /varset Following FALSE
    /varset FollowingTarget NULL
    /varset Movement_StopCalled TRUE
/RETURN

| MovetoLoc
SUB Movement_ToLoc_Blocking(float yLoc, float xLoc, float zLoc)
	/if (${Math.Distance[${yLoc},${xLoc}]} <= ${movement_moveto_distance}) {
        /moveto off
        /return TRUE
    }

    | blocking part
    /declare oldpos string local ${Me.Loc}
    /declare stuckflag int local 0
	/declare i int local
    /declare retry bool local FALSE

    /call travel_goloc ${yLoc} ${xLoc} ${zLoc}
    /if (${Macro.Return}) /return TRUE

    /echo ToLoc: moving to ${yLoc} ${xLoc} ${zLoc}
    :tolocblockstart
	/squelch /moveto loc ${yLoc} ${xLoc} dist ${Math.Calc[${movement_moveto_distance}-2]}
	/for i 1 to 240
		/if (${Math.Distance[${yLoc},${xLoc}]} <= ${movement_moveto_distance}) {
			/moveto off
			/return TRUE
		}
    	/delay 1
        /if (${Math.Distance[${oldpos}]} <= 10) {
            /if (${stuckflag} > 30) {
                /echo ToLoc: Looks like Im stuck. ${Math.Distance[${yLoc},${xLoc}]}, ${Math.Distance[${oldpos}]}
                /if (${Me.Feigning}) /stand
                /if (${IsSHM}) {
                    /call Buff_RemoveShortBuff "Inconspicuous Totem"
                }
                /moveto off
                /if (!${retry}) {
                    /echo ToLoc: Retrying by reloading MQ2moveutils
                    /squelch /plugin MQ2MoveUtils unload
                    /squelch /plugin MQ2MoveUtils
                    /varset stuckflag 0
                    /varset retry TRUE
                    /goto :tolocblockstart
                } else {
                    /echo ToLoc: Aborting...
                    /return FALSE
                }
            } else {
                /varcalc stuckflag ${stuckflag}+1
            }
        }
        /varset oldpos ${Me.Loc}
	/next i
    /echo ToLoc: Cannot reach to destination. Abort moving. ${Math.Distance[${yLoc},${xLoc}]}
	/moveto off
/RETURN FALSE

| MovetoID
SUB Movement_ToID_Blocking(int targetID)
    /if (!${Bool[${targetID}]}) {
        /echo ToLoc: ToID: Unknown target
        /return FALSE
    }
    /call Movement_ToLoc_Blocking ${Spawn[${targetID}].Y} ${Spawn[${targetID}].X} ${Spawn[${targetID}].Z}
/RETURN ${Macro.Return}


| rtz
#EVENT RTZ "<#1#> rtz #2# #3#"
SUB EVENT_RTZ(line, starter, float degree, string loc)
    /if (!${CheckGroupInZoneOrMe[${starter}]}) /return
    /if (${Skill_memorizing}) /varset Skill_memorizing FALSE
	/if (${Me.Name.Equal[${starter}]}) /delay 1s
	/echo RTZ: Attempting to run through zone

    /declare i int local
    /declare g int local

	/declare locToStart string local ${loc}
	/declare startHeading float local ${degree}

	/declare startZone int local ${Zone.ID}
	| -Check that you can see the location to start from.
	/if (!${LineOfSight[${Me.Loc.Replace[ ,]}:${locToStart}]}) {
		/echo RTZ: I cannot see the starting location.
        /return
	}
    | -Check range to object.
    /if (${Math.Distance[${locToStart}]} > 200) {
        /echo RTZ: I am to far away from that zone line.
    }
    /call Movement_Stop
    /if (${Bool[${Me.Casting.ID}]} && !${IsBRD}) /call Skill_Interrupt

    /for i 1 to 4
    	/if (${Bool[${Me.Mount.ID}]}) /dismount
        | -Check if I need to move closer.
        /if (${Math.Distance[${locToStart}]} > 10) /call Movement_ToLoc_Blocking ${locToStart}
        | -Face the zone and run foward for 6 seconds.
        /squelch /face fast heading ${Math.Calc[${startHeading}*-1]}
        /delay 2
        /echo RTZ: running ${i}/4
        /keypress forward hold
        | -Wait to zone.
        /for g 1 to 60
            /delay 1
            /if (!${Zone.ID} || ${Zone.ID} != ${startZone}) {
                /keypress forward
                /break
            } else /if (${Following}) {
                /keypress forward
                /break
            }
        /next g
        /keypress forward

        | -Check if I zoned.
        /if (${Zone.ID} != ${startZone}) {
            /echo RTZ: Succeed
            /return
        }
    /next i
    /echo RTZ: I failed to run through the zoneline in [${Zone}].
    /call Movement_ToLoc_Blocking ${locToStart}
/RETURN

| Bark
#EVENT Bark "<#1#> #2# RawBark #3#"
SUB EVENT_Bark(line, chatSender, targetID, barkMsg)
    /if (!${NetBots[${chatSender}].InZone}) /return
    /if (${Spawn[${targetID}].Distance} > 200) /return
    |/if (!${InGroup[${chatSender}]}) /return
    /if (!${barkMsg.Length}) /return
    /if (${Skill_memorizing}) /varset Skill_memorizing FALSE
    /if (${chatSender.Equal[${Me}]}) /delay 3s

    /declare i int local
    /declare g int local

    | is this guy barkable?
    /varset movement_bark_tries 4
    /if (${Spawn[${targetID}].Type.Equal[PC]}) {
        /echo Bark: You cannot bark PC.
    } else /if (${Zone.ID} == 202) {
        /if (${Spawn[${targetID}].CleanName.Find[Valium]}) {
            /varset movement_bark_tries 2
        } else /if (${Spawn[${targetID}].CleanName.Find[Magus]}) {
            /varset movement_bark_tries 2
        } else /if (${Spawn[${targetID}].CleanName.Find[Priest of Luclin]}) {
            /varset movement_bark_tries 1
        }
    } else /if (${Spawn[${targetID}].CleanName.Find[Rokuth]}) {
        /varset movement_bark_tries 2
    } else /if (${Spawn[${targetID}].CleanName.Find[Klonopin]}) {
        /varset movement_bark_tries 2
    }


    /varset movement_barkName ${Spawn[${targetID}].Name}

    /echo Bark: Barking to ${Spawn[${targetID}].CleanName}

    /declare startingLoc string local
	/declare startZone int local ${Zone.ID}
    /declare barkdone bool local FALSE
    /declare myid int local ${Me.ID}

    /if (${Spawn[${targetID}].Distance} > 200) {
        /echo Bark: Failed - ${Spawn[${targetID}].CleanName} is > 200 distance
        /return
    }

    /call Movement_Stop
    /if (${Bool[${Me.Casting.ID}]} && !${IsBRD}) /call Skill_Interrupt

    /for i 1 to ${movement_bark_tries}
        /if (${Me.Sneaking}) {
            /doability Sneak
            /delay 5
        }
        /if (${Me.Invis}) {
            /makemevisible
            /delay 5
        }

        /if (${targetID} != ${Target.ID}) /call MobTarget ${targetID}
        /if (${Target.Distance} > 10) /call Movement_ToID_Blocking ${Target.ID}
        /varset startingLoc ${Me.Loc}

        /echo Bark: Attempt ${i}/${movement_bark_tries}
        /say ${barkMsg}

        /for g 1 to 20
            /delay 1
        	/doevents Loading
            /doevents Entering
            /if (${movement_barkName.NotEqual[${Spawn[${targetID}].Name}]}) {
                /varset barkdone TRUE
                /break
            } else /if (${myid} != ${Me.ID}) {
                /varset barkdone TRUE
                /break
            } else /if (${Math.Distance[${startingLoc}]} > 20 || ${Zone.ID} != ${startZone} || ${Window[ConfirmationDialogBox].Open} || ${Window[LargeDialogWindow].Open}) {
                /varset barkdone TRUE
                /break
            } else /if (${Following}) {
                /varset barkdone TRUE
                /break
            }
        /next g

        | -Click Yes to a confirmation box.
        /if (${Window[ConfirmationDialogBox].Open}) {
            /notify ConfirmationDialogBox Yes_Button leftmouseup
            /delay 1s
        } else /if (${Window[LargeDialogWindow].Open}) {
            /notify LargeDialogWindow LDW_YesButton leftmouseup
            /delay 1s
        }

        /doevents charFlag

        /if (${barkdone}) {
            /doevents Entering
            /echo Bark: Succeed
            /return
        }
    /next i

    /echo Bark: End
/RETURN


| Open Door
#EVENT OpenDoor "<#1#> OpenDoor"
#EVENT OpenDoor "<#1#> Clickit"
#EVENT OpenDoor "<#1#> Clickit #2#"
SUB EVENT_OpenDoor(line, chatSender, int doorID)
    /if (${line.Find[clickit]}) {
        /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return
    } else {
        /if (${chatSender.NotEqual[${Me}]}) /return
    }
    /if (${Skill_memorizing}) /varset Skill_memorizing FALSE
    /if (${Defined[doorID]}) {
        /echo OpenDoor: attempt to click door ${doorID}
        /doortarget id ${doorID}
    } else {
        /doortarget
    }
    /if (${Zone.ID} == 202) {
        /if (${DoorTarget.Name.Equal[PORTBASE]}) {
            /doortarget PORTARENA
        }
    } else /if (${Zone.ID} == 35) {
        /if (${DoorTarget.Name.Equal[HUT1]}) {
            /doortarget RUJPORTAL701
        }
    } else /if (${Zone.ID} == 39) {
        /if (${DoorTarget.Name.Equal[PAROCK101]}) {
            /doortarget PAROCK105
        }
    } else /if (${Zone.ID} == 316) {
        /if (${DoorTarget.Name.Equal[PGFLOORPLATE]}) {
            /doortarget MMLTTM
        }
    } else /if (${Zone.ID} == 291) {
        /if (${DoorTarget.Name.Equal[TMTELEPORT800]}) {
            /doortarget NIHALTAR801
        }
    } else /if (${Zone.ID} == 203) {
        /if (${DoorTarget.Name.Equal[POTORMFNT501]}) {
            /doortarget POTORMFNT500
        }
    }
    /click left door
/RETURN

| GoHome
|- Attempt to go PoK or Temple of Marr
#EVENT GoHome "<#1#> GoHome"
#EVENT GoHome "<#1#> GoMarr"
#Event GoHome "<#1#> blackout"
SUB EVENT_GoHome(line, ChatSender)
    /if (!${ChatSender.Equal[${Me}]}) /return
    /if (${line.Find[Home]}) {
        /call Bind_NetGroupCommand "/echo <${Me.Name}> zone pok"
    } else /if (${line.Find[Marr]}) {
        /call Bind_NetGroupCommand "/echo <${Me.Name}> zone marr"
    } else /if (${line.Find[blackout]}) {
        /if (${Bool[${FindItem[=Blackout Brew].ID}]}) {
            /echo Movement: So if by the time the bar closes
            /call Bind_NetGroupCommand "/echo <${Me.Name}> qself Blackout Brew"
        }
    }
/RETURN

| Evac
#bind FastEvac /rawfastevac
SUB Bind_FastEvac
	/if (!${E4_Loaded}) /return
    /if (${IsDRU} || ${IsWIZ}) {

        /declare evacspellname string local NULL
        /if (${Me.AltAbilityTimer[Exodus]} == 0) {
            /varset evacspellname Exodus
        } else /if (${Me.Book[Succor]}) {
            /varset evacspellname Succor
        } else /if (${Me.Book[Lesser Succor]}) {
            /varset evacspellname Lesser Succor
        } else /if (${Me.Book[Evacuate]}) {
            /varset evacspellname Evacuate
        } else /if (${Me.Book[Lesser Evacuate]}) {
            /varset evacspellname Lesser Evacuate
        }

        /if (${Bool[${evacspellname}]}) {
            /call AutoBattle_EndCombat
            /call Combat_BackOff
            /call Movement_Stop
            /stopcast

            /bc [+r+]Evacuating!! (${evacspellname})
            /call Skill_Enqueue ${Me.ID} "${evacspellname}"
        }
    }
/RETURN

#EVENT Evacuate "<#1#> Evac"
SUB EVENT_Evacuate(line, chatSender)
    /if (${IsDRU} || ${IsWIZ}) {
        /if (!${CheckGroupInZoneOrMe[${chatSender}]}) /return
        /if (${Skill_memorizing}) /varset Skill_memorizing FALSE

        /declare evacspellname string local NULL
        /if (${Me.AltAbilityTimer[Exodus]} == 0) {
            /varset evacspellname Exodus
        } else /if (!${Me.Gem[Succor]} && ${Bool[${FindItem[=Shield of Coral].ID}]} && ${IsDRU}) {
            /varset evacspellname Shield of Coral
        } else /if (${Me.Book[Succor]}) {
            /varset evacspellname Succor
        } else /if (${Me.Book[Lesser Succor]}) {
            /varset evacspellname Lesser Succor
        } else /if (${Me.Book[Evacuate]}) {
            /varset evacspellname Evacuate
        } else /if (${Me.Book[Lesser Evacuate]}) {
            /varset evacspellname Lesser Evacuate
        }

        /if (${Bool[${evacspellname}]}) {
            /call AutoBattle_EndCombat
            /call Combat_BackOff
            /call Movement_Stop

            /bc [+r+]Evacuating!! (${evacspellname})
            /call Skill_Enqueue ${Me.ID} "${evacspellname}"
        }
    }
/RETURN


| Anchor
#EVENT Anchor "<#1#> Anchor"
#EVENT Anchor "<#1#> Anchor #2#"
SUB EVENT_Anchor(line, ChatSender, WhotoAnchor)
    /if (${ChatSender.Equal[${Me}]}) /return
    /if (!${CheckGroupInZoneOrMe[${ChatSender}]}) /return

    /if (${Defined[WhotoAnchor]}) {

        /if (${WhotoAnchor.Equal[off]}) {
            /echo Movement: Anchor: disable
            /varset movement_anhorName NULL
        } else /if (${NetBots[${WhotoAnchor}].ID}) {
            /echo Movement: Anchor: set to ${WhotoAnchor}
            /varset movement_anchorName ${WhotoAnchor}
            /call Movement_AnchorMove
        }
    } else {
        /if (${movement_anchorName.Length} > 0) /call Movement_AnchorMove
    }
/RETURN

SUB Movement_AnchorMove
    /if (!${Bool[${movement_anchorName}]}) /return
    /if (${movement_anchorName.Equal[${Me}]}) /return
    /if (!${CheckGroupInZoneOrMe[${movement_anchorName}]}) /return

    /if (!${NetBots[${movement_anchorName}].ID}) {
        /echo Movement: Anchor: unknown anchor(${movement_anchorName}). Aborting
        /varset movement_anchorName NULL
    } else {
        /if (${Spawn[${NetBots[${movement_anchorName}].ID}].Distance} > 200) /return
        /echo Movement: Anchor: Moving to ${movement_anchorName}
        /call Movement_Stop
        /call Utilities_petghold
        /call Movement_ToID_Blocking ${NetBots[${movement_anchorName}].ID}
    }
/RETURN
