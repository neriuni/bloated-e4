SUB Rez_Setup

    | Control
    /declare Rez_wait4rez bool outer FALSE

    | Parameter

    | Alias
	/noparse /squelch /alias /wait4rez /echo <${Me.Name}> Wait4Rez
	/noparse /squelch /alias /lootCorpses /echo <${Me.Name}> Loot Now

    | Internal
    /declare rez_progress int outer
    /call Core_AddTask_LowMain Rez_LowMain

    /declare rez_waiting timer outer

    /declare canRez int outer 0
    /declare CorpseListSize int outer 0
    /declare corpseList string outer

/RETURN

SUB Rez_Main
    /doevents dead
/RETURN

SUB Rez_LowMain
    /doevents AE_Rez

    /doevents waitNow
    /doevents lootNow
    /if (${Rez_wait4rez}) {
        /if (${rez_waiting} == 0) {
            /bc [+r+]oo oOo oOoOoo(I'm dead and waiting for ressurection...)
            /varset rez_waiting 30s
        }
        /call rez_wait4rezcheck
    }
/RETURN


#EVENT AE_Rez "<#1#> #2# AERez#3#"
|#EVENT AE_Rez "#1# tells you, 'AERez'"
SUB EVENT_AE_Rez(line, ChatSender, whoCast, whoRez)
    /declare rezSpell string local
    /declare numIterations int local
    /declare nomana bool local FALSE

    /if (!${whoCast.Equal[all]}) {
        /if (${Me.Class.ShortName.NotEqual[${whoCast}]} && ${whoCast.NotEqual[${Me}]}) /return
    }

    /if (${IsCLR}) {
        /if (${Bool[${FindItem[=Water Sprinkler of Nem Ankh].ID}]}) {
            /varset rezSpell Water Sprinkler of Nem Ankh
            /varset nomana TRUE
        } else /if (${Me.AltAbility[Blessing of Resurrection].ID}) {
            /varset rezSpell Blessing of Resurrection
            /varset nomana TRUE
        } else /if (${Me.Book[Spiritual Awakening]}) {
            /varset rezSpell Spiritual Awakening
        } else /if (${Me.Book[Reviviscence]}) {
            /varset rezSpell Reviviscence
        } else /if (${Me.Book[Resurrection]}) {
            /varset rezSpell Resurrection
        } else /if (${Me.Book[Restoration]}) {
            /varset rezSpell Restoration
        } else /if (${Me.Book[Resuscitate]}) {
            /varset rezSpell Resuscitate
        } else /if (${Me.Book[Renewal]}) {
            /varset rezSpell Renewal
        } else /if (${Me.Book[Revive]}) {
            /varset rezSpell Revive
        } else /if (${Me.Book[Reparation]}) {
            /varset rezSpell Reparation
        } else /if (${Me.Book[Reconstitution]}) {
            /varset rezSpell Reconstitution
        } else /if (${Me.Book[Reanimation]}) {
            /varset rezSpell Reanimation
        } else {
            /bc [+r+]AEREZ: I do not have a resurrection spell
            /return
        }
        /varset numIterations 2
    } else {
        /if (${Bool[${FindItem[=Token of Resurrection].ID}]}) {
            /varset rezSpell Token of Resurrection
            /varset numIterations 1
            /varset nomana TRUE
        } else /if (!${InCombat}) {
            /if (${IsDRU} || ${IsSHM}) {
                /if (${Me.Book[Incarnate Anew]}) {
                    /varset rezSpell Incarnate Anew
                } else {
                    /bc [+r+]AEREZ: I do not have a resurrection spell
                    /return
                }
                /varset numIterations 2
            } else /if (${IsPAL}) {
                /if (${Me.AltAbility[Gift of Resurrection].ID}) {
                    /varset rezSpell Gift of Resurrection
                    /varset nomana TRUE
                } else /if (${Me.Book[Resurrection]}) {
                    /varset rezSpell Resurrection
                } else /if (${Me.Book[Restoration]}) {
                    /varset rezSpell Restoration
                } else /if (${Me.Book[Renewal]}) {
                    /varset rezSpell Renewal
                } else /if (${Me.Book[Revive]}) {
                    /varset rezSpell Revive
                } else /if (${Me.Book[Reparation]}) {
                    /varset rezSpell Reparation
                } else /if (${Me.Book[Reconstitution]}) {
                    /varset rezSpell Reconstitution
                } else /if (${Me.Book[Reanimation]}) {
                    /varset rezSpell Reanimation
                } else {
                    /bc [+r+]AEREZ: I do not have a resurrection spell
                    /return
                }
                /varset numIterations 2
            } else {
                /return
            }
        } else {
            /return
        }
    }

    /if (${whoRez.Length} == 0) {
        /varset whoRez all
    }
    /call BuildCorpseList "${whoRez}"
    /if (!${CorpseListSize}) {
        /echo AEREZ: I couldn't find any corpses to resurrect...
    } else {
        /varset Skill_memorizing FALSE
        /call Movement_Stop

        /declare i int local
        /declare n int local
        /declare g int local
        /declare playerName string local
        /declare corpseID int local
        /declare trycount int local 0

        /for n 1 to ${numIterations}
            /for i 1 to ${CorpseListSize}
                /if (!${IsPAL} && ${IsTank} && ${InCombat}) {
                    /echo AEREZ: Combat detect. Aborting...
                    /return
                }
                /varset canRez 0
                /varset corpseID ${CorpseListPriority[${i}]}
                /varset playerName ${Spawn[id ${corpseID}].CleanName}
                /varset playerName ${playerName.Arg[1,']}

                /if (!${Bool[${Spawn[id ${corpseID}].CleanName}]}) {
                    /echo AEREZ: Cannot find Corpse id ${corpseID}. Skipping... ${playerName}
                    /goto :skipRez
                } else {
                    /echo AEREZ: Attempt ${Spawn[id ${corpseID}].CleanName}
                }

                /if (${corpseList.Find[${corpseID},]}) {
                    /echo I have already rezzed ${playerName}-${corpseID}
                    /goto :skipRez
                }
                /call MobTarget ${corpseID}
                /delay 1
                /call check_CorpseRezzable

                /if (${canRez}!=1) {
                    /echo ${Target.Name} is not eligible for rez
                    /goto :skipRez
                } else {
                    /tell ${playerName} Wait4Rez
                    /delay 3
                    /if (${Target.Distance} > 15 && ${Target.Distance} < 100) {
                        /corpse
                        /delay 5
                        /if (${Target.Distance} > 35) {
                            /tell ${playerName} skipping rez, corpse drag failed: ${Target.Distance} distance
                            /goto :skipRez
                        }
                    }

                    /bc AEREZ: Resurrecting-${corpseID} ${playerName}
                    /varset trycount 0
                    :checkReady
                    /if (${InCombat}) {
                        /if (${IsSHM} || ${IsDRU}) {
                            /if (${rezSpell.NotEqual[Token of Resurrection]}) {
                                /echo AEREZ: Combat detected. Aborting
                                /return
                            }
                        }
                    }
                    /if (!${Bool[${Spawn[${corpseID}].ID}]}) {
                        /echo AEREZ: target lost skipping ${playerName}
                        /goto :skipRez
                    }

                    /while (${Me.SpellInCooldown}) {
                        /delay 1
                    }
                    /if (!${nomana}) {
                        /if (${InCombat}) {
                            /echo AEREZ: Low mana. Aborting AEREZ
                            /return
                        }
                        /while (${Me.PctMana} < 60) {
                            /if (!${Me.Sitting}) /sit
                            /echo AEREZ: waiting for mana ${Me.PctMana}
                            /if (${Me.PctHPs} > 50) {
                                /if (${Bool[${FindItem[=Manastone].ID}]}) {
                                    /varset basic_ismanastone TRUE
                                }
                                /if (${Bool[${FindItem[=Corrupted Manastone].ID}]}) {
                                    /varset basic_iscorruptstone TRUE
                                }
                                /if (${basic_ismanastone} || ${basic_iscorruptstone}) {
                                    /call basic_ManastoneCheck
                                } else {
                                    /call basic_automedicheck
                                    /echo 1s
                                }
                            } else {
                                /call basic_automedicheck
                                /echo 1s
                            }
                        }
                    }

                    /call Skill_Cast ${corpseID} "${rezSpell}"
                    /if (${Macro.Return} == ${CAST_SUCCESS}) {
                        /if (${playerName.Equal[${Me}]}) {
                            /varset Rez_wait4rez TRUE
                            /varset rez_progress 0
                            | accept ressurection
                            /call rez_wait4rezcheck
                            | try to loot corpse
                            /call rez_wait4rezcheck
                        }
                        /varset corpseList ${corpseList}${corpseID},
                    } else /if (${Macro.Return} == ${CAST_INTERRUPTED}) {
                        /delay 5
                        /goto :checkReady
                    } else /if (${Macro.Return} == ${CAST_NOTREADY}) {
                        /echo AEREZ: waiting...  (${Macro.Return}) ${Spawn[id ${corpseID}].Name} ${corpseID} ${playerName}
                        /delay 5
                        /goto :checkReady
                    } else {
                        /if (${trycount} == 5) {
                            /echo AEREZ: too much attempt. trying next corpse... (${Macro.Return}) ${Spawn[id ${corpseID}].Name} ${corpseID} ${playerName}
                            /goto :skipRez
                        } else {
                            /echo AEREZ: rez failed (${Macro.Return}) ${Spawn[id ${corpseID}].Name} ${corpseID} ${playerName}
                            /varcalc trycount ${trycount}+1
                            /delay 5
                            /goto :checkReady
                        }
                    }
                }
                :skipRez
            /next i
            /call BuildCorpseList
            /varset corpseList
        /next n
        /bc AEREZ: All nearby corpses have been resurrected.
    }
/RETURN

Sub BuildCorpseList(string rezcls)
    /declare c int local
    /declare p int local 1
    /declare cID int local
    /declare CorpseList[100] int local -1

    /declare tcls int local

    /if (${Defined[CorpseListPriority]}) /deletevar CorpseListPriority
    /declare CorpseListPriority[100] int outer -1
    /varset CorpseListSize 0
    /if (${SpawnCount[pccorpse radius 100]} == 0) {
        /echo AEREZ: No corpse around.
        /return FALSE
    }
    /for c 1 to ${SpawnCount[pccorpse radius 100]}
        /varset cID ${NearestSpawn[${c},pccorpse radius 100].ID}
        /varcalc CorpseListSize ${CorpseListSize}+1
        /varset CorpseList[${CorpseListSize}] ${cID}
    /next c

    /if (${rezcls.Find[all]}) {
        |add clr
        /for c 1 to ${CorpseListSize}
            /if (${Spawn[id ${CorpseList[${c}]}].Class.ID} == ${ID_CLASS_CLR}) {
                /varset CorpseListPriority[${p}] ${CorpseList[${c}]}
                /varcalc p ${p}+1
            }
        /next c
        |add war/dru/shm
        /for c 1 to ${CorpseListSize}
            /varset tcls ${Spawn[id ${CorpseList[${c}]}].Class.ID}
            /if (${tcls} == ${ID_CLASS_DRU} || ${tcls} == ${ID_CLASS_SHM} || ${tcls} == ${ID_CLASS_WAR}) {
                /varset CorpseListPriority[${p}] ${CorpseList[${c}]}
                /varcalc p ${p}+1
            }
        /next c
        |add bst,enc,pal,rng,shd
        /for c 1 to ${CorpseListSize}
            /varset tcls ${Spawn[id ${CorpseList[${c}]}].Class.ID}
            /if (${tcls} == ${ID_CLASS_PAL} || ${tcls} == ${ID_CLASS_SHD} || ${tcls} == ${ID_CLASS_RNG} || ${tcls} == ${ID_CLASS_BST} || ${tcls} == ${ID_CLASS_ENC} || ${tcls} == ${ID_CLASS_MAG} || ${tcls} == ${ID_CLASS_WIZ} || ${tcls} == ${ID_CLASS_NEC}) {
                /varset CorpseListPriority[${p}] ${CorpseList[${c}]}
                /varcalc p ${p}+1
            }
        /next c
        |add the rest
        /for c 1 to ${CorpseListSize}
            /varset tcls ${Spawn[id ${CorpseList[${c}]}].Class.ID}
            /if (${tcls} == ${ID_CLASS_ROG} || ${tcls} == ${ID_CLASS_MNK} || ${tcls} == ${ID_CLASS_BER} || ${tcls} == ${ID_CLASS_BRD}) {
                /varset CorpseListPriority[${p}] ${CorpseList[${c}]}
                /varcalc p ${p}+1
            }
        /next c
        /echo AEREZ: rez list
        /for c 1 to ${CorpseListSize}
            /echo ${c}: ${Spawn[id ${CorpseListPriority[${c}]}].Name} class: ${Spawn[id ${CorpseListPriority[${c}]}].Class.ShortName}
        /next c
    } else {
        /for c 1 to ${CorpseListSize}
            /if (${rezcls.Find[${Spawn[id ${CorpseList[${c}]}].Class.ShortName}]}) {
                /varset CorpseListPriority[${p}] ${CorpseList[${c}]}
                /varcalc p ${p}+1
            }
        /next c
    }
/return TRUE

#event CorpseExpired "This corpse #1# be resurrected."
Sub Event_CorpseExpired(line, corpseExpired)
    /if (${corpseExpired.Equal[cannot]}) {
        /varset canRez 2
    } else {
        /varset canRez 1
    }
/return
Sub check_CorpseRezzable
    /declare i int local
    /varset canRez 0

    /doevents CorpseExpired flush
    /for i 1 to 4
        /consider
        /delay 2
        /doevents CorpseExpired
        /if (${canRez} != 0) {
            /return
        }
    /next i
/RETURN

#EVENT lootNow "<#1#> Loot Now"
SUB EVENT_lootNow(line, ChatSender)
    /if (${ChatSender.NotEqual[${Me}]}) /return

    /varset Rez_wait4rez TRUE
    /varset rez_progress 1
/RETURN

#EVENT dead	"You have been slain by #*#"
#EVENT dead	"You died."
SUB EVENT_dead
	/call Combat_BackOff

    /if (${Me.Level} > 11) {
        /bc [+r+]oo oOo oOoOoo(I'm dead and waiting for ressurection...)
        /consent group
        /delay 5
        /consent raid
        /delay 5
        /beep sounds/mail2
        /varset Rez_wait4rez TRUE
        /varset rez_progress 0
        /varset rez_waiting 10s
    }
/RETURN

#EVENT waitNow "#1# tells you, 'Wait4Rez'"
#EVENT waitNow "<#1#> Wait4Rez"
SUB EVENT_waitNow(line, ChatSender)
    /if (!${IsDead}) /return
    /if (!${Rez_wait4rez}) {
        /consent ${ChatSender}
        /delay 5
        /consent group
        /delay 5
        /consent raid
        /delay 5
        /bc [+r+] Waiting for rez.
        /varset Rez_wait4rez TRUE
        /varset rez_progress 0
        /varset rez_waiting 10s
    }
/RETURN

SUB rez_wait4rezcheck
    /declare i int local
    /if (${rez_progress} == 0) {
        /if (${Window[ConfirmationDialogBox].Open}) {
            :trypressyes
            /echo Rez: recieved rez spell
            /nomodkey /notify ConfirmationDialogBox Yes_Button leftmouseup
            /for i 1 to 10
                /if (!${Window[ConfirmationDialogBox].Open}) /break
                /delay 5
            /next i
            /if (!${Window[ConfirmationDialogBox].Open}) {
                /varset rez_progress 1
                /varset Buff_PausePeriod 60s
                /varset loot_PauseLoot 60s
            } else /if (${i} > 10) {
                /echo Rez: waiting ressurection...
                /goto :trypressyes
            }
        }
    } else /if (${rez_progress} == 1) {
        /varset Buff_PausePeriod 180s
        /varset loot_PauseLoot 180s
        /for i 1 to 10
            /if (${Spawn[${Me}'s].ID}) /break
            /delay 5
        /next i
        /if (${Spawn[${Me}'s].ID}) {
            /echo Rez: Looting...
            :Looting
            
            /if (!${Defined[lootTotal]}) /declare lootTotal int local 0
            /call MobTarget ${Spawn[${Me}'s].ID}
            /delay 1
            /if (${Target.Distance}<100 && ${Target.CleanName.Equal[${Me}'s corpse]}) {
            |Pull the corpse.
                /if (${Target.Distance}>15) /corpse
                /for i 1 to 8
                    /if (${Target.Distance}<15) /break
                    /delay 3
                /next i
                /loot
                /for i 1 to 8
                    /if (${Me.State.Equal[BIND]}) /break
                    /delay 3
                /next i
                /if (${Me.State.NotEqual[BIND]}) /goto :Looting
            |Wait for all the items on your corpse to appear in your loot window.
                /varset lootTotal -1
                /varset i 0
                :autoxplootlag
                /varcalc i ${i}+1
                /if (${lootTotal}!=${Corpse.Items}) {
                    /varset lootTotal ${Corpse.Items}
                    /delay 5
                    /if (${i}<50) /goto :autoxplootlag
                }
            |Loot All Items.
                /notify LootWnd LootAllButton leftmouseup
                /for i 1 to 40
                    /if (!${Window[LootWnd].Open}) /break
                    /delay 1s
                /next i
            |Check you don't still have a corpse.
                /if (!${Me.State.Equal[BIND]}) {
                    /for i 1 to 8
                        /if (!${Bool[${Spawn[${Me}'s].ID}]}) /break
                        /delay 5
                    /next i
                    /if (${Spawn[${Me}'s].ID}) /goto :Looting
                }
                /bc [+g+]Rez: Ready to die again!
                /if (${Bool[${FindItem[=Manastone].ID}]}) {
                    /varset basic_ismanastone TRUE
                } else /if (${Bool[${FindItem[=Corrupted Manastone].ID}]}) {
                    /varset basic_iscorruptstone TRUE
                }
                /varset rez_progress 0
                /varset Rez_wait4rez FALSE
                /varset Buff_PausePeriod 5s
            } else {
                /echo Rez: My corpse is too far away(${Target.Distance}). Get closer!
            }
        } else {
            /echo Rez: Cannot find my corpse
        }
    }
/RETURN

