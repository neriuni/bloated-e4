SUB Buff_Setup

    | *** set to global
    /if (!${Defined[Buff_Pause]}) {
        /declare Buff_Pause bool global FALSE
    } else {
        /varset Buff_Pause FALSE
    }
    /declare Buff_PausePeriod timer outer

    /declare Buff_BegProgress int outer

    | Parameter
    /declare Buff_RecastTime int outer 12

    | Alias
    /noparse /squelch /alias /BuffMe /netgroup "/echo <${Me.Name}> Buff me"

    | internal
    /declare buff_Begger string outer
    /declare buff_firsttime bool outer TRUE

    /declare buff_pos int outer 1

    /declare buff_illusioncheck timer outer 0

    /declare buff_caststatus int outer

    /call IniToArrayV "${CharacterIni},Buffs,Buff#" buff_List
    /call IniToArrayV "${CharacterIni},Buffs,Beg Buff#" buff_BegList

    /call IniToArrayV "${CharacterIni},Buffs,Pet Block#" buff_PetBlock

    /call iniToVarV "${CharacterIni},Misc,Default Gem8 Spell" buff_defaulteight string outer
    /if (${Macro.Return}) {
        /declare buff_defaulteightenable bool outer TRUE
    } else {
        /declare buff_defaulteightenable bool outer FALSE
    }

    /call iniToVarV "${CharacterIni},Misc,Pet God Illusion" buff_petgodname string outer
    /if (${Macro.Return}) {
        /call Skill_CreateData ITEM_GODILL "Metamorph Totem: Ancient Gods"
        /declare buff_petgodenable int outer
        /if (${buff_petgodname.Equal[any]}) {
            /varset buff_petgodenable 1
        } else {
            /varset buff_petgodenable 2
        }
    } else {
        /declare buff_petgodenable int outer 0
    }

    /call buff_AuraSetup
    /call Core_AddTask_LowMain Buff_LowMain
    /call Core_AddTask_ZoneChange Buff_ZoneChange

    /if (${Defined[buff_List]}) {
        /declare i int local
        /for i 1 to ${buff_List.Size}
            /call Skill_CreateData buff_auto${i} "${buff_List[${i}]}"
        /next i
    }

    | timer for manaflare
    /if (${IsENC}) {
        /declare buff_encmanaflare timer outer 30s
    }

/RETURN

SUB Buff_Main
    /call Skill_AmIReady
    /if (${Macro.Return} != ${CAST_SUCCESS}) /return
    /call buff_Check
/RETURN

SUB Buff_LowMain
    /call Skill_AmIReady
    /if (${Macro.Return} != ${CAST_SUCCESS}) /return

    /doevents buff_BegBuff
    /call buff_BegCheck
    /call buff_AuraCheck
    /call buff_BlockRemove
    /doevents buff_RemoveGMBuff
/RETURN

SUB Buff_ZoneChange
    /if (${buff_firsttime}) {
        /varset buff_firsttime FALSE
        /return
    }
    /declare i int local
    /declare tooltiptxt string local
    /declare removedbuffcount int local 0

/RETURN

SUB Buff_UpdateMembers
    /declare i int local
    /if (${Defined[buff_List]}) {
        /for i 1 to ${buff_List.Size}
            |/echo buff ${buff_auto${i}_spellname}
            /call Skill_UpdateData buff_auto${i}
            |/echo --
        /next i
    }

/RETURN

#EVENT buff_RemoveGMBuff "<#*#> removegmbuff"
SUB EVENT_buff_RemoveGMBuff
    /echo Buff: Removing GM Buffs
    /call Buff_RemoveBuff "Odin's Opportune Onslaught"
    /call Buff_RemoveBuff "bootsy's boisterous bootstrutting"
    /call Buff_RemoveBuff "Hotshott's Honed Hardiness"
    /call Buff_RemoveBuff "Ralia's Radiant Rampart"
    /call Buff_RemoveBuff "KinglyKrab's Knowing Keenness"
    /call Buff_RemoveBuff "Dhaymion's Dauntless Divinity"
    /call Buff_RemoveBuff "Eris' Exquisite Endurance"
    /call Buff_RemoveBuff "Kage's Kinetic Kinesthesia"
    /call Buff_RemoveBuff "Gnock's Gracious Gift"
    /call Buff_RemoveBuff "It's Levitation, But Fancier!"
    /call Buff_RemoveBuff "Tenzing's Tiger Talisman"

    /if (${Me.Pet.ID}) {
        /removepetbuff Odin
        /removepetbuff bootsy
        /removepetbuff Hotshott
        /removepetbuff Ralia
        /removepetbuff KinglyKrab
        /removepetbuff Dhaymion
        /removepetbuff Eris
        /removepetbuff Kage
        /removepetbuff Gnock
        /removepetbuff It's
        /removepetbuff Tenzing
    }

/RETURN

| Dismiss familiar and remove illusion
SUB buff_BlockRemove
	/if (!${InCombat}) {
        /if (${buff_illusioncheck} == 0) {
            /if (${Bool[${Me.Pet.ID}]}) {
                /if (${Me.Pet.Race.ID} == ${ID_RACE_SNAKE} || ${Me.Pet.Race.ID} == ${ID_RACE_WARBOAR}) /pet get lost
                /if (${IsWIZ}) {
                    /if (${Me.Pet.Race.ID} == ${ID_RACE_DRAKE1} || ${Me.Pet.Race.ID} == ${ID_RACE_DRAKE2} || ${Me.Pet.Race.ID} == ${ID_RACE_DRAKE3}) {
                        /pet get lost
                    }
                }
                /if (${Me.Pet.Name.Find[s_familiar]}) {
                    /if (${Me.Pet.Race.ID} != ${ID_RACE_GNOMEWORK}) {
                        /pet get lost
                    }
                }
            }

            /if (${IsPetClass}) {
                /if (${Me.Pet.ID}) {
                    /if (${Defined[buff_PetBlock]}) {
                        /declare i int local
                        /for i 1 to ${buff_PetBlock.Size}
                            /if (${Me.PetBuff[${buff_PetBlock[${i}]}]}) {
                                /removepetbuff ${buff_PetBlock[${i}]}
                            }
                        /next i
                    }
                    /call Utilities_petghold
                } else /if (!(${IsBST} || ${IsMAG} || ${IsNEC} || ${IsSHM} || ${IsDRU} || ${IsSHD} || ${IsENC})) {
                    | looks like I lost goblin wizard pet
                    /varset IsPetClass FALSE
                }
            } else {
                /if (${Me.Pet.Class.ID} == 12) {
                    | I have goblin wizard pet from mystery box
                    /varset IsPetClass TRUE
                }
            }

            | id 43134
            /call Buff_RemoveBuff "Environmental Adaption Effect"

            /varset buff_illusioncheck 10s
        }
    }

/RETURN

| Beg Buff
#EVENT buff_BegBuff "<#*#> Tell from #1#: buff #2#"
#EVENT buff_BegBuff "<#1#> Buff #2#"
SUB EVENT_buff_BegBuff(line, chatSender, begger)
    /doevents flush buff_BegBuff
    /if (!${Defined[buff_BegList]}) /return
    |/if (!${IsSafe}) {
    |    /if (!${Bool[${NetBots[${chatSender}].ID}]}) {
    |        /tell ${chatSender} I'm busy. Try again in safe zone.
    |        /return
    |    }
    |}
    /if (${IsDead}) {
        /tell ${chatSender} I'm dead. Try again later.
        /return
    }
    /if (${InCombat}) {
        /tell ${chatSender} I'm in combat. Try again later.
        /return
    }
    /if (${Buff_Pause} || ${IsWindowOpen}) {
        /tell ${chatSender} I'm busy. Try again later.
        /return
    }
    /if (${Buff_BegProgress} == 0) {
        /declare whotobuff string local
        /if (${begger.Equal[me]}) {
            /varset whotobuff ${chatSender}
        } else {
            /varset whotobuff ${begger}
        }
        /if (!${SpawnCount[pc ${whotobuff}]}) {
            /tell ${chatSender} You(${whotobuff}) are not in same zone or not available.
            /return
        }
        /if (${Spawn[${whotobuff}].Distance} > 100) {
            /tell ${chatSender} You(${whotobuff}) are out of range!
            /return
        }

        /doevents flush buff_BegBuff
        /tell ${chatSender} Casting buffs on ${whotobuff}...
        /varset Buff_BegProgress 1
        /varset buff_Begger ${whotobuff}
    } else {
        /tell ${chatSender} I'm busy. Try again later.
    }
/RETURN

SUB buff_BegCheck
    /if (${Buff_PausePeriod} == 0) {
        /if (${Buff_BegProgress} > 0) {
            /if (${Buff_Pause}) {
                /varset Buff_BegProgress 0
                /bc [+r+]Buff: Beg: Pause detect. Aborting
                /return
            }
            /if (${InCombat}) {
                /varset Buff_BegProgress 0
                /bc [+r+]Buff: Beg: Im in combat. Aborting
                /return
            }
            /if (!${SpawnCount[pc ${buff_Begger}]}) {
                /varset Buff_BegProgress 0
                /bc [+r+]Buff: Beg: Lost target. Aborting
                /return
            }
            /if (${Spawn[${buff_Begger}].Distance} > 100) {
                /varset Buff_BegProgress 0
                /bc [+r+]Buff: Beg: Out of Range. Aborting
                /return
            }
            /if (${Me.Moving} && ${Following}) {
                /varset Buff_BegProgress 0
                /bc [+y+]Buff: Beg: Moving detect. Aborting
                /return
            }

            /declare spellresult string local
            /echo Buff: Beg: Casting ${buff_BegList[${Buff_BegProgress}].Arg[1,/]} to ${buff_Begger}(${Spawn[pc ${buff_Begger}].ID})
            /call Skill_Cast ${Spawn[pc ${buff_Begger}].ID} "${buff_BegList[${Buff_BegProgress}].Arg[1,/]}"
            /varset spellresult ${Macro.Return}
            /if (${spellresult} == ${CAST_SUCCESS} || ${spellresult} == ${CAST_TAKEHOLD}) {
                /if (${Buff_BegProgress} == ${buff_BegList.Size}) {
                    /varset Buff_BegProgress 0
                    /bc [+g+]Buff: Beg: done.
                } else {
                    /varcalc Buff_BegProgress ${Buff_BegProgress}+1
                }
                /varset Buff_PausePeriod 15
            }
        }
    }
/RETURN


| Aura settings
SUB buff_AuraSetup
    /declare buff_aura1 string outer
    /declare buff_auraenable bool outer FALSE

    /if (${IsWAR}) {
        /if (${Me.CombatAbility[Myrmidon's Aura]}) /varset buff_aura1 Myrmidon's Aura
        /if (${Me.CombatAbility[Champion's Aura]}) /varset buff_aura1 Champion's Aura
    } else /if (${IsMNK}) {
        /if (${Me.CombatAbility[Disciple's Aura]}) /varset buff_aura1 Disciple's Aura
        /if (${Me.CombatAbility[Master's Aura]}) /varset buff_aura1 Master's Aura
    } else /if (${IsBER}) {
        /if (${Me.CombatAbility[Aura of Rage]}) /varset buff_aura1 Aura of Rage
        /if (${Me.CombatAbility[Bloodlust Aura]}) /varset buff_aura1 Bloodlust Aura
    } else /if (${IsBRD}) {
        /if (${Me.Book[Aura of Insight]}) /varset buff_aura1 Aura of Insight
        /if (${Me.Book[Aura of the Muse]}) /varset buff_aura1 Aura of the Muse
    } else /if (${IsDRU}) {
        /if (${Me.Book[Aura of the Grove]}) /varset buff_aura1 Aura of the Grove
        /if (${Me.Book[Aura of Life]}) /varset buff_aura1 Aura of Life
    } else /if (${IsCLR}) {
        /if (${Me.Book[Aura of the Zealot]}) /varset buff_aura1 Aura of the Zealot
        /if (${Me.Book[Aura of the Pious]}) /varset buff_aura1 Aura of the Pious
        /if (${Me.Book[Aura of Divinity]}) /varset buff_aura1 Aura of Divinity
        /if (${Bool[${Me.AltAbility[Spirit Mastery]}]}) {
            /varset buff_aura1 Spirit Mastery
        }
    } else /if (${IsENC}) {
        /if (${Me.Book[Beguiler's Aura]}) /varset buff_aura1 Beguiler's Aura
        /if (${Me.Book[Illusionist's Aura]}) /varset buff_aura1 Illusionist's Aura
        /if (${Me.Book[Twincast Aura]}) /varset buff_aura1 Twincast Aura
        /if (${Bool[${Me.AltAbility[Auroria Mastery]}]}) {
            /varset buff_aura1 Auroria Mastery
        }
    } else /if (${IsPAL}) {
        /if (${Me.Book[Holy Aura]}) /varset buff_aura1 Holy Aura
        /if (${Me.Book[Blessed Aura]}) /varset buff_aura1 Blessed Aura
    } else /if (${IsMAG}) {
        /if (${Me.Book[Earthen Strength]}) /varset buff_aura1 Earthen Strength
        /if (${Me.Book[Rathe's Strength]}) /varset buff_aura1 Rathe's Strength
    }

    /if (${buff_aura1.Length} > 0) {
        /varset buff_auraenable TRUE
        /call Skill_CreateData AURA "${buff_aura1}"
    }
/RETURN

SUB buff_AuraCheck
    /if (${buff_auraenable}) {
        /if (${InCombat}) /return
        /if (!${Buff_Pause} && ${Buff_PausePeriod} == 0 && !${IsWindowOpen} && !${Me.Invis}) {
            /if (${IsMAG}) {
                /if (${Bool[${Me.Pet.ID}]}) {
                    /if (${Me.Pet.Distance} < 100) {
                        /if (${Bool[${Spawn[${buff_aura1}].ID}]}) {
                            /if (${Spawn[${buff_aura1}].Distance} < 100) /return
                        }
                        /call Skill_Cast_Data -1 AURA -1
                    }
                }
            } else /if (!${Bool[${Me.Aura[1]}]}) {
                /call Skill_Cast_Data -1 AURA -1
            }
        }
    }
/RETURN

SUB buff_Check
    /if (${Buff_PausePeriod} == 0 && !${Buff_Pause} && !${IsWindowOpen}) {

        /if (!${Defined[buff_List]}) /return
        /if (${Navigation.Active}) /return
        /if (${Me.Invis}) {
            /if (${IsROG}) {
                /if (${SpawnCount[pc radius 40]} < 2 && ${SpawnCount[npc radius 200 zradius 100]}) /return
            } else {
                /return
            }
        }

        /declare i int local
        /declare s int local

        /declare hsize int local
        /declare whoto string local

        /varset buff_caststatus -1
        /if (${InCombat}) /varset buff_pos 1
        /for s ${buff_pos} to ${buff_List.Size}
            | if there is someone to heal, prioritize it
            /if (${IsHealer} || ${IsSubHealer}) {
                /call Heal_IsInjured
                /if (${Macro.Return}) {
                    /call Heal_Check
                    /if (${Macro.Return}) /return
                }
            }

            /if (${buff_auto${s}_ok} != 2) {
                /if (${buff_auto${s}_ok} < 1) {
                    /call Skill_CreateData buff_auto${s} "${buff_List[${s}]}"
                    /if (!${Macro.Return}) {
                        /continue
                    }
                }
                /call Skill_UpdateData buff_auto${s}
                /if (${buff_auto${s}_ok} != 2) /continue
            }

            | check combat or out of combat first
            /if (${buff_auto${s}_combat}) {
                /if (!${InCombat}) /continue
                /if (!${Attacking}) /continue
            } else /if (!${buff_auto${s}_always}) {
                /if (${InCombat}) /continue
            }

            /if (${InCombat}) {
                | hate check
                /if (${buff_auto${s}_hatebelow} != 200) {
                    /if (${buff_auto${s}_hatebelow} < ${Me.PctAggro}) /continue
                }
                /if (${buff_auto${s}_hateover} != 0) {
                    /if (${buff_auto${s}_hateover} > ${Me.PctAggro}) /continue
                }
                /if (${buff_auto${s}_hate2ndbelow} != 200) {
                    /if (${buff_auto${s}_hate2ndbelow} < ${Me.SecondaryPctAggro}) /continue
                }
                /if (${buff_auto${s}_hate2ndover} != 0) {
                    /if (${buff_auto${s}_hate2ndover} > ${Me.SecondaryPctAggro}) /continue
                }
                | named check
                /if (${Attacking}) {
                    /if (${buff_auto${s}_named} != 0) {
                        /if (${Spawn[${AttackingTarget}].Type.NotEqual[PC]}) {
                            /call combat_IsNamed ${Target.ID}
                            /if (${Macro.Return}) {
                                /if (${buff_auto${s}_named} == -1) /continue
                            } else {
                                /if (${buff_auto${s}_named} == 1) /continue
                            }
                        }
                    }
                }
            }

            /call Skill_MySpellReady ${buff_auto${s}_spelltype} "${buff_auto${s}_spellname}"
            /if (${Macro.Return} != ${CAST_SUCCESS} && ${Macro.Return} != ${CAST_NOTMEMMED}) /continue

            /if (${Group.Members}) {
                /if (${Group.Member[${Me}].Leader} && ${Me.Moving} || ${Stopped} > 0) {
                    /if (${skill_GetCastTime[${buff_auto${s}_spellname}]} != 0) /return
                }
            } else {
                /if (${Me.Moving} || ${Stopped} > 0) {
                    /if (${skill_GetCastTime[${buff_auto${s}_spellname}]} != 0) /return
                }
            }
            /if (${Following}) {
                /if (${buff_auto${s}_notfollowing}) /continue
                /if (${skill_GetCastTime[${buff_auto${s}_spellname}]} != 0) /continue
                /if (${Me.Moving}) {
                    /if (${Basic_ManastoneSickness} == 0) {
                        /if (${skill_GetCastTime[${buff_auto${s}_spellname}]} != 0) /continue
                    }
                }
            }

            /if (${IsDead}) {
                /if (${buff_auto${s}_notdead}) /continue
                /if (!${Bool[${buff_auto${s}_spellname}]}) /continue
                /if (${buff_auto${s}_spelltype} == ${CAST_TYPE_ITEM}) /continue
            }

            /if (!${basic_tribute}) {
                /if (${buff_auto${s}_tribute} == 1) {
                     /continue
                }
            } else {
                /if (${buff_auto${s}_tribute} == -1) {
                    /continue
                }
            }
            /if (${buff_auto${s}_raid} == 1) {
                /if (${Raid.Members} == 0) {
                    /continue
                }
            } else /if (${buff_auto${s}_raid} == -1) {
                /if (${Raid.Members} > 0) {
                    /continue
                }
            }

            /if (${buff_auto${s}_safe} == 1) {
                /if (!${IsSafe}) /continue
            } else /if (${buff_auto${s}_safe} == -1) {
                /if (${IsSafe}) /continue
            }

            /if (${IsENC}) {
                /if (${buff_auto${s}_spellid} == 11903 || ${buff_auto${s}_spellid} == 8032) {
                    /if (${buff_encmanaflare} > 0) {
                        /continue
                    }
                }
            }

            /if (${buff_auto${s}_ispet}) {
                /if (!${Bool[${Me.Pet.ID}]}) /continue
            }

            /if (!${IsNoMana}) {
                /if (${buff_auto${s}_manabelow} != 200) {
                    /if (${buff_auto${s}_manabelow} < ${Me.PctMana}) /continue
                }
                /if (${buff_auto${s}_spelltype} == ${CAST_TYPE_SPELL}) {
                    /if (${buff_auto${s}_gom}) {
                        /call Skill_GoMCheck
                        /if (!${Macro.Return}) /continue
                    } else {
                        /if (${buff_auto${s}_manaover} != 0) {
                            /if (${buff_auto${s}_manaover} > ${Me.PctMana}) /continue
                        }
                    }

                    /if (${buff_auto${s}_reagentreqnum} > 0) {
                        /call buff_CheckReagent "${buff_auto${s}_reagentname}" "${buff_auto${s}_spellName}" ${buff_auto${s}_reagentreqnum}
                        /if (!${Macro.Return}) /continue
                    }
                    /if (${IsPetClass}) {
                        /if (${buff_auto${s}_summon}) {
                            /if (${IsDead} && !${IsSubPetClass}) /continue

                            /declare petresult string local
                            /if (!${Bool[${Me.Pet.ID}]}) {
                                /call Skill_cast_Data -1 buff_auto${s} -1
                                /varset petresult ${Macro.Return}
                                /if (${petresult} != ${CAST_SUCCESS}) {
                                    /varset Buff_PausePeriod 15
                                    /varset Basic_StopExchangePeriod 5
                                    /if (${Bool[${Me.Pet.ID}]}) {
                                        /call Utilities_petghold
                                    }
                                    /varset buff_pos ${s}
                                    /return
                                }
                            }
                            /continue
                        }
                    }
                }
            }

            /if (${buff_auto${s}_notarget}) {
                /call buff_Castbuff NOT ${s}
                /if (${Macro.Return}) {
                    /if (${buff_List.Size} == ${buff_pos}) {
                        /varset buff_pos 1
                    } else {
                        /varcalc buff_pos ${buff_pos}+1
                    }
                    /return
                }
                /continue
            }

            | buff for pc
            /call ArrayLength buff_auto${s}_wholist
            /varset hsize ${Macro.Return}

            /if (${hsize} > 0) {
                /for i 1 to ${hsize}
                    /call ArrayGet buff_auto${s}_wholist ${i}
                    /varset whoto ${Macro.Return}
                    /if (!${NetBots[${whoto}].InZone}) /continue
                    /if (!${whoto.Equal[${Me}]} && ${buff_auto${s}_grouped}) {
                        /if (!${InGroup[${whoto}]}) /continue
                    }

                    /call buff_Castbuff ${whoto} ${s}
                    /if (${Macro.Return}) {
                        /varset buff_pos ${s}
                        /return
                    }
                /next i
            }

            | buff for pet
            /call ArrayLength buff_auto${s}_petlist
            /varset hsize ${Macro.Return}

            /if (${hsize} > 0) {
                /for i 1 to ${hsize}
                    /call ArrayGet buff_auto${s}_petlist ${i}
                    /varset whoto ${Macro.Return}
                    /if (!${NetBots[${whoto}].InZone}) /continue
                    /if (${buff_auto${s}_grouped}) {
                        /if (!${InGroup[${whoto}]}) /continue
                    }
                    /call buff_CastPetbuff ${whoto} ${s}
                    /if (${Macro.Return}) {
                        /varset buff_pos ${s}
                        /return
                    }
                /next i
            }
        /next s

        | looks like no buff to cast
        /varset buff_pos 1
        /if (${buff_caststatus} != -1) /return

        /if (!${InCombat}) {
            /if (${Me.Moving} || ${Following}) /return
            /if (${buff_defaulteightenable}) {
                /if (${Stopped} > 0) /return
                /if (${IsSafe}) /return

                /if (!${Bool[${Me.Gem[${buff_defaulteight}]}]}) {
                    /if (${Me.Book[${buff_defaulteight}]}) {
                        /call Skill_MemorizeSpell_Blocking ${DefaultGem} "${buff_defaulteight}" TRUE
                        |**
                        /memspell ${DefaultGem} "${buff_defaulteight}"
                        /for i 1 to 50
                            /if (${Bool[${Me.Gem[${buff_defaulteight}]}]}) {
                                /return
                            }
                            /delay 1
                        /next i
                        **|
                    }
                }
            }
            /if (${buff_petgodenable} > 0) {
                /if (!${IsWindowOpen}) {
                    /if (${Bool[${FindItem[=Metamorph Totem: Ancient Gods].ID}]}) {
                        /if (!${Bool[${Me.Pet.Buff[Illusion: Ancient]}]}) {
                            /call Skill_Cast_Data -1 ITEM_GODILL -1
                        }
                        /if (${buff_petgodenable} == 2) {
                            /if (!${buff_petgodname.Find[${Me.Pet.Race.Name}]}) {
                                /while (${Bool[${Me.Pet.Buff[Illusion: Ancient]}]}) {
                                    /removepetbuff Illusion: Ancient
                                }
                            }
                        }
                    }
                }
            }
        } else /if (${IsZuru}) {
            /if (${IsHealer}) {
                /call Heal_IsInjured
                /if (${Macro.Return}) /return
                /call Skill_Cast_Data -1 AA_SILENT -1
                /if (${Macro.Return}) /return
            } else /if (${IsCaster}) {
                /call Skill_Cast_Data -1 AA_SILENT -1
                /if (${Macro.Return}) /return
            }
            /if (${Attacking}) {
                /call Zuru_aftercast ${CAST_BENEFICIAL} -1 TRUE
            }
        }
    }
/RETURN

SUB buff_Castbuff(string targetName, int buffindex)

    /declare notarget bool local FALSE
    /if (${buff_auto${buffindex}_notarget}) {
        /varset targetName ${Me}
        /varset notarget TRUE
    }

    /if (${NetBots[${targetName}].InZone}) {
        /if (${NetBots[${targetName}].FreeBuffSlots} > 0) {
            /declare spellresult string local
            /declare curtarget int local -1
            /declare spellID int local ${buff_auto${buffindex}_spellid}

            | range check
            /if (${buff_auto${buffindex}_range} != 0) {
                /if (${Spawn[PC ${targetName}].Distance} > ${buff_auto${buffindex}_range}) {
                    /return FALSE
                }
            }

            /if (${buff_auto${buffindex}_spelltype} == ${CAST_TYPE_ITEM}) {
                | Do not summon horse while you are shadowman
                /if (${IsSafe}) {
                    /if (${buff_auto${buffindex}_checkname.Equal[Summon Horse]}) {
                        /if (${Me.Race.ID} == ${ID_RACE_INVISIBLEMAN}) {
                            /return FALSE
                        }
                    }
                }
            }

            /if (${targetName.Equal[${Me}]}) {
                /if (${Bool[${Me.Buff[${buff_auto${buffindex}_checkname}]}]}) {
                    /if (${Me.Buff[${buff_auto${buffindex}_checkname}].Duration} == 0) {
                        | It is permanent
                        /return FALSE
                    } else /if (${Me.Buff[${buff_auto${buffindex}_checkname}].Duration.TotalSeconds} > ${Buff_RecastTime}) {
                        /return FALSE
                    }
                }
                /if (${Bool[${Me.Song[${buff_auto${buffindex}_checkname}]}]}) {
                    /if (${Me.Song[${buff_auto${buffindex}_checkname}].Duration} == 0) {
                        | It is permanent
                        /return FALSE
                    } else /if (${Me.Song[${buff_auto${buffindex}_checkname}].Duration.TotalSeconds} > ${Buff_RecastTime}) {
                        /return FALSE
                    }
                }
            } else /if (${Bool[${NetBots[${targetName}].Buff.Find[${spellID} ]}]} || ${Bool[${NetBots[${targetName}].ShortBuff.Find[${spellID} ]}]}) {
                /return FALSE
            }
            | treatment for Flight of Eagles
            /if (${spellID} == 3185 || ${spellID} == 16814 || ${spellID} == 2517) {
                | taka does not allow to cast levitation
                /if (${Zone.ID} == 231) {
                    /return FALSE
                }
                /if (${Bool[${NetBots[${targetName}].Buff.Find[16814 ]}]} || ${Bool[${NetBots[${targetName}].Buff.Find[3185 ]}]}) /return FALSE
            | treatment for Group Pact of the Wolf
            } else /if (${spellID} == 27489) {
                /if (${Bool[${NetBots[${targetName}].Buff.Find[27491 ]}]}) /return FALSE
            | treatment for blessing of oak
            } else /if (${spellID} == 5353) {
                | life of recourse
                /if (${Bool[${NetBots[${targetName}].Buff.Find[23526 ]}]}) /return FALSE
            | treatment for blessing of replenishment
            } else /if (${spellID} == 3441) {
                /if (${Bool[${NetBots[${targetName}].Buff.Find[23526 ]}]}) /return FALSE
            | treatment for divine intervension and divine guardian conflict
            } else /if (${spellID} == 1546) {
                /if (${Bool[${NetBots[${targetName}].Buff.Find[30663 ]}]}) /return FALSE
            | treatment for panopy of vie
            } else /if (${spellID} == 5261) {
                /if (${Bool[${NetBots[${targetName}].Buff.Find[16091 ]}]}) /return FALSE
            | treatment for celerity
            } else /if (${spellID} == 171) {
                /if (${Bool[${NetBots[${targetName}].Buff.Find[3391 ]}]}) /return FALSE
            | treatment for talisman of celerity
            } else /if (${spellID} == 38062) {
                /if (${Bool[${NetBots[${targetName}].Buff.Find[3391 ]}]}) /return FALSE
            }

            /declare i int local
            /for i 1 to 4
                /if (${buff_auto${buffindex}_check${i}id} == -1) /break
                /if (${Bool[${NetBots[${targetName}].Buff.Find[${buff_auto${buffindex}_check${i}id} ]}]} || ${Bool[${NetBots[${targetName}].ShortBuff.Find[${buff_auto${buffindex}_check${i}id} ]}]}) {
                    /return FALSE
                }
            /next i

            /if (${buff_auto${buffindex}_hot}) {
                /if (${IsHealer} || ${IsPAL}) {
                    /call heal_HoTCheck ${buff_auto${buffindex}_spellid} "${NetBots[${targetName}].ShortBuff}"
                    /if (!${Macro.Return}) /return FALSE
                }
            }

            /if (${Attacking}) {
                /if (${Target.ID} != 0) {
                    /varset curtarget ${Target.ID}
                }
            }

            /if (${buff_auto${buffindex}_canmgb}) {
                /if (${class_canmgb}) {
                    /if (${buff_auto${buffindex}_targettype.Find[Group]}) {
                        /if (${InCombat}) {
                            /if (${buff_auto${buffindex}_mgb}) {
                                /call Skill_Cast_Data -1 AA_MGB -1
                            }
                        } else {
                            |/echo Buff: Enabling Tranquil Blessing for ${buff_auto${buffindex}_spellname}
                            /call Skill_Cast_Data -1 AA_TRANQB -1
                        }
                    }
                }
            } else /if (${IsENC}) {
                /if (${buff_auto${buffindex}_project}) {
                    /call Skill_Cast_Data -1 ENC_PROJILL -1
                }
            }

            /if (${notarget}) {
                /call Skill_Cast_Data -1 buff_auto${buffindex} -1
            } else {
                /call Skill_Cast_Data ${NetBots[${targetName}].ID} buff_auto${buffindex} -1
            }
            /varset spellresult ${Macro.Return}
            /varset buff_caststatus ${Macro.Return}
            /if (${Attacking}) {
                /if (${curtarget} != -1) {
                    /call MobTarget ${curtarget}
                }
            }
            /if (${spellresult} == ${CAST_SUCCESS}) {
                /if (${IsENC}) {
                    /if (${buff_auto${buffindex}_spellid} == 11903 || ${buff_auto${buffindex}_spellid} == 8032) {
                        /varset buff_encmanaflare 32s
                    }
                }
                /varset Buff_PausePeriod 10
                /return TRUE
            } else /if (${spellresult} == ${CAST_TAKEHOLD}) {
                /varset Buff_PausePeriod 10
                /return TRUE
            }
        }
    }
/RETURN FALSE

SUB buff_CastPetbuff(string targetName, int buffindex)
    /if (${NetBots[${targetName}].InZone}) {
        /if (!${NetBots[${targetName}].PetID}) /return FALSE

        | skip if pet is vendor
        /if (${Spawn[${NetBots[${targetName}].PetID}].Race.ID} == ${ID_RACE_GNOMEWORK}) {
            /return FALSE
        }

        /declare spellID int local ${buff_auto${buffindex}_spellid}
        /declare spellresult string local

        /declare spellType string local ${buff_auto${buffindex}_spelltype}
        /declare gem int local ${buff_auto${buffindex}_gem}

        /declare curtarget int local -1

        /if (${Bool[${NetBots[${targetName}].PetBuff.Find[${spellID} ]}]}) {
            /return FALSE
        }
        | treatment for Flight of Eagles
        /if (${spellID} == 3185 || ${spellID} == 16814) {
            /if (${Bool[${NetBots[${targetName}].PetBuff.Find[16814 ]}]} || ${Bool[${NetBots[${targetName}].PetBuff.Find[3185 ]}]}) /return FALSE
        | treatment for Group Pact of the Wolf
        } else /if (${spellID} == 27489) {
            /if (${Bool[${NetBots[${targetName}].PetBuff.Find[27491 ]}]}) /return FALSE
        }

        /declare i int local
        /for i 1 to 4
            /if (!${Bool[${buff_auto${buffindex}_check${i}}]}) /break
            /varset spellID ${buff_auto${buffindex}_check${i}id}
            /if (${Bool[${NetBots[${targetName}].PetBuff.Find[${spellID} ]}]}) {
                /return FALSE
            }
        /next i

        /if (${Attacking}) {
            /if (${Target.ID} != 0) {
                /varset curtarget ${Target.ID}
            }
        }
        /call Skill_Cast_Data ${NetBots[${targetName}].PetID} buff_auto${buffindex} -1
        /varset spellresult ${Macro.Return}
        /if (${Attacking}) {
            /if (${curtarget} != -1) {
                /call MobTarget ${curtarget}
            }
        }
        /if (${spellresult} == ${CAST_SUCCESS} || ${spellresult} == ${CAST_INTERRUPTED}) {
            /if (${buff_auto${buffindex}_spelltype} == ${CAST_TYPE_SPELL}) {
                /varset Buff_PausePeriod 15
            }
            /varset Basic_StopExchangePeriod 5
            /return TRUE
        }
    }
/RETURN FALSE

SUB buff_CheckReagent(reagent, spellName, reqnum)
    /if (!${Defined[buff_reagentyell]}) {
        /declare buff_reagentyell int outer 0
    }
    /if (${FindItemCount[=${reagent}]} < ${reqnum}) {
        /if (${buff_reagentyell} == 0) {
            /bc [+r+]Buff: missing <${reagent}> for ${spellName}!
        } else {
            /echo Buff: missing <${reagent}> for ${spellName}!
        }
        /varcalc buff_reagentyell ${buff_reagentyell}+1
        /if (${buff_reagentyell} == 100) {
            /varset buff_reagentyell 0
        }
        /return FALSE
    } else {
        /varset buff_reagentyell 0
        /return TRUE
    }
/RETURN TRUE

SUB Buff_RemoveShortBuff(buffName)
    /declare slot int local ${Me.Song[${buffName}].ID}
    /if (${Bool[${slot}]}) {
        /declare i int local
        /declare tooltip string local

        /varcalc slot ${slot}-1
        |/echo try remove ${slot} ${Window[ShortDurationBuffWindow].Child[SDBW_Buff${slot}_Button].Tooltip} a ${Me.Buff[${buffName}].ID}
        /if (!${Window[ShortDurationBuffWindow].Child[SDBW_Buff${slot}_Button].Tooltip.Find[${buffName}]}) {
            |/echo cannot find buff ${slot} a ${buffName}
            /for i 0 to 19
                /varset tooltip ${Window[ShortDurationBuffWindow].Child[SDBW_Buff${i}_Button].Tooltip}
                /if (${tooltip.Length} > 0) {
                    /if (${tooltip.Find[${buffName}]}) {
                        |/echo adjusting slog number from ${slot} to ${i}
                        /varset slot ${i}
                        /break
                    }
                }
            /next i
        }
        /nomodkey /notify ShortDurationBuffWindow SDBW_Buff${slot}_Button leftmouseup
    }
/RETURN

SUB Buff_RemoveBuff(buffName)
    /declare sslot int local ${Me.Song[${buffName}].ID}
    /declare slot int local ${Me.Buff[${buffName}].ID}

    /declare i int local
    /declare tooltip string local
    /declare found bool TRUE
    /if (${Bool[${slot}]}) {
        /varcalc slot ${slot}-1
        |/echo try remove ${slot} ${Window[DurationBuffWindow].Child[BW_Buff${slot}_Button].Tooltip} a ${Me.Song[${buffName}].ID}
        /if (!${Window[BuffWindow].Child[BW_Buff${slot}_Button].Tooltip.Find[${buffName}]}) {
            /varset found FALSE
            |/echo cannot find buff ${slot} a ${buffName}
            /for i 0 to 36
                /varset tooltip ${Window[BuffWindow].Child[BW_Buff${i}_Button].Tooltip}
                /if (${tooltip.Length} > 0) {
                    |/echo aa ${i} aaa ${tooltip}
                    /if (${tooltip.Find[${buffName}]}) {
                        |/echo adjusting slog number from ${slot} to ${i}
                        /varset slot ${i}
                        /varset found TRUE
                        /break
                    }
                }
            /next i
        }
        /if (${found}) {
            /nomodkey /notify BuffWindow BW_Buff${slot}_Button leftmouseup
        }
    } else /if (${Bool[${sslot}]}) {
        /varcalc sslot ${sslot}-1
        |/echo try remove ${slot} ${Window[ShortDurationBuffWindow].Child[SDBW_Buff${slot}_Button].Tooltip} a ${Me.Buff[${buffName}].ID}
        /if (!${Window[ShortDurationBuffWindow].Child[SDBW_Buff${sslot}_Button].Tooltip.Find[${buffName}]}) {
            /varset found FALSE
            |/echo cannot find buff ${slot} a ${buffName}
            /for i 0 to 19
                /varset tooltip ${Window[ShortDurationBuffWindow].Child[SDBW_Buff${i}_Button].Tooltip}
                /if (${tooltip.Length} > 0) {
                    /if (${tooltip.Find[${buffName}]}) {
                        |/echo adjusting slog number from ${slot} to ${i}
                        /varset sslot ${i}
                        /varset found TRUE
                        /break
                    }
                }
            /next i
            /if (!${found}) {
                /for i 0 to 36
                    /varset tooltip ${Window[BuffWindow].Child[BW_Buff${i}_Button].Tooltip}
                    /if (${tooltip.Length} > 0) {
                        |/echo aa ${i} aaa ${tooltip}
                        /if (${tooltip.Find[${buffName}]}) {
                            /nomodkey /notify BuffWindow BW_Buff${i}_Button leftmouseup
                            /RETURN
                        }
                    }
                /next i
            }
        }
        /if (${found}) {
            /nomodkey /notify ShortDurationBuffWindow SDBW_Buff${sslot}_Button leftmouseup
        }
    }
/RETURN
