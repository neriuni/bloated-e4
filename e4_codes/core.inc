#include e4_codes\utilities.inc
#include e4_codes\movement.inc
#include e4_codes\skill.inc
#include e4_codes\basic.inc
#include e4_codes\buff.inc
#include e4_codes\heal.inc
#include e4_codes\autoheal.inc
#include e4_codes\cure.inc
#include e4_codes\combat.inc
#include e4_codes\loot.inc
#include e4_codes\sell.inc
#include e4_codes\rez.inc
#include e4_codes\alert.inc
#include e4_codes\autobattle.inc
#include e4_codes\zuru.inc
#include e4_codes\class.inc
#include e4_codes\trade.inc
#include e4_codes\donate.inc
#include e4_codes\travel.inc
#include e4_codes\cruise.inc
| ********** add new module inc path here! **********



Sub Core_Setup(characterIni)
	| register module
	/call ArrayCreate core_TaskMain
	/call ArrayAdd core_TaskMain Utilities
	/call ArrayAdd core_TaskMain Basic
	/call ArrayAdd core_TaskMain Movement
	/call ArrayAdd core_TaskMain Skill
	/call ArrayAdd core_TaskMain Heal
	/call ArrayAdd core_TaskMain AutoHeal
	/call ArrayAdd core_TaskMain Cure
	/call ArrayAdd core_TaskMain Rez
	/call ArrayAdd core_TaskMain Buff
	/call ArrayAdd core_TaskMain AutoBattle
	/call ArrayAdd core_TaskMain Combat
	/call ArrayAdd core_TaskMain Class
	/call ArrayAdd core_TaskMain Zuru
	/call ArrayAdd core_TaskMain Loot
	/call ArrayAdd core_TaskMain Sell
	/call ArrayAdd core_TaskMain Alert
	/call ArrayAdd core_TaskMain Trade
	/call ArrayAdd core_TaskMain donate
	/call ArrayAdd core_TaskMain travel
	/call ArrayAdd core_TaskMain Cruise
| ********** add new module name here! **********

	| Global
	/declare PreviousZone int outer -1
	/declare CurrentZone int outer ${Zone.ID}
	/declare CurrentZoneShortName string outer ${Zone.ShortName}

	/declare MyID int outer ${Me.ID}

	/declare DefaultGem int outer 8

	/if (${Bool[${characterIni}]}) {
		/echo Core: Loading ${characterIni}
		/declare CharacterIni string outer e4_botsettings\${characterIni}
	} else {
		/declare CharacterIni string outer e4_botsettings\${Me.Name}.ini
	}

	/declare numInventorySlots int outer 10

	/declare InCombat bool outer FALSE
	/declare IsWindowOpen bool outer FALSE
	/declare IsDead bool outer
	/if (!${Bool[${Me.Inventory[chest].ID}]} && ${Me.Copper} == 0 && ${Me.Level} > 10) {
		/varset IsDead TRUE
	}
    /declare IsSafe bool outer
	/if (${CurrentZone} == 202 || ${CurrentZone} == 152 || ${CurrentZone} == 345 || ${CurrentZone} == 386 || ${CurrentZone} == 151) {
		/varset IsSafe TRUE
	} else {
		/varset IsSafe FALSE
	}

	/declare Stopped timer outer FALSE
	/declare Moving int outer FALSE
	/declare core_lastmoving bool outer FALSE

	| Parameter
	/declare core_LowPriorityTimer_delay int outer 8

	| Base settings
	/echo Loading E4 v${E4_Version}...
	/call core_checkPlugins

	/declare core_LowPriorityTimer timer outer 0
	/declare core_zoneloaded timer outer

	/declare core_nomembercheck timer outer 0
	/call core_UpdateMembers

	/declare IsZuru bool outer FALSE

	/call ArrayCreate core_TaskZoneChange
	/call ArrayCreate core_TaskLowMain
	/call ArrayCreate core_TaskMobSlain
	/call ArrayCreate core_TaskEndCombat

	/declare i int local
	/for i 1 to ${ArrayLength[core_TaskMain]}
		|/echo Core: Setup calling ${ArrayGet[core_TaskMain,${i}]}_Setup
		/call ${ArrayGet[core_TaskMain,${i}]}_Setup
	/next i

	/setwintitle ${Me.Name}

	/echo Core: Loaded ${ArrayLength[core_TaskMain]} modules. Setup done.

	/varset Basic_StopExchangePeriod 3s
	/varset Basic_PauseAutoMediPeriod 3s

	| hide corpses when loaded
	/squelch /hidec npc

	| dismount
	/if (${Bool[${Me.Mount.ID}]}) {
		/dismount
    }

/RETURN

| Main loop
SUB Core_Main

	/declare i int local

	| check Zone change
	/if (${Zone.ID} == 190) /endmacro
	/doevents Loading
	/doevents Entering
	/if (${Zone.ID} != ${CurrentZone} || ${Me.ID} != ${MyID}) {
		/call core_zonechanged
	}
	/if (${core_zoneloaded} > 0) /return

	/call core_UpdateMembers

	| Check Im dead
	/if (!${Bool[${Me.Inventory[chest].ID}]} && ${Me.Copper} == 0 && ${Me.Level} > 10) {
		/varset IsDead TRUE
	} else {
		/varset IsDead FALSE
	}

	| End of combat
	/declare lastcombat bool local ${InCombat}
	/call Core_checkcombat
	/if (!${InCombat} && ${lastcombat}) {
		/varset Buff_PausePeriod 5s
		/for i 1 to ${ArrayLength[core_TaskEndCombat]}
			/call ${ArrayGet[core_TaskEndCombat, ${i}]}
		/next i
	}

	| Check stopped
	/if (${core_lastmoving}) {
		/if (!${Me.Moving}) {
			| Stopped
			/varset Stopped 1s
			/if (${IsTank}) {
				/varset Buff_PausePeriod 1s
			}
			/varset Moving 0
		} else {
			| Moving
			/varcalc Moving ${Moving}+1
		}
	}
	/varset core_lastmoving ${Me.Moving}

	| check interracting window is open or not
	/varset IsWindowOpen FALSE
	/if (!${InCombat}) {
		/if (${Merchant.Open}) {
			/varset IsWindowOpen TRUE
		} else /if (${Window[lootWnd].Open}) {
			/varset IsWindowOpen TRUE
		} else /if (${Window[TributeMasterWnd].Open}) {
			/varset IsWindowOpen TRUE
		} else /if (${Window[BigBankWnd].Open}) {
			/varset IsWindowOpen TRUE
		} else /if (${Window[TradeWnd].Open}) {
			/varset IsWindowOpen TRUE
		} else /if (${Window[GiveWnd].Open}) {
			/varset IsWindowOpen TRUE
		} else /if (${Window[QuantityWnd].Open}) {
			/varset IsWindowOpen TRUE
		}
	}

    | check queued spell(highest priority)
    /call skill_Qcheck
	/for i 1 to ${ArrayLength[core_TaskMain]}
		/call ${ArrayGet[core_TaskMain, ${i}]}_Main
	/next i

	/doevents MobSlain

	| Low priority task
	/if (${core_LowPriorityTimer} == 0) {
		/doevents Vercheck

		/if (${Zone.ID} == 202) {
			/squelch /maphide pc
			/squelch /maphide npc
		} else {
			/squelch /mapshow npc
		}

		| call OneSec routine
		/if (${ArrayLength[core_TaskLowMain]} > 0) {
			/for i 1 to ${ArrayLength[core_TaskLowMain]}
				/call ${ArrayGet[core_TaskLowMain, ${i}]}
			/next i
		}
		/varset core_LowPriorityTimer ${core_LowPriorityTimer_delay}
	}
/RETURN


| Register Task
SUB Core_AddTask_ZoneChange(name)
	/call ArrayAdd core_TaskZoneChange ${name}
/RETURN

SUB Core_AddTask_LowMain(name)
	/call ArrayAdd core_TaskLowMain ${name}
/RETURN

SUB Core_AddTask_MobSlain(name)
	/call ArrayAdd core_TaskMobSlain ${name}
/RETURN

SUB Core_AddTask_EndCombat(name)
	/call ArrayAdd core_TaskEndCombat ${name}
/RETURN


| Check Mob slain
#EVENT MobSlain "#1# has been slain by #2#!"
#EVENT MobSlain "You have slain #1#!"
SUB EVENT_MobSlain(line, Killed, Killer)
	/declare i int local
	/if (!${Defined[Killer]}) {
		/declare Killer string local ${Me}
	} else /if (${Bool[${NetBots[${Spawn[${Killer}].Master.Name}].ID}]}) {
		/varset Killer ${Spawn[${Killer}].Master.Name}
	}
	/for i 1 to ${ArrayLength[core_TaskMobSlain]}
		/call ${ArrayGet[core_TaskMobSlain, ${i}]} "${Killed}" ${Killer}
	/next i
/RETURN

| Check Loading message. last event of previous zone
#EVENT Loading "LOADING, PLEASE WAIT..."
SUB EVENT_Loading
	/echo Zone: Zone is changing... ${Time}
	/call SKill_Interrupt
    /if (${Skill_memorizing}) /varset Skill_memorizing FALSE

	/varset Basic_StopExchangePeriod 15s
	/varset Buff_PausePeriod 15s
	/varset Basic_PauseAutoMediPeriod 15s
	/varset Buff_BegProgress 0
	/if (${Following}) {
		|/call Movement_Follow ${FollowingTarget}
		/call Movement_Stop
	}
	/varset core_zoneloaded 15s
/RETURN

| Check Entered message. first event of new zone
#EVENT Entering "You have entered #1#."
#EVENT Entering "Channels: #1#"
SUB EVENT_Entering(line, zonename)
	/if (${zonename.Find[an area]}) /return
	/doevents flush Entering
	/call core_zonechanged
/RETURN

SUB core_zonechanged
	/echo Zone: Zone change detected from ${CurrentZoneShortName} to ${Zone.ShortName}. ${Time}

	/varset PreviousZone ${CurrentZone}
	/varset CurrentZone ${Zone.ID}
	/varset CurrentZoneShortName ${Zone.ShortName}

	| set window caption
	/setwintitle ${Me.Name}

	| PoK, Nexus, Guild hall, Temple of Marr and Bazaar are safe zone
	/if (${CurrentZone} == 202 || ${CurrentZone} == 152 || ${CurrentZone} == 345 || ${CurrentZone} == 386) {
		/varset IsSafe TRUE
		/squelch /maphide pc
		/squelch /hidec ALLBUTGROUP
		/squelch /mapshow npc
	} else /if (${CurrentZone} == 151) {
		/varset IsSafe TRUE
		/squelch /mapshow npc
	} else {
		/varset IsSafe FALSE
		/squelch /mapshow npc
		/squelch /hidec npc
	}

	/varset Basic_StopExchangePeriod 0
	/varset Buff_PausePeriod 15s
	/varset Basic_PauseAutoMediPeriod 0

	| reload moveutils
	/squelch /plugin MQ2MoveUtils unload
	/squelch /plugin MQ2MoveUtils

	/varset core_zoneloaded 0

	/declare i int local
	/for i 1 to ${ArrayLength[core_TaskZoneChange]}
		/call ${ArrayGet[core_TaskZoneChange, ${i}]}
	/next i

	/call Utilities_petghold
	/if (${Me.Sitting}) {
		/stand
	}
	/if (${Bool[${Me.Mount.ID}]}) {
		/dismount
    }

	/varset MyID ${Me.ID}

/RETURN

#EVENT reqname "#*# Names: #1#."
SUB EVENT_reqname(line, namelist)
	/if (${namelist.Find[LOGIN]} > 0) {
		/echo \arCore: Wrong EQBCS connected name detected. Attempt to reconnect
		/bccmd reconnect
		/delay 1s
	}
	/varset core_nomembercheck 2s
/RETURN


| Get information connecting to EQBCS
SUB core_UpdateMembers
	/doevents reqname
	/if (${InCombat}) /return
	/if (${Defined[Member]}) {
		/if (${core_nomembercheck} > 0) /return
		/if (${Member.Size} != ${NetBots.Counts}) /echo Core: Updating Member List. ${Member.Size} -> ${NetBots.Counts}
		/deletevar Member
	} else {
		/echo Core: Updating Member List. New(${NetBots.Counts})
	}
	/if (${NetBots.Counts} > 0) {
		/declare nc int local ${NetBots.Counts}
		/declare nl string local ${NetBots.Client}
		/declare Member[${nc}] string outer

		/declare i int local
		/for i 1 to ${Member.Size}
			/varset Member[${i}] ${nl.Arg[${i}, ]}
			/if (!${Bool[${Member[${i}]}]}) {
				/return
			}
		/next i
	} else {
		/declare Member[1] string outer
		/varset Member[1] ${Me.Name}
	}
	/call Heal_UpdateMembers
	/call Buff_UpdateMembers
	/varset core_nomembercheck 600s

/RETURN

| Checks Im in combat or not
SUB Core_checkcombat
	/declare i int local
	/varset InCombat FALSE
	/for i 1 to 13
		/call Basic_CheckXTMob ${i}
		/if (!${Macro.Return}) /continue

		/if (${Me.XTarget[${i}].Distance} > 250) /continue
		/if (${Me.XTarget[${i}].Aggressive}) {
			/varset InCombat TRUE
			/return
		}
	/next i

	/declare HPmobcount int local ${SpawnCount[npc radius 120]}
	/declare tid int local -1

	/for i 1 to ${HPmobcount}
		/varset tid ${NearestSpawn[${i},npc].ID}
		/call Basic_CheckMob ${tid}
		/if (!${Macro.Return}) /continue
		/if (${Spawn[${tid}].Aggressive}) {
			/varset InCombat TRUE
			/return
		}
	/next i

/RETURN


#EVENT Camp "It will take about 20 more seconds to prepare your camp."
#EVENT Camp "It will take about 25 more seconds to prepare your camp."
|#EVENT Camp "It will take you about 30 seconds to prepare your camp."
SUB EVENT_Camp
	/echo Core: E4 end.
	/endmacro
/RETURN

#EVENT Vercheck "<#*#> Tell from #1#: version"
SUB EVENT_Vercheck(line, chatSender)
	/tell ${chatSender} Bloated Rev. ${E4_Version}
/RETURN


| Check plugin is loaded. Dead copy from E3
SUB core_checkPlugins
    |Check MQ2EQBC.
	/if (!${Plugin[MQ2EQBC].Name.Length}) {
		/echo Plugin MQ2EQBC is not loaded, attempting to resolve...
		/plugin MQ2EQBC
		/delay 10s !${Select[${EQBC},NULL]}
		/if (${Select[${EQBC},NULL]}) {
			/echo ***WARNING*** Could not load MQ2EQBC, macro functionality may be limited.
			/echo Starting in Solo Mode...
		}
	}
    |Auto-Connect to EQBCS.
	/if (${Bool[${EQBC}]}) {
		/declare eqbcserver string local ${Ini[e4_globalsettings\config.ini,global,eqbc]}
		/if (!${EQBC.Connected}) {
			/delay 5s
			/if (!${Bool[${eqbcserver}]}) {
				/bccmd connect
				|/beep sounds/mail2
				|/echo \ar ***ERROR*** Could not connect to EQBCS! Please open EQBCS and/or do "/bccmd connect [serverip]" and try again. Terminating E4...
				|/endmacro
			} else {
				/bccmd connect ${eqbcserver}
			}
			/delay 5s ${EQBC.Connected}
			/if (!${EQBC.Connected}) {
				/beep sounds/mail2
				/echo \ar ***ERROR*** Could not connect to EQBCS! Please open EQBCS and/or do "/bccmd connect [serverip]" and try again(${eqbcserver})2. Terminating E4...
				/endmacro
			}
		}
		/if (!${Bool[${eqbcserver}]} && ${EQBC.Server.NotEqual[127.0.0.1]}) {
			/ini "e4_globalsettings\config.ini" "global" "eqbc" "${EQBC.Server}"
		}

		| sometimes EQBC has issue to add wrong charactername
		/if (${NetBots.Client.Find[LOGIN]} > 0) {
			/echo \ar Wrong EQBCS connected name detected. Attempt to reconnect
			/bccmd reconnect
			/delay 5s ${EQBC.Connected}
			/if (!${EQBC.Connected}) {
				/beep sounds/mail2
				/echo \ar ***ERROR*** Could not connect to EQBCS! Please open EQBCS and/or do "/bccmd connect [serverip]" and try again. Terminating E4...
				/endmacro
			}
		}

	|Check MQ2NetBots.
		/if (!${Plugin[MQ2NetBots].Name.Length}) {
			/echo Plugin MQ2NetBots is not loaded, attempting to resolve...
			/plugin MQ2NetBots
			/delay 3s ${NetBots}
			/if (!${NetBots}) {
				/echo ***WARNING*** Could not load MQ2NetBots! Macro functionality may be limited.
			}
		}
	|Auto-Connect to NetBots.
		/if (${NetBots}) {
			/squelch /netbots on grab=on send=on
		}
	|Check MQ2AdvPath.
		/if (!${Plugin[MQ2AdvPath].Name.Length}) {
			/echo Plugin MQ2AdvPath is not loaded, attempting to resolve...
			/plugin MQ2AdvPath
			/delay 3s ${Plugin[MQ2AdvPath].Name.Length}
			/if (!${Plugin[MQ2AdvPath].Name.Length}) {
				/echo ***WARNING*** Could not load MQ2AdvPath. Please ensure you're using a copy of MQ2 which was compiled with the MQ2AdvPath plugin and try again.
			}
		}
	}
    |Check MQ2collections
	/if (!${Plugin[mq2collections].Name.Length}) /plugin mq2collections
	/delay 3s ${Plugin[mq2collections].Name.Length}
    |Check MQ2DanNet
	/if (!${Plugin[MQ2DanNet].Name.Length}) /plugin MQ2DanNet
	/delay 3s ${Plugin[MQ2DanNet].Name.Length}

    |Check MQ2MoveUtils.
	/if (!${Plugin[MQ2MoveUtils].Name.Length}) {
		/echo Plugin MQ2MoveUtils is not loaded, attempting to resolve...
		/plugin MQ2MoveUtils
		/delay 3s !${Select[${Stick.Status},NULL]}
		/if (${Select[${Stick.Status},NULL]}) {
			/echo ***WARNING*** Could not load MQ2MoveUtils! Macro functionality may be limited.
			/if (!${Plugin[MQ2AdvPath].Name.Length}) {
				/echo Follow and Assist stick DISABLED.
			} else {
				/echo Assist and NPC stick DISABLED.
				/echo Follow restricted to NetBots.
			}
		}
	} else {
		/echo Core: Reloading MQ2MoveUtils
		/squelch /plugin MQ2MoveUtils unload
		/squelch /plugin MQ2MoveUtils
	}
    |Check MQ2Exchange.
	/if (!${Plugin[MQ2Exchange].Name.Length}) {
		/echo Plugin MQ2Exchange is not loaded, attempting to resolve...
		/plugin MQ2Exchange
		/delay 3s ${Plugin[MQ2Exchange].Name.Length}
		/if (!${Plugin[MQ2Exchange].Name.Length}) {
			/echo ***WARNING*** Could not load MQ2Exchange! Macro functionality may be limited.
			/echo Item swapping is DISABLED.
		}
	}
    |Check MQ2Twist.
	/if (${Me.Class.ShortName.Equal[BRD]}) {
		/if (!${Plugin[MQ2Twist].Name.Length}) {
			/echo Plugin Unable to detect MQ2Twist, attempting to resolve...
			/plugin MQ2Twist
			/delay 3s !${Select[${Twist},NULL]}
			/if (${Select[${Twist},NULL]}) {
				/echo ***WARNING*** Could not load MQ2Twist! Macro functionality may be limited.
				/echo Bard melodies DISABLED.
			}
		}	
	}
    |Check MQ2ItemDisplay.
	/if (!${Plugin[mq2itemdisplay].Name.Length}) /plugin MQ2ItemDisplay
	|Check MQ2LinkDB
	/if (!${Plugin[MQ2LinkDB].Name.Length}) /plugin MQ2LinkDB

	| Check MQ2Nav
	/if (${Bool[${Plugin[mq2nav].Name.Length}]}) {
		/if (${Navigation.Active} || ${Navigation.Paused}) {
			/squelch /nav stop
		}
		/nav reload
	} else {
		/plugin mq2nav
	}

/RETURN
