SUB Alert_Setup

    | Control

    | Parameter

    | Alias

    | Internal
    /call Core_AddTask_LowMain Alert_LowMain

    /call iniToVarV "${CharacterIni},Misc,Mask" Alert_Mask string outer
    /if (!${Macro.Return}) {
        /declare Alert_Mask string outer ${Me.Inventory[face].Name}
    } else /if (${Alert_Mask.Length} == 0) {
        /varset Alert_Mask ${Me.Inventory[face].Name}
    }
    /if (!${Bool[${Alert_Mask}]}) {
        /varset Alert_Mask
    }

    /call Core_AddTask_ZoneChange Alert_ZoneChange

/RETURN

SUB Alert_Main
    /if (${InCombat}) {
        /if (${Raid.Leader.Name.Equal[${Me}]} || ${Group.Leader.Name.Equal[${Me}]}) {
            | ikkinz3 294
            /if (${Zone.ID} == 294) {
                /doevents setClass_Warrior
                /doevents setClass_Shaman
                /doevents setClass_Beastlord
                /doevents setClass_Necromancer
                /doevents setClass_Cleric
                /doevents setClass_Shadowknight
                /doevents setClass_Monk
                /doevents setClass_Bard
                /doevents setClass_Paladin
                /doevents setClass_Rogue
                /doevents setClass_Enchanter
                /doevents setClass_Wizard
                /doevents setClass_Berserker
                /doevents setClass_Magician
                /doevents setClass_Druid
                /doevents setClass_Ranger
                /doevents startClass_Priests
                /doevents startClass_Hybrids
                /doevents startClass_Casters
                /doevents startClass_Melees
                /return
            }
        } else /if (${Zone.ID} == 296) {
            /doevents InktutaDeathtouch
            /return
        } else /if (${Zone.ID} == 317) {
            /doevents angmask
            /return
        }
    } else {
        /if (${Raid.Leader.Name.Equal[${Me}]} || ${Group.Leader.Name.Equal[${Me}]}) {
            /if (${Zone.ID} == 292) {
                /doevents Define_UquaKey
                /return
            }
        }
    }
/RETURN

SUB Alert_LowMain
    /doevents charFlag

    /if (${Zone.ID} == 317) {
        /doevents maskcheck
    } else /if (${Zone.ID} == 279 || ${Zone.ID} == 298 || ${IsSafe}) {
        /doevents tacvibook

        /if (${IsSafe}) {
            /doevents ldonaug
            /if (${Zone.ID} == 202) {
                /doevents poksummoncorpse
            }
        }
    } else /if (${Zone.ID} == 306) {
        | MPG group trial Subversion
        /call alert_mpgopenbox
    }
/RETURN

SUB Alert_ZoneChange
    /if (${CurrentZone} != 317) {
        /call Basic_QEquip face 4 "${Alert_Mask}"
    }
/RETURN

#EVENT maskcheck "#*# tells the raid#*#equipmask #*#'"
#EVENT maskcheck "#*#you feel the magical aura filling the room#*#"
SUB EVENT_maskcheck
    /call Basic_QEquip face 1 "Mirrored Mask"
/RETURN

#EVENT ldonaug "<#1#> ldonaug"
SUB EVENT_ldonaug(line, chatSender)
    /if (${chatSender.NotEqual[${Me}]}) /return
    /if (${Skill_memorizing}) /varset Skill_memorizing FALSE
    /call Basic_AutoInventory
    | takish
    /call alert_goandhail "Ruanya Windleaf" nro
    /call alert_goandhail "Farwein Windrun" sro "-1500 913 -20"
    /call alert_goandhail "Ruanya Windleaf" nro
    /call alert_goandhail "Niflel Faliwae" gfay
    /call alert_goandhail "Ruanya Windleaf" nro

    | guk
    /call alert_goandhail "Selephra Giztral" sro
    /call alert_goandhail "Bealya Tanilsuia" commons
    /call alert_goandhail "Selephra Giztral" sro
    /call alert_goandhail "Deblik Grumblok" innothule
    /call alert_goandhail "Selephra Giztral" sro

    | mistmoore
    /call alert_goandhail "Vual Stoutest" butcher
    /call alert_goandhail "Henai Silentwalker" cauldron
    /call alert_goandhail "Vual Stoutest" butcher
    /call alert_goandhail "Elwinn Prelliaen" lfay
    /call alert_goandhail "Vual Stoutest" butcher

    | miragul
    /call alert_goandhail "Teria Grinntli" everfrost
    /call alert_goandhail "Ubzial Iyeaql" qeytoqrg
    /call alert_goandhail "Teria Grinntli" everfrost

    | rujark
    /call alert_goandhail "Barstre Songweaver" commonlands
    /call alert_goandhail "Ginehl Wiquar" freportw
    /call alert_goandhail "Barstre Songweaver" commonlands
    /call alert_goandhail "Shumpi Wimahnn" highpasshold
    /call travel_goandrun 80 -63 -309 -20
    /call alert_goandhail "Barstre Songweaver" commonlands

/RETURN

SUB alert_goandhail(string whoto, string whereto, string loc)
    /call travel_zone ${whereto}
    /declare whoid int local
    /while (!${Bool[${Spawn[${whoto}].ID}]}) {
        /delay 1
    }

    /if (${Defined[loc]}) {
        /call travel_goloc ${loc}
    }

    /varset whoid ${Spawn[${whoto}].ID}
    /call travel_go ${whoid}
    /declare i int local
    /for i 1 to 25
        /target id ${whoid}
        /hail
        /delay 5
    /next i
/RETURN

SUB alert_mpgopenbox
    /if (${Target.Name.Find[chest]} && ${Target.Distance} < 20) {
        /open
    }
/RETURN

#EVENT tacvibook "<#1#> tacvibook"
SUB EVENT_tacvibook(line, chatSender)
    /if (${chatSender.NotEqual[${Me}]}) /return

    /if (${Zone.ID} == 298) {
        /declare rid int local ${NearestSpawn[remains].ID}
        /if (${Bool[${rid}]}) {
            /if (${NearestSpawn[remains].Type.Equal[chest]}) {
                /if (${Spawn[${rid}].Distance} < 10) {
                    /call Bind_NetGroupButMeCommand "/varset loot_PauseLoot 10s"
                    /call MobTarget ${rid}
                    /delay 5
                    /open
                    /delay 5
                    /call loot_area
                }
            }
        }
        /declare bookok bool local TRUE
        /if (!${Bool[${FindItem[=Zajeer's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Zajeer's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Zajeer's Writings.
        }

        | upper hallway
        /if (!${Bool[${FindItem[=Frizznik's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Frizznik's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Frizznik's Writings.
        }
        /if (!${Bool[${FindItem[=Xenaida's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Xenaida's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Xenaida's Writings.
        }
        /if (!${Bool[${FindItem[=Kaikachi's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Kaikachi's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Kaikachi's Writings.
        }
        /if (!${Bool[${FindItem[=Rytan's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Rytan's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Rytan's Writings.
        }
        /if (!${Bool[${FindItem[=Rashere's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Rashere's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Rashere's Writings.
        }

        | lower hallway
        /if (!${Bool[${FindItem[=Absor's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Absor's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Absor's Writings.
        }
        /if (!${Bool[${FindItem[=Prathun's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Prathun's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Prathun's Writings.
        }
        /if (!${Bool[${FindItem[=Valtron's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Valtron's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Valtron's Writings.
        }
        /if (!${Bool[${FindItem[=Wijdan's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Wijdan's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Wijdan's Writings.
        }
        /if (!${Bool[${FindItem[=Vahlara's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Vahlara's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Vahlara's Writings.
        }

        | last room
        /if (!${Bool[${FindItem[=Maddoc's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Maddoc's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Maddoc's Writings.
        }
        /if (!${Bool[${FindItem[=Lyndro's Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Lyndro's Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Lyndro's Writings.
        }
        /if (!${Bool[${FindItem[=Silius' Writings].ID}]}) {
            /echo \ar Tacvibook: You don't have Silius' Writings.
            /varset bookok FALSE
        } else {
            /echo \ag Tacvibook: You have Silius' Writings.
        }
        /if (${bookok}) {
            /echo \ar Tacvibook: You have all of the Book!
        }
        /hidec none

    } else {
        /echo aaa
        /if (${Bool[${FindItem[=Tome of New Beginnings].ID}]}) {
            /echo \ar Tacvibook: You already have Tome of New Begining!! Aborting.
            /return
        } else {
            /declare i int local
            /declare e int local
            /declare ItemToFind string local Tome of New Beginnings
            /for i 1 to 24
                /if (${Me.Bank[${i}].Name.Find[${ItemToFind}]}) {
                    /echo \ar Tacvibook: You already have Tome of New Begining in bankslot ${i} !! Aborting.
                    /return
                }
                /if (${Me.Bank[${i}].Container}) {
                    /for e 1 to ${Me.Bank[${i}].Container}
                        /if (${Me.Bank[${i}].Item[${e}].Name.Find[${ItemToFind}]}) {
                            /echo \ar Tacvibook: You already have Tome of New Begining in bankslot ${i} containerslot ${e} !! Aborting.
                            /return
                        }
                    /next e
                }
            /next i
        }
        /echo Tacvibook: You dont have Tome of New Beginnings. Continue to give to NPC

        | move to abysmal
        /if (${IsSafe}) {
            /call travel_zone "abysmal"
        } else /if (${Zone.ID} != 279) {
            /echo \ar Tacvibook: Im not in Abysmal Sea!
            /return
        }

        /if (${Bool[${FindItem[=Lyndro's Writings].ID}]}) {
            /call travel_goid ${Spawn[npc Sophia].ID}
            /if (${Spawn[npc Sophia].Distance} < 11) {
                /target Sophia
                /call EVENT_givetonpc "<${Me}> 1 giveitem Lyndro's Writings" ${Me} 1 "Lyndro's Writings"
            } else {
                /echo Tacvibook: Sphia Florence is not near. (${Spawn[Sophia].Distance})
            }
        } else {
            /echo Tacvibook: I dont have Lyndro(Sophia)
        }
        /if (${Bool[${FindItem[=Silius' Writings].ID}]}) {
            /call travel_goid ${Spawn[npc Verna Abella].ID}
            /if (${Spawn[Verna Abella].Distance} < 11) {
                /target Verna Abella
                /call EVENT_givetonpc "<${Me}> 1 giveitem Silius' Writings" ${Me} 1 "Silius' Writings"
            } else {
                /echo Tacvibook: Verna Abella is not near. (${Spawn[Verna Abella].Distance})
            }
        } else {
            /echo Tacvibook: I dont have Silius(Verna)
        }

        /if (${Bool[${FindItem[=Rashere's Writings].ID}]}) {
            /call travel_goid ${Spawn[npc Jenna].ID}
            /if (${Spawn[Jenna].Distance} < 11) {
                /target Jenna
                /call EVENT_givetonpc "<${Me}> 1 giveitem Rashere's Writings" ${Me} 1 "Rashere's Writings"
            } else {
                /echo Tacvibook: Jenna Gurell is not near. (${Spawn[Jenna].Distance})
            }
        } else {
            /echo Tacvibook: I dont have Rashere(Jenna)
        }

        /if (${Bool[${FindItem[=Prathun's Writings].ID}]}) {
            /call travel_goid ${Spawn[npc April].ID}
            /if (${Spawn[April].Distance} < 11) {
                /target April
                /call EVENT_givetonpc "<${Me}> 1 giveitem Prathun's Writings" ${Me} 1 "Prathun's Writings"
            } else {
                /echo Tacvibook: Aprilla Marrow is not near. (${Spawn[April].Distance})
            }
        } else {
            /echo Tacvibook: I dont have Prathun(April)
        }

        /if (${Bool[${FindItem[=Maddoc's Writings].ID}]}) {
            /call travel_goid ${Spawn[npc Gizzmu].ID}
            /if (${Spawn[npc Gizzmu].Distance} < 11) {
                /target Gizzmu
                /call EVENT_givetonpc "<${Me}> 1 giveitem Maddoc's Writings" ${Me} 1 "Maddoc's Writings"
            } else {
                /echo Tacvibook: Gizzmu the Wise is not near. (${Spawn[Gizzmu].Distance})
            }
        } else {
            /echo Tacvibook: I dont have Maddoc(Gizzmu)
        }

        /if (${Bool[${FindItem[=Absor's Writings].ID}]}) {
            /call travel_goid ${Spawn[npc Irena].ID}
            /if (${Spawn[npc Irena].Distance} < 11) {
                /target Irena
                /call EVENT_givetonpc "<${Me}> 1 giveitem Absor's Writings" ${Me} 1 "Absor's Writings"
            } else {
                /echo Tacvibook: Irena Pereira is not near. (${Spawn[Irena].Distance})
            }
        } else {
            /echo Tacvibook: I dont have Absor(Irena)
        }


        /call travel_goid ${Spawn[npc Szostek].ID}
        /if (${Spawn[npc Szostek].Distance} < 11) {
            /target Szostek
            /call EVENT_givetonpc "<${Me}> 1 giveitem Frizznik's Writings" ${Me} 1 "Frizznik's Writings"
            /if (${Macro.Return}) /delay 5
            /call EVENT_givetonpc "<${Me}> 1 giveitem Kaikachi's Writings" ${Me} 1 "Kaikachi's Writings"
            /if (${Macro.Return}) /delay 5
            /call EVENT_givetonpc "<${Me}> 1 giveitem Rytan's Writings" ${Me} 1 "Rytan's Writings"
            /if (${Macro.Return}) /delay 5
            /call EVENT_givetonpc "<${Me}> 1 giveitem Vahlara's Writings" ${Me} 1 "Vahlara's Writings"
            /if (${Macro.Return}) /delay 5
            /call EVENT_givetonpc "<${Me}> 1 giveitem Valtron's Writings" ${Me} 1 "Valtron's Writings"
            /if (${Macro.Return}) /delay 5
            /call EVENT_givetonpc "<${Me}> 1 giveitem Wijdan's Writings" ${Me} 1 "Wijdan's Writings"
            /if (${Macro.Return}) /delay 5
            /call EVENT_givetonpc "<${Me}> 1 giveitem Zajeer's Writings" ${Me} 1 "Zajeer's Writings"
            /if (${Macro.Return}) /delay 5
            /call EVENT_givetonpc "<${Me}> 1 giveitem Xenaida's Writings" ${Me} 1 "Xenaida's Writings"
            /if (${Macro.Return}) /delay 5

            /say my task is complete
            /delay 5
            /call Basic_AutoInventory
        } else {
            /echo Tacvibook: De'Van Szostek is not near. (${Spawn[Szostek].Distance})
        }
    }
/RETURN

#EVENT poksummoncorpse "<#1#> summonmycorpse"
SUB EVENT_poksummoncorpse(line, chatSender)
    /if (!${IsDead}) {
        /if (${IsCLR}) {
            /if (${Zone.ID} != 202) {
                /call travel_zone poknowledge
                /if (!${Macro.Return}) /return
            }
            /hidec allbutgroup
            |/call travel_goid ${Spawn[priest of luclin].ID}
            /call travel_goloc 444 96 -150
        }
        /return
    } else /if (${Bool[${Me.Inventory[chest].ID}]}) {
        /echo PoKsummon: Looks like you are not dead
        /return
    }
    /if (${Zone.ID} != 202) /return

    /declare i int local
    /if (${Me.Cash} == 900000) {
        /split 900
        /delay 1
    } else {
        /for i 1 to 30
            /if (${Me.Cash} >= 150000) /break
            /delay 1
        /next i
    }

    /if (${Me.Cash} < 150000) {
        /echo POKsummon: I dont have enough cash to summon.
        /return
    }
    /if (${Bool[${Spawn[corpse ${Me}].ID}]}) {
        /echo PoKsummon: You already have a corpse.
        /return
    }

    /squelch /hidec allbutgroup
    /delay 1s
    /call travel_goid ${Spawn[priest of luclin].ID}
    /if (!${Macro.Return}) {
        /echo PoKsummon: cannot go to corpse summoner
        /return
    }
    /target id ${Spawn[priest of luclin].ID}
    /say summon
    /delay 1s
    /while (${Me.Cash} >= 150000) {
        /if (${Bool[${Spawn[pccorpse ${Me}].ID}]}) {
            /break
        } else /if (${Bool[${Me.Inventory[chest].ID}]}) {
            /break
        }
        /squelch /hidec allbutgroup
        /target id ${Spawn[priest of luclin].ID}
        /say summon
        /delay 1s
    }
    /if (!${Bool[${Spawn[corpse ${Me}].ID}]}) {
        /echo PoKsummon: out of money
    }

    /call travel_goloc 444 96 -150
    /target id ${Me.ID}
    /delay 1

    /corpse

    /echo PoKsummon: Done

/RETURN

#EVENT hereticcheck "#*# blistered backside are you?#*#"
SUB EVENT_hereticcheck
    /doevents flush hereticcheck
    /varset yoho_flag 1
    /echo Yoho: Heretic check detected
/RETURN

#EVENT hereticfail "#*#Speed up the digging my pets#*#"
SUB EVENT_hereticfail
    /doevents flush hereticfail
    /varset yoho_flag -1
    /echo Yoho: Seems quest is over. Failed...
/RETURN

#EVENT hereticok "#*#a druid that wants to be a miner#*#"
SUB EVENT_hereticok
    /doevents flush hereticok
    |/varset yoho_flag 2
    /echo Yoho: Heretic response detected
    /squelch /target clear
/RETURN

| say "i will do it" to skeleton and equip the recived minig pick before start
#EVENT yohoevent "<#1#> yoho"
SUB EVENT_yohoevent(line, chatSender)
    /if (${chatSender.NotEqual[${Me}]}) /return

    /declare skeletonid int local ${Target.ID}
    /declare hereticid int local ${Spawn[abandoned].ID}
    /declare loopflag bool local TRUE
    /declare yoho_flag int outer

    /declare echotimer timer local

    /doevents flush hereticcheck
    /doevents flush hereticfail

    /moveto loc -2557 -347 dist 10
    /delay 1s

    /while (${loopflag}) {
        /if (${Cursor.ID}) {
            /if (${Cursor.ID} == 62828) {
                /echo Yoho: Item on my hand(${Cursor.Name}). Success
                /break
            } else {
                /echo Yoho: Unknown item(${Cursor.Name}) in my hand...
            }
        }
        /if (${echotimer} == 0) {
            /squelch /target clear
            /delay 5
            /say Yo ho. No sun!!
            /delay 5
            /say A skeleton's day is never done.
            /delay 5
            /varset echotimer 4s
            /mapshow npc
        }

        /varset yoho_flag 0
        /doevents hereticcheck
        /doevents hereticfail
        /delay 5

        /if (${yoho_flag} == 1) {
            /call MobTarget ${hereticid}
            /delay 5
            /say I am on the job
            /delay 1s
            /doevents hereticok
        } else /if (${yoho_flag} == -1) {
            /break
        }

        /delay 5
    }
    /echo Yoho: end

/return


#EVENT charFlag "You receive a character flag#*#"
SUB EVENT_charFlag
    /doevents flush charFlag
	/bc Alert: I have recieved a character flag!
/return


SUB checkmaskequip
    /if (${InCombat}) /return
    /if (${Bool[${Me.Casting.ID}]}) /return
    /if (${Basic_StopExchangePeriod} > 0) /return
    /if (${Me.Inventory[face].Name.NotEqual[Mirrored Mask]}) {
        /if (${FindItem[=Mirrored Mask].ID}) {
            /echo Alert: Equipping Mirrored Mask
            /call Basic_SafeExchange "Mirrored Mask" face
        }
    }
/RETURN


#event angmask "#*#You feel a gaze of deadly power focusing on you#*#"
Sub event_angmask
    /call Alert_Angmask
/RETURN

Sub Alert_Angmask
    /bc [+r+]Alert: ********* GAZE ON ME ********* ${Time} 8

    /if (${Me.Inventory[face].Name.Equal[Mirrored Mask]}) {
        /echo starting to use mask
        /declare i int local 0
        /declare s int local

        /call Buff_RemoveShortBuff "Harmshield"
        /if (${Bool[${Me.Casting.ID}]}) /stopcast
        /if (${IsBRD}) /call bard_stopSong

        :mirrortries
        /echo attempt to cast

        /if (${FindItem[=Mirrored Mask].TimerReady} > 0) {
            /echo [+r+]Alert: mirror mask still in cooldown ${FindItem[=Mirrored Mask].TimerReady}
            /beep sounds/mail2
            /return
        }

        /if (${Bool[${Cursor.ID}]}) {
            /beep sounds/mail2
            /bc [+r+]Alert: ${Cursor.Name} is in my hand!!!
            /call Basic_AutoInventory
            /if (!${Macro.Return}) /return
        }

        /call Skill_Cast -1 "Mirrored Mask"

        /for s 1 to 20
            /if (${Bool[${Me.Song[Reflective Skin].ID}]}) /break
            /delay 1
        /next s
        /if (!${Bool[${Me.Song[Reflective Skin].ID}]}) {
            /varcalc i ${i}+1
            /if (${i} != 4) {
                /goto :mirrortries
            }
            /echo mirror buff failed ${i}
        }
    } else {
        /echo mask is not equipped
    }

    /if (!${Bool[${Me.Song[Reflective Skin].ID}]}) {
        /bc [+r+]********* FAIL TO USE MASK ********* ${Time}
        /beep sounds/mail2
    } else {
        /bc [+g+]********* MASK USED!! ********* ${Time}
    }
/return

#event Define_UquaKey "#*#The #1# must unlock the door to the next room.#*#"
SUB event_Define_UquaKey(string line, string KeyToUse)
	/rs >>^<< The ${KeyToUse} unlocks the door >>^<<
	/popup >>^<< The ${KeyToUse} unlocks the door >>^<<
/return

#event setClass_Warrior "#*#Brute force and brawn#*#"
#event setClass_Shaman "#*#Cringes at the appearance of talismans#*#"
#event setClass_Beastlord "#*#Deep gashes of feral savagery#*#"
#event setClass_Necromancer "#*#Doom of death#*#"
#event setClass_Cleric "#*#Dread of celestial spirit#*#"
#event setClass_Shadowknight "#*#It appears that this creature dreads the strike of death#*#"
#event setClass_Monk "#*#Focused tranquility#*#"
#event setClass_Bard "#*#Foreboding melody#*#"
#event setClass_Paladin "#*#fears a holy blade#*#"
#event setClass_Rogue "#*#Ignores anything behind it#*#"
#event setClass_Enchanter "#*#Mind and body are vulnerable#*#"
#event setClass_Wizard "#*#Falters when struck with the power of the elements#*#"
#event setClass_Berserker "#*#Shies from heavy blades#*#"
#event setClass_Magician "#*#Summoned elements#*#"
#event setClass_Druid "#*#The creature seems weak in the face of the power of nature#*#"
#event setClass_Ranger "#*#True shots and fast blades#*#"

SUB event_setClass_Warrior
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< WARRIOR >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< WARRIOR >>^<<
/return

SUB event_setClass_Shaman
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< SHAMAN >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< SHAMAN >>^<<
/return

SUB event_setClass_Beastlord
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< BEASTLORD >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< BEASTLORD >>^<<
/return

SUB event_setClass_Necromancer
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< NECROMANCER >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< NECROMANCER >>^<<
/return

SUB event_setClass_Cleric
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< CLERIC >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< CLERIC >>^<<
/return

SUB event_setClass_Shadowknight
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< SHADOWKNIGHT >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< SHADOWKNIGHT >>^<<
/return

SUB event_setClass_Monk
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< MONK >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< MONK >>^<<
/return

SUB event_setClass_Bard
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< BARD >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< BARD >>^<<
/return

SUB event_setClass_Paladin
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< PALADIN >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< PALADIN >>^<<
/return

SUB event_setClass_Rogue
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< ROGUE >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< ROGUE >>^<<
/return

SUB event_setClass_Enchanter
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< ENCHANTER >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< ENCHANTER >>^<<
/return

SUB event_setClass_Wizard
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< WIZARD >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< WIZARD >>^<<
/return

SUB event_setClass_Berserker
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< BERSERKER >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< BERSERKER >>^<<
/return

SUB event_setClass_Magician
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< MAGICIAN >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< MAGICIAN >>^<<
/return

SUB event_setClass_Druid
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< DRUID >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< DRUID >>^<<
/return
SUB event_setClass_Ranger
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< RANGER >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< RANGER >>^<<
/return

#event startClass_Priests "#*#The creature cannot stand up to the power of healers#*#"
#event startClass_Hybrids "#*#The creature appears weak to the combined effort of might and magic#*#"
#event startClass_Casters "#*#The creature will perish under the strength of intelligent magic#*#"
#event startClass_Melees "#*#The creature appears weak to the combined effort of strength and cunning#*#"
SUB event_startClass_Priests
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< PRIEST >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< PRIEST >>^<<
/return

SUB event_startClass_Hybrids
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< HYBRID >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< HYBRID >>^<<
/return

SUB event_startClass_Casters
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< CASTER >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< CASTER >>^<<
/return

SUB event_startClass_Melees
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs Spawn must be killed by a >>^<< MELEE >>^<<
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay Spawn must be killed by a >>^<< MELEE >>^<<
/return

#event InktutaDeathtouch "#*#thoughts of a cursed trusik invade your mind#*#"
SUB event_InktutaDeathtouch
    /rs I, >>^<< ${Me.Name} >>^<<, who am about to die, salute you!!
/return

#event goDuck "#*#From the corner of your eye, you notice a Kyv taking aim at your head. You should duck."
#event dragornPBAE "#*#You notice that the Dragorn before you is preparing to cast a devastating close-range spell."
#event dragornBlast "You notice that the Dragorn before you is making preparations to cast a devastating spell.  Doing enough damage to him might interrupt the process."
#event dragornThorn "#*#The Dragorn before you is sprouting sharp spikes."
#event dragornReflect "#*#The Dragorn before you is developing an anti-magic aura."

Sub Event_dragornPBAE
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs @@@ PBAE INC @@@
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay @@@ PBAE INC @@@
/return

Sub Event_dragornBlast
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs @@@ BLAST INC @@@
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay @@@ BLAST INC @@@
/return

Sub Event_dragornThorn
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs ^^^ Thorns ON ^^^
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay ^^^ Thorns ON ^^^
/return

Sub Event_dragornReflect
    /if (${Raid.Leader.Name.Equal[${Me}]}) /rs ~~~ Reflect ON ~~~
    /if (${Group.Leader.Name.Equal[${Me}]}) /gsay ~~~ Reflect ON ~~~
/return

SUB Alert_CycleTarget(spellID)
    | choose "an egg sack"
    /if (${Zone.ID} == 63) {
        /declare nid int local
        /declare ttid int local -1
        /declare tthp int local -1

        /declare i int local
        /for i 1 to 5
            /varset nid ${NearestSpawn[${i},an egg sack].ID}
            /if (!${Bool[${nid}]}) /return -1

            /call Basic_CheckMob ${nid}
            /if (!${Macro.Return}) /continue
            /if (!${Spawn[${nid}].LineOfSight}) /continue
            /if (${Defined[combat_dot_${nid}_${spellID}]}) {
                /if (${combat_dot_${nid}_${spellID}} > 0) /continue
            }
            /if (${Spawn[${nid}].PctHPs} > ${tthp}) {
                /varset ttid ${nid}
                /varset tthp ${Spawn[${nid}].PctHPs}
                /if (${tthp} >= 98) {
                    /break
                }
            }
        /next i
        /return ${ttid}
    }
/RETURN -1
