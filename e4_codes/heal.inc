SUB Heal_Setup
    | Control
    /declare Heal_DontUseTempItems bool outer FALSE

    | Parameter

    | Alias

    | Internal
    /declare i int local

    /declare heal_enable bool outer FALSE
    /declare heal_potenable bool outer FALSE

    /declare heal_threshould int outer -1

    | reading Auto heal settings
    /call IniToArrayV "${CharacterIni},Heals,Heal#" heal_List
    /if (${Defined[heal_List]}) {
        /if (${heal_List.Size} > 0) {
            /for i 1 to ${heal_List.Size}
                /call Skill_CreateData heal_auto${i} "${heal_List[${i}]}"
                /if (${heal_auto${i}_ok} == 1) {
                    /if (!${heal_auto${i}_hot} && ${heal_auto${i}_all}) {
                        /if (${heal_auto${i}_hpbelow} < 95) {
                            /if (${heal_threshould} < ${heal_auto${i}_hpbelow}) {
                                /varcalc heal_threshould ${heal_auto${i}_hpbelow}-5
                            }
                        }
                    }
                }
            /next i
            /varset heal_enable TRUE
            /echo Heal: Threshould is ${heal_threshould}%
        }
    }
    | reading potion settings
    /call IniToArrayV "${CharacterIni},Heals,Potion#" heal_PotList
    /if (${Defined[heal_PotList]}) {
        /if (${heal_PotList.Size} > 0) {
            /for i 1 to ${heal_PotList.Size}
                /call Skill_CreateData heal_potauto${i} "${heal_PotList[${i}]}"
                /varset heal_potauto${i}_notarget TRUE
            /next i
            /varset heal_potenable TRUE
        }
    }

	| disable pet heal with orb of souls
	/call iniToVarV "${CharacterIni},Heals,Enable PetHeal by Orb of Souls(On/Off)" EnablePetOoS bool outer
    /if (!${Macro.Return}) {
        /declare EnablePetOoS bool outer FALSE
    }

    /call Core_AddTask_LowMain Heal_LowMain

    /declare heal_orbtimer timer outer

    /call heal_HoTSetup

    | rod setup
    | Large modulation shard
    /declare heal_rod_79322_mana int outer 3000
    /declare heal_rod_79322_hp int outer 12000
    | Middle
    /declare heal_rod_79321_mana int outer 1500
    /declare heal_rod_79321_hp int outer 7500
    | Small
    /declare heal_rod_79320_mana int outer 1250
    /declare heal_rod_79320_hp int outer 1500

    | enc orb manager
    /declare heal_iwantazure bool outer FALSE
    /declare heal_iwantsanguine bool outer FALSE
    /if (!${IsDead}) {
        /if (!${Bool[${FindItem[Azure Mind Crystal].ID}]}) /varset heal_iwantazure TRUE
        /if (!${Bool[${FindItem[Sanguine Mind Crystal].ID}]}) /varset heal_iwantsanguine TRUE
    }

    /declare heal_error string outer
/RETURN

SUB Heal_Main
    /call Heal_PotCheck
    /call Heal_Check
    /if (${IsTank}) {
        /if (!${InCombat}) {
            |/if (${Moving} >= 5) {
                /call Buff_RemoveBuff "Transcendent Torpor"
            |}
        }
    }
/RETURN

SUB Heal_LowMain
    /call heal_autotempitem
    /call Heal_UseOrb
/RETURN

SUB Heal_UpdateMembers
    /declare i int local

    /if (${Defined[heal_List]}) {
        /for i 1 to ${heal_List.Size}
            |/echo hu ${heal_auto${i}_spellname}
            /call Skill_UpdateData heal_auto${i}
            |/echo ----
        /next i
    }
/RETURN


| Use Orb of Souls automatically
SUB heal_autotempitem
	/if (${Heal_DontUseTempItems}) /return
    /if (${heal_orbtimer}) /return
	/if (${Bool[${Me.Casting.ID}]}) /return
    /if (${Bool[${Cursor.ID}]}) /return

    /declare i int local
    /if (${Math.Calc[${Me.MaxHPs}-${Me.CurrentHPs}].Int} > 5000 || ${Me.PctHPs} < 50) {
        /if (${Bool[${FindItem[Sanguine Mind Crystal].ID}]}) {
            /varset heal_iwantsanguine FALSE
            /if (${FindItem[Sanguine Mind Crystal].TimerReady} == 0) {
                /call Skill_Cast -1 "${FindItem[Sanguine Mind Crystal].Name}"
                /varset heal_orbtimer 1s
                /if (!${Bool[${FindItem[Sanguine Mind Crystal].ID}]}) /varset heal_iwantsanguine TRUE
                /return TRUE
            }
        } else {
            /varset heal_iwantsanguine TRUE
        }
    }

    /if (!${IsNoMana} && !${IsDead}) {
        /declare emptymana int local ${Math.Calc[${Me.MaxMana}-${Me.CurrentMana}].Int}
        /if (${emptymana} >= 3000) {
            /if (${Bool[${FindItem[Azure Mind Crystal].ID}]}) {
                /varset heal_iwantazure FALSE
                /if (${FindItem[Azure Mind Crystal].TimerReady} == 0) {
                    /call Skill_Cast -1 "${FindItem[Azure Mind Crystal].Name}"
                    /varset heal_orbtimer 1s
                    /if (!${Bool[${FindItem[Azure Mind Crystal].ID}]}) /varset heal_iwantazure TRUE
                    /return TRUE
                }
            } else {
                /varset heal_iwantazure TRUE
            }
        }
        /declare rodid int local ${FindItem[Modulation Shard].ID}
        /if (${Bool[${rodid}]}) {
            /if (${FindItem[Modulation Shard].TimerReady} == 0) {
                /if (!${Defined[heal_rod_${rodid}_mana]}) {
                    /echo \arRod: Unknown ID ${rodid}
                } else /if (${emptymana} >= ${heal_rod_${rodid}_mana} && ${Me.CurrentHPs} > ${heal_rod_${rodid}_hp}) {
                    /call Skill_Cast -1 "${FindItem[Modulation Shard].Name}"
                    |/nomodkey /ItemNotify "${FindItem[Modulation Shard].Name}" rightmouseup
                    /varset heal_orbtimer 1s
                    /return TRUE
                }
            }
        }
    }
/RETURN FALSE

SUB Heal_UseOrb
    /if (${heal_orbtimer} == 0) {
        /declare i int local
        /declare idid int local
        /declare orbitem string local
        /if (${FindItem[=Orb of Shadows].ID}) {
            /varset orbitem Orb of Shadows
        } else {
            /varset orbitem Orb of Souls
        }
        /if (${Bool[${FindItem[=${orbitem}].ID}]}) {
            /if (${FindItem[=${orbitem}].TimerReady} == 0) {
                /if (${Group.Members}) {
                    /for i 0 to ${Group.Members}
                        /if (${Group.Member[${i}].Dead}) /continue
                        /if (!${NetBots[${Group.Member[${i}].Name}].InZone}) /continue
                        /if (${NetBots[${Group.Member[${i}].Name}].PctHPs} < 75) {
                            /if (${Group.Member[${i}].Distance} < 100) {
                                /varset idid ${Target.ID}
                                /call Skill_Cast ${Group.Member[${i}].ID} "${orbitem}"
                                /echo Heal: Using ${orbitem} to ${Group.Member[${i}].Name}
                                /call MobTarget ${idid}
                                /varset heal_orbtimer 1s
                                /return TRUE
                            }
                        }
                    /next i
                    /if (${EnablePetOoS}) {
                        /for i 0 to ${Group.Members}
                            /if (${Group.Member[${i}].Dead}) /continue
                            /if (!${NetBots[${Group.Member[${i}].Name}].InZone}) /continue
                            /if (${Group.Member[${i}].Pet.ID}) {
                                /if (${NetBots[${Group.Member[${i}].Name}].PetHP} < 50) {
                                    /if (${Group.Member[${i}].Pet.Distance} < 100) {
                                        /varset idid ${Target.ID}
                                        /call Skill_Cast ${Group.Member[${i}].ID} "${orbitem}"
                                        /echo Heal: Using ${orbitem} to ${Group.Member[${i}].Name}
                                        /call MobTarget ${idid}
                                        /varset heal_orbtimer 1s
                                        /return TRUE
                                    }
                                }
                            }
                        /next i
                    }
                } else {
                    /if (${Me.PctHPs} < 75) {
                        /varset idid ${Target.ID}
                        /call Skill_Cast ${Group.Member[${i}].ID} "${orbitem}"
                        /echo Heal: Using ${orbitem} to ${Group.Member[${i}].Name}
                        /call MobTarget ${idid}
                        /varset heal_orbtimer 1s
                        /return TRUE
                    }
                }
            }
        }
    }
/RETURN FALSE

| auto potion(actually, this is a feature to cast selfonly non-target spell)
SUB Heal_PotCheck
    /if (!${heal_potenable}) /return FALSE
    /if (${IsSafe}) /return FALSE
    /varset heal_error
    /call Skill_AmIReady
    /if (${Macro.Return} != ${CAST_SUCCESS}) {
        /varset heal_error I am not ready ${Macro.Return}
        /return FALSE
    }

    /declare s int local
    /for s 1 to ${heal_PotList.Size}
        /if (${heal_potauto${s}_ok} != 2) {
            /if (${heal_potauto${s}_ok} < 1) {
                /call Skill_CreateData heal_potauto${s} "${heal_PotList[${s}]}"
                /if (!${Macro.Return}) {
                    /continue
                }
                /varset heal_potauto${s}_notarget TRUE
            }
        }
        /varset heal_error ${heal_error}#${s}${heal_potauto${s}_spellname}|

        /call Skill_MySpellReady ${heal_potauto${s}_spelltype} "${heal_potauto${s}_spellname}"
        /if (${Macro.Return} != ${CAST_SUCCESS} && ${Macro.Return} != ${CAST_NOTMEMMED}) /continue

        /if (${heal_potauto${s}_combat}) {
            /if (!${InCombat}) /continue
            /if (${Following}) /continue
            /if (!${Attacking}) /continue
        } else /if (${heal_potauto${s}_calm}) {
            /if (${Buff_PausePeriod}) /continue
            /if (${InCombat}) /continue
            /if (${Following}) /continue
            /if (${Me.Invis}) /continue
        } else /if (${heal_potauto${s}_notfollowing}) {
            /if (${Following}) /continue
        }
        /if (${heal_potauto${s}_raid} == 1) {
            /if (${Raid.Members} == 0) {
                /continue
            }
        } else /if (${heal_potauto${s}_raid} == -1) {
            /if (${Raid.Members} > 0) {
                /continue
            }
        }
        /if (${heal_potauto${s}_manabelow} <= ${Me.PctMana}) /continue
        /if (${heal_potauto${s}_manaover} > ${Me.PctMana}) /continue
        /if (${heal_potauto${s}_endbelow} <= ${Me.PctEndurance}) /continue
        /if (${heal_potauto${s}_endover} > ${Me.PctEndurance}) /continue

        /if (${heal_potauto${s}_hpbelow} != 200) {
            /if (${heal_potauto${s}_hpbelow} <= ${Me.PctHPs}) /continue
        }
        /if (${heal_potauto${s}_hpover} != 0) {
            /if (${heal_potauto${s}_hpover} > ${Me.PctHPs}) /continue
        }

        /call Skill_Cast_Data -1 heal_potauto${s} 100
        /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
        /varset heal_error ${heal_error}*fwr ${s} ${heal_potauto${s}_spellname} ${Macro.Return}
    /next s
/RETURN FALSE

SUB Heal_Check
    /if (${autoheal_enable}) {
        /call AutoHeal_Check
    } else {
        /call Heal_rawCheck
    }
/return ${Macro.Return}

| heal algo v2
SUB Heal_rawCheck
    /varset heal_error
    /if (!${heal_enable}) {
        /varset heal_error not enabled
        /return FALSE
    }
    /call Skill_AmIReady
    /if (${Macro.Return} != ${CAST_SUCCESS}) {
        /varset heal_error I am not ready ${Macro.Return}
        /return FALSE
    }
    /declare hsize int local
    /declare tempw string local
    /declare temphp int local
    /declare avehp int local 0
    /declare avect int local 0
    /declare tempid int local -1

    /declare whoto string local
    /declare whoid int local -1

    /declare pethp int local
    /declare petto string local
    /declare petid int local -1

    /declare i int local
    /declare s int local

    /varset temphp 1000
    /varset pethp 1000
    /for i 1 to ${Member.Size}
        /varset tempw ${Member[${i}]}
        /if (!${NetBots[${tempw}].InZone}) /continue
        /varset tempid ${NetBots[${tempw}].ID}
        /if (!${Bool[${Spawn[${tempid}].ID}]}) /continue
        /if (${Spawn[${tempid}].Dead}) /continue

        /if (${InGroup[${tempw}]}) {
            /if (${Spawn[${tempid}].Distance} < 70) {
                /varcalc avehp ${avehp}+${NetBots[${tempw}].PctHPs}
                /varcalc avect ${avect}+1
            }
        }

        /if (${NetBots[${tempw}].PctHPs} < ${temphp}) {
            /varset whoto ${tempw}
            /varset whoid ${tempid}
            /varset temphp ${NetBots[${tempw}].PctHPs}
        }
        /if (${NetBots[${tempw}].PetID} > 0) {
            /if (${NetBots[${tempw}].PetHP} < ${pethp}) {
                /varset petto ${tempw}
                /varset petid ${tempid}
                /varset pethp ${NetBots[${tempw}].PetHP}
            }
        }
    /next i
    /if (${avect} > 0) {
        /varcalc avehp ${avehp}/${avect}
    } else {
        /varset avehp 200
    }
    /if (${temphp} == 100) {
        | no need to heal
        /varset whoto ${Group.Leader}
        /varset whoid ${Group.Leader.ID}
    }

    | calling class specified skills
    /if (${IsHealer}) {
        /call ${Me.Class.ShortName}_Heal ${whoto} ${whoid} ${temphp} ${avehp}
        /if (${Macro.Return}) /return TRUE
        /varset heal_error failed class spell
    }

    | attempt to cast heal
    /for s 1 to ${heal_List.Size}
        /if (${heal_auto${s}_ok} != 2) {
            /if (${heal_auto${s}_ok} < 1) {
                /call Skill_CreateData heal_auto${s} "${heal_List[${s}]}"
                /if (!${Macro.Return}) {
                    /continue
                }
            }
            /call Skill_UpdateData heal_auto${s}
            /if (${heal_auto${s}_ok} != 2) /continue
        }
        /varset heal_error ${heal_error}#${s}${heal_auto${s}_spellname}w${whoto}|

        /call Skill_MySpellReady ${heal_auto${s}_spelltype} "${heal_auto${s}_spellname}"
        /if (${Macro.Return} != ${CAST_SUCCESS} && ${Macro.Return} != ${CAST_NOTMEMMED}) /continue

        /if (${IsDead}) {
            /if (${heal_auto${s}_notdead}) /continue
        }
        /if (${heal_auto${s}_tribute} == 1) {
            /if (!${basic_tribute}) /continue
        } else /if (${heal_auto${s}_tribute} == -1) {
            /if (${basic_tribute}) /continue
        }
        /if (${heal_auto${s}_raid} == 1) {
            /if (${Raid.Members} == 0) {
                /continue
            }
        } else /if (${heal_auto${s}_raid} == -1) {
            /if (${Raid.Members} > 0) {
                /continue
            }
        }
        /if (${heal_auto${s}_combat}) {
            /if (!${InCombat}) /continue
            /if (${Following}) /continue
            /if (!${Attacking}) /continue
        } else /if (${heal_auto${s}_calm}) {
            /if (${Buff_PausePeriod}) /continue
            /if (${InCombat}) /continue
            /if (${Following}) /continue
            /if (${Me.Invis}) /continue
        } else /if (${heal_auto${s}_notfollowing}) {
            /if (${Following}) /continue
        }

        /if (${heal_auto${s}_safe} == 1) {
            /if (!${IsSafe}) /continue
        } else /if (${heal_auto${s}_safe} == -1) {
            /if (${IsSafe}) /continue
        }

        /if (${heal_auto${s}_ispet}) {
            /if (!${Bool[${Me.Pet.ID}]}) /continue
        }

        /if (!${IsNoMana}) {
            /if (${heal_auto${s}_gom}) {
                /call Skill_GoMCheck
                /if (!${Macro.Return}) /continue
            }
            /if (${heal_auto${s}_donate}) {
                /if (${Me.PctHPs} < 40) /continue
            }
            /if (${heal_auto${s}_manabelow} <= ${Me.PctMana}) /continue
            /if (${heal_auto${s}_manaover} > ${Me.PctMana}) /continue
        }

        /varset heal_error ${heal_error}@1
        /if (${heal_auto${s}_avehpbelow} != 200) {
            /if (${avehp} >= ${heal_auto${s}_avehpbelow}) /continue
        }
        /varset heal_error ${heal_error}@2

        /call Skill_MySpellReady ${heal_auto${s}_spelltype} "${heal_auto${s}_spellname}"
        /if (${Macro.Return} != 0) {
            /varset heal_error ${heal_error}@nr${Macro.Return}
            /continue
        }

        /if (${heal_auto${s}_notarget} || ${heal_auto${s}_tot}) {
            /call heal_Cast -1 ${s} FALSE
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
            /varset heal_error ${heal_error}*tot ${heal_auto${s}_spellname} ${Macro.Return}
            /continue
        }

        /if (${heal_auto${s}_whoraw.Find[${whoto}]}) {
            /call heal_Cast ${whoto} ${s} FALSE
            /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
            /varset heal_error ${heal_error}*fwr ${s} ${whoto} ${heal_auto${s}_spellname} ${Macro.Return}
        }
        /if (${Bool[${Raid.Member[${heal_auto${s}_mate}].ID}]}) {
            /varset tempid ${Raid.Member[${heal_auto${s}_mate}].ID}
            /if (${Spawn[${tempid}].Name.Equal[${heal_auto${s}_mate}]}) {
                /if (!${Spawn[${tempid}].Dead}) {
                    /if (${Spawn[${tempid}].PctHPs} != 0) {
                        /call heal_Cast ${heal_auto${s}_matename} ${s} TRUE
                        /if (${Macro.Return} == ${CAST_SUCCESS}) /return TRUE
                        /varset heal_error ${heal_error}*mate ${heal_auto${s}_matename} ${heal_auto${s}_spellname} ${Macro.Return}
                    }
                }
            }
        }
        /if (${heal_auto${s}_petraw.Find[${petto}]}) {
            /call heal_CastPet ${petto} ${s} FALSE
            /if (${Macro.Return}) /return TRUE
            /varset heal_error ${heal_error}*pet
        }
    /next s

/RETURN FALSE

SUB heal_Cast(string targetName, int healindex, bool notmember)
    /declare spellresult string local
    /declare abortLine int local 100

    /declare curtarget int local -1

    /declare chp int local
    /declare nmid int local


    /if (${heal_auto${healindex}_notarget}) {
        /varset targetName ${Me}
        /varset chp ${Me.PctHPs}
        /varset nmid ${Me.ID}
    } else /if (${heal_auto${healindex}_tot}) {
        /if (${Bool[${Me.TargetOfTarget.ID}]}) {
            /if (${Me.TargetOfTarget.Dead}) /return -2
            /if (${Me.TargetOfTarget.Type.NotEqual[PC]}) /return -3
            /varset chp ${Me.TargetOfTarget.PctHPs}
            /varset targetName ${Target.Name}
            /varset nmid ${Target.ID}
        } else {
            /return -4
        }
    } else /if (${targetName.Equal[${Me}]}) {
        /varset chp ${Me.PctHPs}
        /varset nmid ${Me.ID}
    } else /if (${NetBots[${targetName}].InZone}) {
        /varset nmid ${NetBots[${targetName}].ID}
        /varset chp ${Spawn[${nmid}].PctHPs}
    } else /if (${Raid.Member[${targetName}].Spawn.ID}) {
        /varset chp ${Raid.Member[${targetName}].Spawn.PctHPs}
        /varset nmid ${Raid.Member[${targetName}].Spawn.ID}
    } else {
        /return -5
    }
    /if (${Spawn[${nmid}].Dead}) /return -6

    /if (${heal_auto${healindex}_hpbelow} != 200) {
        /if (${heal_auto${healindex}_force}) {
            /varset abortLine ${heal_auto${healindex}_hpbelow}
        } else /if (${heal_auto${healindex}_hpbelow} < ${chp}) {
            /varset heal_error ${heal_error}A${heal_auto${healindex}_hpbelow}B${chp}A
            /return -7
        }
    }
    /if (${heal_auto${healindex}_hpover} != 0) {
        /if (${heal_auto${healindex}_hpover} > ${chp}) {
            /return -8
        }
    }

    /if (${notmember}) {
        /if (!${Bool[${nmid}]}) /return -9

        /if (${Attacking}) {
            /if (${Target.ID} != 0) {
                /varset curtarget ${Target.ID}
            }
        }
        /call Skill_Cast_Data ${nmid} heal_auto${healindex} ${abortLine}
        /varset spellresult ${Macro.Return}
        /if (${Attacking}) {
            /if (${curtarget} != -1) {
                /if (${Bool[${Spawn[${curtarget}].ID}]}) {
                    /if (${curtarget} != ${Target.ID}) {
                        /call MobTarget ${curtarget}
                    }
                }
            }
        }
    } else {
        /if (!${heal_auto${healindex}_notarget} && !${heal_auto${healindex}_tot}) {
            /if (!${NetBots[${targetName}].InZone}) /return -10
        }
        /if (${heal_auto${healindex}_hot}) {
            | HoT is treated as buff
            /if (${Buff_Pause} || ${Buff_PausePeriod} > 0) /return -11

            /if (${IsHealer} || ${IsPAL}) {
                /call heal_HoTCheck ${heal_auto${healindex}_spellid} "${NetBots[${targetName}].ShortBuff}"
                /if (!${Macro.Return}) /return -12
            } else {
                /if (${NetBots[${targetName}].Buff.Find[${heal_auto${healindex}_spellid}]}) {
                    /return -13
                }
            }
            /varset abortLine -1
        }

        /if (${Attacking}) {
            /if (${Target.ID} != 0) {
                /varset curtarget ${Target.ID}
            }
        }
        /if (${heal_auto${healindex}_notarget} || (!${IsHealer} && ${heal_auto${healindex}_isae})) {
            /call Skill_Cast_Data -1 heal_auto${healindex} ${abortLine}
            /varset spellresult ${Macro.Return}
        } else {
            /call Skill_Cast_Data ${nmid} heal_auto${healindex} ${abortLine}
            /varset spellresult ${Macro.Return}
        }
        /if (${Attacking}) {
            /if (${curtarget} != -1) {
                /if (${Bool[${Spawn[${curtarget}].ID}]}) {
                    /if (${curtarget} != ${Target.ID}) {
                        /call MobTarget ${curtarget}
                    }
                }
            }
        }
    }

/RETURN ${spellresult}

SUB heal_CastPet(string targetName, int healindex)
    /declare spellresult string local
    /declare curtarget int local -1
    /if (!${NetBots[${targetName}].PetID}) /return FALSE

    /declare chp ${NetBots[${targetName}].PetHP}

    /if (${heal_auto${healindex}_hpbelow} != 200) {
        /if (${heal_auto${healindex}_hpbelow} < ${chp}) /return FALSE
    }
    /if (${heal_auto${healindex}_hpover} != 0) {
        /if (${heal_auto${healindex}_hpover} > ${chp}) /return FALSE
    }

    /if (${heal_auto${healindex}_hot}) {
        /if (${IsHealer}) {
            /call heal_HoTCheck ${heal_auto${healindex}_spellid} "${NetBots[${targetName}].PetBuff}"
            /if (!${Macro.Return}) /return FALSE
        } else {
            /if (${NetBots[${targetName}].PetBuff.Find[${heal_auto${healindex}_spellid}]}) {
                /return FALSE
            }
        }
    }

    /if (!${NetBots[${targetName}].InZone}) /return FALSE
    /if (${Attacking}) {
        /if (${Target.ID} != 0) {
            /varset curtarget ${Target.ID}
        }
    }
    /call Skill_Cast_Data ${NetBots[${targetName}].PetID} heal_auto${healindex} -1
    /varset spellresult ${Macro.Return}
    /if (${Attacking}) {
        /if (${curtarget} != -1) {
            /call MobTarget ${curtarget}
        }
    }
    /if (${spellresult} == ${CAST_SUCCESS}) /return TRUE
/RETURN FALSE

SUB heal_HoTSetup
    /call ArrayCreate heal_hotlist
    | Focused Celestial Regeneration(most powerful. overwrites all) 27660 3800, AE
    |/call ArrayAdd heal_hotlist 27660
    | Transcendent Torpor 15236 2000, Single
    /call ArrayAdd heal_hotlist 15236
    | Celestial Regeneration 27656 1900, AE
    |/call ArrayAdd heal_hotlist 27656
    | Pious Elixir 5259, 1170, Single
    /call ArrayAdd heal_hotlist 5259
    | Elixir of Divinity 8493 900, AE
    /call ArrayAdd heal_hotlist 8493
    | Holy Elixir 4882, 900, Single
    /call ArrayAdd heal_hotlist 4882
    | Spiritual Serenity 5416, 820 single
    /call ArrayAdd heal_hotlist 5416
    | Ghost of Renewal 8503, 630, AE
    /call ArrayAdd heal_hotlist 8503
    | Breath of Trushar 4899, 630 single
    /call ArrayAdd heal_hotlist 4899
    | Supernal Elixir 3475, 600 single
    /call ArrayAdd heal_hotlist 3475
    | Pious Cleansing 5293, 585 single
    /call ArrayAdd heal_hotlist 5293
    | Celestial Elixr 1522, 300 single
    /call ArrayAdd heal_hotlist 1522
    | Celestial Healing 1444, 180 single
    /call ArrayAdd heal_hotlist 1444
    | Celestial Cleansing 1283, 175 single
    /call ArrayAdd heal_hotlist 1283
    | Celestial Health 2175, 115 single
    /call ArrayAdd heal_hotlist 2175
    | Ethereal Cleansing 3683, 100 single
    /call ArrayAdd heal_hotlist 3683
    | Celestial Remedy 2502, 65 single
    /call ArrayAdd heal_hotlist 2502
/RETURN

SUB heal_HoTCheck(int spellid, string shortBuff)
    | check same hot is already in or not
    /if (${shortBuff.Find[${spellid} ]}) /return FALSE

    /declare i int local
    /declare hlsize int local
    /declare hotdata int local
    /call ArrayLength heal_hotlist
    /varset hlsize ${Macro.Return}
    /for i 1 to ${hlsize}
        /call ArrayGet heal_hotlist ${i}
        /varset hotdata ${Macro.Return}
        /if (${spellid} == ${hotdata}) /break
        /if (${shortBuff.Find[${hotdata} ]}) /return FALSE
    /next i
/return TRUE

SUB Heal_IsInjured
    /if (!${IsHealer} && !${IsSubHealer}) /return FALSE
    /if (${Me.PctHPs} < ${heal_threshould}) {
        |/bc IsInjured: me ${Me.PctHPs} threshould ${heal_threshould}
        /return TRUE
    }
    /if (${Group.Members} == 0) /return FALSE

    /declare i int local
    /for i 1 to ${Group.Members}
        /if (!${NetBots[${Group.Member[${i}].Name}].InZone}) /continue
        /if (${Group.Member[${i}].Distance} >= 220) /continue
        /if (${Group.Member[${i}].PctHPs} < ${heal_threshould}) {
            |/bc IsInjured: ${Group.Member[${i}].Name} ${Group.Member[${i}].PctHPs} threshould ${heal_threshould}
            /return TRUE
        }
    /next i
/return FALSE

#EVENT checkheallist "<#1#> heallist"
SUB EVENT_checkheallist(line, chatsender)
    /if (!${chatsender.Equal[${Me}]}) /return
    /declare i int local
    /declare s int local
    /declare hsize int local
    /declare tempw string local

    /for s 1 to ${heal_List.Size}
        /varset tempw
        /call ArrayLength heal_auto${s}_wholist
        /varset hsize ${Macro.Return}
        /if (${hsize} > 0) {
            /for i 1 to ${hsize}
                /call ArrayGet heal_auto${s}_wholist ${i}
                /varset tempw ${tempw}${Macro.Return}#
            /next i
        }
        /echo ${s} ${hsize} ${heal_auto${s}_spellname},${tempw}
    /next s
/RETURN
