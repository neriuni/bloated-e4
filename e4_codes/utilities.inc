SUB Utilities_Setup

    /declare IsWAR bool outer FALSE
    /declare IsPAL bool outer FALSE
    /declare IsSHD bool outer FALSE
    /declare IsCLR bool outer FALSE
    /declare IsBRD bool outer FALSE
    /declare IsSHM bool outer FALSE
    /declare IsROG bool outer FALSE
    /declare IsRNG bool outer FALSE
    /declare IsBER bool outer FALSE
    /declare IsMNK bool outer FALSE
    /declare IsDRU bool outer FALSE
    /declare IsBST bool outer FALSE
    /declare IsNEC bool outer FALSE
    /declare IsENC bool outer FALSE
    /declare IsWIZ bool outer FALSE
    /declare IsMAG bool outer FALSE

    /varset Is${Me.Class.ShortName} TRUE

    /declare IsTank bool outer FALSE
    /if (${IsSHD} || ${IsPAL} || ${IsWAR}) /varset IsTank TRUE
    /declare IsKnight bool outer FALSE
    /if (${IsSHD} || ${IsPAL}) /varset IsKnight TRUE
    /declare IsHealer bool outer FALSE
    /if (${IsSHM} || ${IsCLR} || ${IsDRU}) /varset IsHealer TRUE
    /declare IsSubHealer bool outer FALSE
    /if (${IsBST} || ${IsRNG} || ${IsPAL}) /varset IsSubHealer TRUE
    /declare IsCaster bool outer FALSE
    /if (${IsENC} || ${IsWIZ} || ${IsMAG} || ${IsNEC}) /varset IsCaster TRUE
    /declare IsAttacker bool outer FALSE
    /if (${IsBST} || ${IsRNG} || ${IsBER} || ${IsROG} || ${IsMNK} || ${IsBRD}) /varset IsAttacker TRUE
    /declare IsPetClass bool outer FALSE
    /if (${IsBST} || ${IsMAG} || ${IsNEC} || ${IsSHM} || ${IsDRU} || ${IsSHD} || ${IsENC}) /varset IsPetClass TRUE
    /declare IsSubPetClass bool outer FALSE
    /if (${IsSHM} || ${IsDRU} || ${IsSHD} || ${IsENC}) /varset IsPetClass TRUE
    /declare IsPureMelee bool outer FALSE
    /if (${IsWAR} || ${IsROG} || ${IsBER} || ${IsMNK}) /varset IsPureMelee TRUE
    /declare IsNoMana bool outer FALSE
    /if (${IsWAR} || ${IsROG} || ${IsBER} || ${IsMNK} || ${IsBRD}) /varset IsNoMana TRUE

    /declare IsHardcore bool outer FALSE

    /declare utilities_petgholdpos int outer -1
    /declare utilities_pettauntpos int outer -1
    /if (${Me.Pet.ID}) {
        /call utilities_petgholdcheck
    }

    | IDs of emulation server
	/declare ID_CLASS_WAR int outer 1
	/declare ID_CLASS_CLR int outer 2
	/declare ID_CLASS_PAL int outer 3
	/declare ID_CLASS_RNG int outer 4
	/declare ID_CLASS_SHD int outer 5
	/declare ID_CLASS_DRU int outer 6
	/declare ID_CLASS_MNK int outer 7
	/declare ID_CLASS_BRD int outer 8
	/declare ID_CLASS_ROG int outer 9
	/declare ID_CLASS_SHM int outer 10
	/declare ID_CLASS_NEC int outer 11
	/declare ID_CLASS_WIZ int outer 12
	/declare ID_CLASS_MAG int outer 13
	/declare ID_CLASS_ENC int outer 14
	/declare ID_CLASS_BST int outer 15
	/declare ID_CLASS_BER int outer 16

    /declare ID_RACE_INVISIBLEMAN int outer 127
    /declare ID_RACE_SNAKE int outer 468
    /declare ID_RACE_WARBOAR int outer 321
    /declare ID_RACE_GNOMEWORK int outer 457
    /declare ID_RACE_GINGERBREAD int outer 666
    /declare ID_RACE_DRAKE1 int outer 430
    /declare ID_RACE_DRAKE2 int outer 432
    /declare ID_RACE_DRAKE3 int outer 89

    /declare ID_RACE_IMP int outer 46

    /declare tempIsDisc bool outer FALSE
    /declare IsDisc bool outer FALSE
    /declare DiscSickness timer outer

    | target cooldown timer
    /declare TargetCoolDown timer outer

    /call Core_AddTask_ZoneChange Utilities_ZoneChange

/RETURN

SUB Utilities_Main
    /call Utilities_UpdateDisc
/RETURN

SUB Utilities_ZoneChange
    | need to find better way to determin hard core zones
    /varset IsHardcore FALSE
/RETURN

| Target command
SUB MobTarget(int id)
    /if (${id} <= 0) /return
    /target id ${id}
    /varset TargetCoolDown 3
/RETURN

| set to ghold
SUB Utilities_petghold
    /if (!${IsPetClass}) /return
    /declare pospos int local -1
    /if (${Defined[combat_pettank]}) {
        /if (${combat_pettank}) {
            /if (${utilities_pettauntpos} == -1) {
                /call utilities_petgholdcheck
                /if (${utilities_pettauntpos} == -1) /return
            }
            /varset pospos ${utilities_pettauntpos}
            /if (${pospos} != -1) {
                /if (!${Window[PetInfoWindow].Child[PIW_Pet${pospos}_Button].Checked}) {
                    /notify PetInfoWindow PIW_Pet${pospos}_Button leftmouseup
                }
            }
        }
    }
    /if (${utilities_petgholdpos} == -1) {
        /call utilities_petgholdcheck
        /if (${utilities_petgholdpos} == -1) /return
    }
    /varset pospos ${utilities_petgholdpos}
    /if (${pospos} != -1) {
        /if (!${Window[PetInfoWindow].Child[PIW_Pet${pospos}_Button].Checked}) {
            /notify PetInfoWindow PIW_Pet${pospos}_Button leftmouseup
        }
    }
/RETURN

| check pet ghold status
SUB utilities_petgholdcheck
    /if (!${Me.Pet.ID}) /return
    /declare i int local
    /for i 0 to 13
        /if (${Window[PetInfoWindow].Child[PIW_Pet${i}_Button].Text.Equal[ghold]}) {
            /varset utilities_petgholdpos ${i}
        } else /if (${Window[PetInfoWindow].Child[PIW_Pet${i}_Button].Text.Equal[taunt]}) {
            /varset utilities_pettauntpos ${i}
        }
    /next i
/RETURN

| check disc is in use instead of using ${Me.ActiveDisc.ID}
SUB Utilities_UpdateDisc
    /if (${Me.ActiveDisc.ID}) {
        /if (!${tempIsDisc}) {
            /varset tempIsDisc TRUE
        }
    } else {
        /if (${tempIsDisc}) {
            /varset tempIsDisc FALSE
            /varset DiscSickness 1s
        }
    }
    /if (${tempIsDisc} || !${tempIsDisc} && ${DiscSickness} > 0) {
        /varset IsDisc TRUE
    } else {
        /varset IsDisc FALSE
    }
/RETURN

| Get Mana of group(non-mana class returns 200)
SUB CheckMemberMana(int no)
    /if (${Group.Members} == 0) /return 200
    /if (!${Bool[${Group.Member[${no}].Name}]}) /return 200
    /if (!${Bool[${NetBots[${Group.Member[${no}].Name}].Class}]}) /return 200
    /declare tcls int local ${NetBots[${Group.Member[${no}].Name}].Class.ID}

    /if (${tcls} == ${ID_CLASS_BRD} || ${tcls} == ${ID_CLASS_WAR} || ${tcls} == ${ID_CLASS_MNK} || ${tcls} == ${ID_CLASS_BER} || ${tcls} == ${ID_CLASS_ROG}) {
        /return 200
    }
    /if (${no} == 0) /return ${Me.PctMana}
/RETURN ${Group.Member[${no}].PctMana}

| Get avarage HP of group
SUB CheckGroupHP(int threshould)
    /if (${Group.Members} == 0) /return -1

    /declare i int local
    /declare lmana int local 0

    /if (${Me.PctHPs} <= ${threshould}) {
        /varset lmana 1
    }

    /for i 1 to ${Group.Members}
        /if (!${NetBots[${Group.Member[${i}].Name}].InZone}) /continue
        /if (${Group.Member[${i}].Distance} >= 120) /continue
        /if (${NetBots[${Group.Member[${i}].Name}].PctHPs} > ${threshould}) /continue
        /varcalc lmana ${lmana}+1
    /next i
/RETURN ${lmana}

| Get avarage Mana of group
SUB CheckGroupMana(int threshould)
    /if (${Group.Members} == 0) /return -1

    /declare i int local
    /declare lmana int local 0

    /if (${Me.MaxMana} != 0) {
        /if (${Me.PctMana} <= ${threshould}) {
            /varset lmana 1
        }
    }

    /for i 1 to ${Group.Members}
        /if (!${NetBots[${Group.Member[${i}].Name}].InZone}) /continue
        /if (${Group.Member[${i}].Class.ID} == ${ID_CLASS_WAR}) /continue
        /if (${Group.Member[${i}].Class.ID} == ${ID_CLASS_MNK}) /continue
        /if (${Group.Member[${i}].Class.ID} == ${ID_CLASS_ROG}) /continue
        /if (${Group.Member[${i}].Class.ID} == ${ID_CLASS_BER}) /continue
        /if (${Group.Member[${i}].Class.ID} == ${ID_CLASS_BRD}) /continue
        /if (${Group.Member[${i}].Distance} >= 120) /continue
        /if (${NetBots[${Group.Member[${i}].Name}].PctMana} > ${threshould}) /continue
        /varcalc lmana ${lmana}+1
    /next i
/RETURN ${lmana}

| Get count of Mana or Endurance below threshould
SUB CheckGroupEnergy(int threshould)
    /if (${Group.Members} == 0) {
        /if (${IsPureMelee}) {
            /if (${Me.PctEndurance} > ${threshould}) /return 0
            /return 1
        } else {
            /if (${Me.PctMana} > ${threshould}) /return 0
            /return 1
        }
    }

    /declare i int local
    /declare lmana int local 0

    /if (${Me.MaxMana} != 0) {
        /if (${Me.PctMana} <= ${threshould}) {
            /varset lmana 1
        }
    } else {
        /if (${Me.PctEndurance} <= ${threshould}) {
            /varset lmana 1
        }
    }

    /for i 1 to ${Group.Members}
        /if (!${NetBots[${Group.Member[${i}].Name}].InZone}) /continue
        /if (${Group.Member[${i}].Class.ID} == ${ID_CLASS_BRD}) /continue
        /if (${Group.Member[${i}].Distance} >= 120) /continue
        /if (${Group.Member[${i}].Class.ID} == ${ID_CLASS_WAR} || ${Group.Member[${i}].Class.ID} == ${ID_CLASS_MNK} || ${Group.Member[${i}].Class.ID} == ${ID_CLASS_ROG} || ${Group.Member[${i}].Class.ID} == ${ID_CLASS_BER}) {
            /if (${NetBots[${Group.Member[${i}].Name}].PctEndurance} > ${threshould}) /continue
            /varcalc lmana ${lmana}+1
        } else {
            /if (${NetBots[${Group.Member[${i}].Name}].PctMana} > ${threshould}) /continue
            /varcalc lmana ${lmana}+1
        }
    /next i
/RETURN ${lmana}


| dictionary
SUB DictionaryCreate(name)
    /if (${Defined[utilities_dictionary_${name}]}) /return FALSE
    /declare utilities_dictionary_${name} map outer
/RETURN TRUE

SUB DictionaryLength(name)
    /if (!${Defined[utilities_dictionary_${name}]}) /return -1
/RETURN ${utilities_dictionary_${name}.Count}

SUB DictionaryAdd(name, key, data)
    /if (!${Defined[utilities_dictionary_${name}]}) /return -1
    /if (!${utilities_dictionary_${name}.Add[${key},${data}]}) /return -1
/RETURN 1

SUB DictionaryGet(name, key)
    /if (!${Defined[utilities_dictionary_${name}]}) /return NULL
    /declare rd mapiterator local
    /vardata rd utilities_dictionary_${name}.Find[${key}]
/RETURN ${rd.Value}

SUB DictionaryDestroy(name)
    /if (!${Defined[utilities_dictionary_${name}]}) /return -1
    /deletevar utilities_dictionary_${name}
/RETURN 1


| Array
SUB ArrayCreate(name)
    /if (${Defined[utilities_array_${name}]}) /return FALSE
    /declare utilities_array_${name} map outer
    |/echo create utilities_array_${name} r ${Defined[utilities_array_${name}]}
/RETURN TRUE

SUB ArrayLength(name)
    /if (!${Defined[utilities_array_${name}]}) /return -1
    |/echo length ${name} d ${utilities_array_${name}.Count}
/RETURN ${utilities_array_${name}.Count}

SUB ArrayAdd(name, data)
    /if (!${Defined[utilities_array_${name}]}) /return -1
    /declare index int local 1
    /varcalc index ${index}+${utilities_array_${name}.Count}
    /if (!${utilities_array_${name}.Add[${index},${data}]}) /return -1
    |/echo add n ${name} d ${data}
/RETURN 1

SUB ArrayPut(name, index, data)
    /if (!${Defined[utilities_array_${name}]}) /return -1
    /if (${index} > ${utilities_array_${name}.Count}) /return -1
    /if (${index} < 1) /return -1
    /if (!${utilities_array_${name}[${index},${data}]}) /return -1
/RETURN 1

SUB ArrayGet(name, index)
    /if (!${Defined[utilities_array_${name}]}) /return NULL
    /if (${index} > ${utilities_array_${name}.Count}) /return NULL
    /if (${index} < 1) /return NULL
    /declare rd mapiterator local
    /vardata rd utilities_array_${name}.Find[${index}]
    |/echo get n ${name} i ${index} d ${rd.Value}
/RETURN ${rd.Value}

SUB ArrayDestroy(name)
    /if (!${Defined[utilities_array_${name}]}) /return -1
    /deletevar utilities_array_${name}
/RETURN 1

| Queue
SUB QueueCreate(name)
    /if (${Defined[utilities_array_${name}]}) /return FALSE
    /declare utilities_array_${name} queue outer
    |/echo create utilities_array_${name} r ${Defined[utilities_array_${name}]}
/RETURN TRUE

SUB QueueLength(name)
    /if (!${Defined[utilities_array_${name}]}) /return -1
    |/echo length ${name} d ${utilities_array_${name}.Count}
/RETURN ${utilities_array_${name}.Count}

SUB QueuePush(name, data)
    /if (!${Defined[utilities_array_${name}]}) /return -1
    /if (!${utilities_array_${name}.Push[${data}]}) /return -1
    |/echo add n ${name} d ${data}
/RETURN 1

SUB QueuePop(name)
    /if (!${Defined[utilities_array_${name}]}) /return NULL
/RETURN ${utilities_array_${name}.Pop}

SUB QueuePeek(name)
    /if (!${Defined[utilities_array_${name}]}) /return NULL
/RETURN ${utilities_array_${name}.Peek}

SUB QueueDestroy(name)
    /if (!${Defined[utilities_array_${name}]}) /return -1
    /deletevar utilities_array_${name}
/RETURN 1

| Legacy stuff from e3
Sub iniToVarV(Ini_Key, VarToMake, VarType)
    /if (!${Bool[${Ini[${Ini_Key}]}]}) {
        /return FALSE
    }
    /if (${VarType.Equal[bool]}) {
        /if (${Defined[${VarToMake}]}) /deletevar ${VarToMake}
        /declare ${VarToMake} bool outer
        /if (${Select[${Ini[${Ini_Key}]},Yes,On]} > 0) {
            /varset ${VarToMake} TRUE
        } else /if (${Select[${Ini[${Ini_Key}]},No,Off]} > 0) {
            /varset ${VarToMake} FALSE
        }
        /return TRUE
    } else /if (${Ini[${Ini_Key}].Length}) {
        /if (${Defined[${VarToMake}]}) /deletevar ${VarToMake}
        /declare ${VarToMake} ${VarType} outer ${Ini[${Ini_Key}]}
        /return TRUE
    }
/return FALSE

SUB argueString(WhatToFind, givenData)
    /declare indx int local
    /declare astr string local

    /varset indx ${givenData.Find[${WhatToFind}]}
    /if (${Bool[${indx}]}) {
        /varset astr ${givenData.Right[${Math.Calc[-1*(${indx}+${WhatToFind.Length}-1)].Int}]}
        /varset indx ${astr.Find[/]}
        /if (${Bool[${indx}]}) {
            /varset astr ${astr.Left[${Math.Calc[${indx}-1].Int}]}
        }
    }
/RETURN ${astr}

Sub IniToArrayV(IniKey, ArrayName)
    /declare i int local
    /declare j int local
    /declare array_size int local
    /declare ArrayScope string local outer
    /if (${IniKey.Right[1].Equal[#]} && ${Ini[${IniKey}1].Length}) {
        /call IniToArray_Deprecated "${IniKey}" "${ArrayName}"
        /return
    }
    /if (${IniKey.Right[1].Equal[#]}) /varset IniKey ${IniKey.Left[-1]}
    /if (${Defined[${ArrayName}]}) /deletevar ${ArrayName}

    /declare ifile string local ${IniKey.Arg[1,,]}
    /declare isec string local ${IniKey.Arg[2,,]}
    /declare ikey string local ${IniKey.Arg[3,,]}

    /if (${Ini.File[${ifile}].Section[${isec}].Key[${ikey}].Count} > 0) {
        /varset array_size 0
        /for i 1 to ${Ini.File[${ifile}].Section[${isec}].Key[${ikey}].Count}
            /if (${Ini.File[${ifile}].Section[${isec}].Key[${ikey}].ValueAtIndex[${i}].Length} > 0) {
                /varcalc array_size ${array_size}+1
            }
        /next i
        /if (${array_size} == 0) /return
        /declare ${ArrayName}[${array_size}] string outer
        /varset j 1
        /for i 1 to ${Ini.File[${ifile}].Section[${isec}].Key[${ikey}].Count}
            /if (${Ini.File[${ifile}].Section[${isec}].Key[${ikey}].ValueAtIndex[${i}].Length} > 0) {
                /varset ${ArrayName}[${j}] ${Ini.File[${ifile}].Section[${isec}].Key[${ikey}].ValueAtIndex[${i}]}
                /varcalc j ${j}+1
            }
        /next i
    }
/return

Sub IniToArray_Deprecated(IniKey, ArrayName)
    /declare i int local 1
    |/declare IniKey ${File},${Section},${Key}
    /declare ArrayScope string local outer
    /if (${Defined[${ArrayName}]}) /deletevar ${ArrayName}
    /if (${Ini[${IniKey}1].Length}) {
        /declare count int local 0
        :NextKey
        /if (${Ini[${IniKey}${i}].Length}) {
            /varset count ${i}
            /varcalc i ${i}+1
            /goto :NextKey
        }
        /declare ${ArrayName}[${count}] string ${ArrayScope}
        /for i 1 to ${count}
            /varset ${ArrayName}[${i}] ${Ini[${IniKey}${i}]}
        /next i
    |} else {
    |  /declare ${ArrayName}[0] string ${ArrayScope}
    }
/return

Sub Type(InStr)
    /declare char string local
    /declare loopctr int local
    /for loopctr 1 to ${InStr.Length}
        /varset char ${InStr.Mid[${loopctr},1]}
        /if (!${char.Length}) {
            /nomodkey /keypress space chat
        } else {
            /nomodkey /keypress ${char} chat
        }
    /next loopctr
/return

SUB InGroup(string name)
/RETURN ${NetBots[${name}].InGroup}

SUB CheckGroupInZoneOrMe(string name)
    /if (${Group.Members} > 0) {
    	/if (!${Bool[${Group.Member[${name}].ID}]}) /return FALSE
    	/if (!${NetBots[${name}].InZone}) /return FALSE
    } else /if (${name.NotEqual[${Me}]}) {
        /return FALSE
    }
/RETURN TRUE

|-----------------------
|- Creates ini entries from supplied ini keys.
|- Allows the user to toggle whether or not to overwrite an existing entry
|-----------------------
|- @param Ini_Key: The ini file, section, and entry to write to
|- @param WhatToWrite: Data to write to the given Ini_Key
|- @param OverWrite(bool): Allow overwrite
|-----------------------
|- Syntax: /call WriteToIni "Ini_Key" "WhatToWrite" "OverWrite"
|-	Ex:
|-		Blank entry 				- 	/call WriteToIni "Loot.ini,B,Bone Chips"
|-		Entry with setting 			- 	/call WriteToIni "Loot.ini,B,Bone Chips" "Keep"
|-		Overwrite existing entry	- 	/call WriteToIni "Loot.ini,B,Bone Chips" "Destroy" TRUE
|-----------------------
SUB WriteToIni(Ini_Key, WhatToWrite, bool OverWrite)
	| By switching ':'s to '*'s in arguemnt 3, to avoid issues when reading variables from the inis.
	/if (${Ini_Key.Arg[3,,].Find[:]}) /varset Ini_Key ${Ini_Key.Arg[1,,]},${Ini_Key.Arg[2,,]},${Ini_Key.Arg[3,,].Replace[:,;]}
	
	| If the Ini_Key already exists, check overwrite	
	/if (${Ini[${Ini_Key}].Length}) {
		/if (${OverWrite}) /goto :OverWrite
	} else {
		:OverWrite
		/if (${Defined[WhatToWrite]}) {
			/ini "${Ini_Key.Arg[1,,]}" "${Ini_Key.Arg[2,,]}" "${Ini_Key.Arg[3,,]}" "${WhatToWrite}"
		
		| Else, create the entry only.
		} else {
			/ini "${Ini_Key.Arg[1,,]}" "${Ini_Key.Arg[2,,]}" "${Ini_Key.Arg[3,,]}"
		}
	}
/RETURN


#bind NetGroupCommand /netgroup
SUB Bind_NetGroupCommand(string Cmd1, string Cmd2, string Cmd3, string Cmd4, string Cmd5)
    /if (!${E4_Loaded}) /return
    /declare txt string local ${Cmd1}
    /declare i int local
    /for i 2 to 5
        /if (${Bool[${Cmd${i}}]}) {
            /varset txt ${txt} ${Cmd${i}}
        } else {
            /break
        }
    /next i
    /if (${Group.Members} == 0) {
        /docommand ${txt}
    } else /if (${Bool[${DanNet.Peers.Find[_${Me}|]}]}) {
        /dgga ${txt}
    } else {
        /bcga /${txt}
    }
/RETURN

#bind NetGroupButMeCommand /netother
SUB Bind_NetGroupButMeCommand(string Cmd1, string Cmd2, string Cmd3, string Cmd4, string Cmd5)
    /if (!${E4_Loaded}) /return
    /declare txt string local ${Cmd1}
    /declare i int local
    /for i 2 to 5
        /if (${Bool[${Cmd${i}}]}) {
            /varset txt ${txt} ${Cmd${i}}
        } else {
            /break
        }
    /next i
    /if (${Group.Members} == 0) {
        /docommand ${txt}
    } else /if (${Bool[${DanNet.Peers.Find[_${Group.Leader.Name}|]}]}) {
        /dgge ${txt}
    } else {
        /bcg /${txt}
    }
/RETURN

#bind NetAllCommand /netall
SUB Bind_NetAllCommand(string Cmd1, string Cmd2, string Cmd3, string Cmd4, string Cmd5)
    /if (!${E4_Loaded}) /return
    /declare txt string local ${Cmd1}
    /declare i int local
    /for i 2 to 5
        /if (${Bool[${Cmd${i}}]}) {
            /varset txt ${txt} ${Cmd${i}}
        } else {
            /break
        }
    /next i
    /if (${Bool[${DanNet.Peers.Find[_${Group.Leader.Name}|]}]}) {
        /dgae all ${txt}
    } else {
        /bcaa /${txt}
    }
/RETURN

SUB Net_ToCommand(whot, Cmd)
    /if (${whot.Equal[${Me}]}) {
        ${Cmd}
    } else /if (${Bool[${DanNet.Peers.Find[_${whot}|]}]}) {
        /dex ${whot} ${Cmd}
    } else {
        /bct ${whot} /${Cmd}
    }
/RETURN

SUB Net_CheckAlive(whot)
    /if (${Bool[${DanNet.Peers.Find[_${whot}|]}]}) {
        /declare res string local
        /dquery ${whot} -q Macro
        /varset res ${DanNet.Q}
        /if (${Bool[${res}]}) {
            /if (${res.Equal[e4.mac]}) /return TRUE
        }
    }
/RETURN FALSE

SUB Net_Query(whot, cvar)
    /dquery ${whot} -q ${cvar}
/RETURN ${DanNet.Q}
